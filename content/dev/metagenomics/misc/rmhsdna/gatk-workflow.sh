#!/bin/bash

# DCI Bioinformatics
# Code for experimenting with GATK features

gatkindex() {
    local fadir=$1
    local fafile=$2
    local dictfile="$(basename ${fafile} .fa)".dict
    
    ${singularity} exec --app bwa \
		   --bind ${fadir}:/mnt/fadir/:rw \
		   ${mysif} \
		   bwa index -a bwtsw /mnt/fadir/${fafile}

    ${singularity} exec \
		   --bind ${fadir}:/mnt/fadir/:rw \
		   --bind ${tmpdir}:/mnt/tmpdir/:rw \
		   ${mysif} \
		   java -jar -Xmx${heapsize} ${picardjar} CreateSequenceDictionary \
		   --REFERENCE /mnt/fadir/${fafile} \
		   --OUTPUT /mnt/fadir/${dictfile} \
		   --TMP_DIR  /mnt/tmpdir/ \
		   --CREATE_MD5_FILE true

    ${singularity} exec --app samtools \
		   --bind ${fadir}:/mnt/fadir/:rw \
		   ${mysif} \
		   samtools  faidx /mnt/fadir/${fafile}

}

    

bwastep() {
	local fqdir=$1
	local R1=$2
	local R2=$3
	local RGID=$4
	local RGLB=$5
	local RGSM=$6
	local RGPU=$7

	local bwasam=${RGID}.sam
	local rgbam=${RGID}-RG-coordsorted.bam

	# Align using BWA MEM
	singularity exec --app bwa \
		--bind ${fadir}:/mnt/fadir/:ro \
		--bind ${fqdir}:/mnt/fqdir/:ro \
		--bind ${procdir}:/mnt/procdir/:rw \
		${mysif} \
		bwa mem -aM -t ${bwacores} /mnt/fadir/${fafile} /mnt/fqdir/${R1} /mnt/fqdir/${R2} \
		>${procdir}${bwasam}

	# ADD RGs and sort by coordinate
	singularity exec \
		--bind ${procdir}:/mnt/procdir/:rw \
		--bind ${tmpdir}:/mnt/tmpdir/:rw \
		${mysif} \
		java -jar -Xmx${heapsize} ${picardjar} AddOrReplaceReadGroups \
		--INPUT /mnt/procdir/${bwasam} \
		--OUTPUT /mnt/procdir/${rgbam} \
		--RGID ${RGID} \
		--RGLB ${RGLB} \
		--RGSM ${RGSM} \
		--RGPL illumina \
		--RGPU ${RGPU} \
		--TMP_DIR /mnt/tmpdir/ \
		--VALIDATION_STRINGENCY STRICT \
		--CREATE_INDEX true \
		--CREATE_MD5_FILE true \
		--SORT_ORDER coordinate

}



getflagstat () {
    local bamfile=$1
    local stem="$(basename ${bamfile} .bam)"
    local mappedbam=${stem}-mapped.bam
    local statfile=${procdir}/${stem}-stats.tsv

    ${singularity} exec --app samtools \
		   --bind ${procdir}:/mnt/procdir/:ro \
		   ${mysif} \
		   samtools flagstat -@ ${bwacores} -O tsv /mnt/procdir/${bamfile}

    ${singularity} exec --app samtools \
		   --bind ${procdir}:/mnt/procdir/:rw \
		   ${mysif} \
		   samtools view --bam --threads ${bwacores} -F 4 --output /mnt/procdir/${mappedbam} /mnt/procdir/${bamfile}  
}
		 

    
