# Setup

First we load libraries.

``` r
library(readr)
library(fs)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(tibble)
library(Biostrings)
```

    ## Loading required package: BiocGenerics

    ## 
    ## Attaching package: 'BiocGenerics'

    ## The following objects are masked from 'package:dplyr':
    ## 
    ##     combine, intersect, setdiff, union

    ## The following object is masked from 'package:fs':
    ## 
    ##     path

    ## The following objects are masked from 'package:stats':
    ## 
    ##     IQR, mad, sd, var, xtabs

    ## The following objects are masked from 'package:base':
    ## 
    ##     anyDuplicated, aperm, append, as.data.frame, basename, cbind,
    ##     colnames, dirname, do.call, duplicated, eval, evalq, Filter, Find,
    ##     get, grep, grepl, intersect, is.unsorted, lapply, Map, mapply,
    ##     match, mget, order, paste, pmax, pmax.int, pmin, pmin.int,
    ##     Position, rank, rbind, Reduce, rownames, sapply, setdiff, sort,
    ##     table, tapply, union, unique, unsplit, which.max, which.min

    ## Loading required package: S4Vectors

    ## Loading required package: stats4

    ## 
    ## Attaching package: 'S4Vectors'

    ## The following objects are masked from 'package:dplyr':
    ## 
    ##     first, rename

    ## The following object is masked from 'package:utils':
    ## 
    ##     findMatches

    ## The following objects are masked from 'package:base':
    ## 
    ##     expand.grid, I, unname

    ## Loading required package: IRanges

    ## 
    ## Attaching package: 'IRanges'

    ## The following objects are masked from 'package:dplyr':
    ## 
    ##     collapse, desc, slice

    ## Loading required package: XVector

    ## Loading required package: GenomeInfoDb

    ## 
    ## Attaching package: 'Biostrings'

    ## The following object is masked from 'package:base':
    ## 
    ##     strsplit

``` r
library(tools)
library(stringr)
library(here)
```

    ## here() starts at /hpc/home/josh/project_repos/mic_course/2023-mic

## Paths, Directories, and Shell Variables

To keep the code readable and portable, it is nice to assign paths to
variables. We also need to use the R `Sys.setenv` command to make shell
variables that can be used in the bash chunks below.

``` r
"content/config.R" %>%
    here() %>%
    source()

# make directory for output
if (dir_exists(demux_dir)) {dir_delete(demux_dir)}
dir_create(demux_dir)
```

## Check Data Integrity

``` r
atacama_md5sum %>%
    read_delim(delim=" ", 
               trim_ws = TRUE,
               col_names = c("md5sum", "filename")) ->
    atacama_md5
```

    ## Rows: 4 Columns: 2
    ## ── Column specification ────────────────────────────────────────────────────────
    ## Delimiter: " "
    ## chr (2): md5sum, filename
    ## 
    ## ℹ Use `spec()` to retrieve the full column specification for this data.
    ## ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.

``` r
atacama_data_dir %>%
    list.files(full.names = TRUE) %>%
    md5sum %>%
    enframe %>%
    dplyr::rename(fullpath=name, observed_md5=value) %>%
    mutate(filename=basename(fullpath)) %>%
    right_join(atacama_md5, by="filename") %>%
    mutate(match=identical(md5sum,observed_md5)) ->
    md5check

md5check
```

    ## # A tibble: 4 × 5
    ##   fullpath                                    observed_md5 filename md5sum match
    ##   <chr>                                       <chr>        <chr>    <chr>  <lgl>
    ## 1 /hpc/group/mic-2023/rawdata/atacama_data/b… df32df8dc62… barcode… df32d… TRUE 
    ## 2 /hpc/group/mic-2023/rawdata/atacama_data/f… b5ee4718605… forward… b5ee4… TRUE 
    ## 3 /hpc/group/mic-2023/rawdata/atacama_data/r… 2ac4ca47f39… reverse… 2ac4c… TRUE 
    ## 4 /hpc/group/mic-2023/rawdata/atacama_data/s… 380b04ad294… sample_… 380b0… TRUE

``` r
md5check %>%
    pull(match) %>%
    all ->
    all_md5s_ok

stopifnot(all_md5s_ok)
```

# Demultiplexing

We will be using `fastq-multx` to demultiplex the data. It does not have
very good documentation. But, we can get some instructions if we run it
without any command line options.

``` bash
fastq-multx -h
```

    ## Usage: fastq-multx [-g|-l|-B] <barcodes.fil> <read1.fq> -o r1.%.fq [mate.fq -o r2.%.fq] ...
    ## Version: 1.02.772
    ## 
    ## Output files must contain a '%' sign which is replaced with the barcode id in the barcodes file.
    ## Output file can be n/a to discard the corresponding data (use this for the barcode read)
    ## 
    ## Barcodes file (-B) looks like this:
    ## 
    ## <id1> <sequence1>
    ## <id2> <sequence2> ...
    ## 
    ## Default is to guess the -bol or -eol based on clear stats.
    ## 
    ## If -g is used, then it's parameter is an index lane, and frequently occuring sequences are used.
    ## 
    ## If -l is used then all barcodes in the file are tried, and the *group* with the *most* matches is chosen.
    ## 
    ## Grouped barcodes file (-l or -L) looks like this:
    ## 
    ## <id1> <sequence1> <group1>
    ## <id1> <sequence1> <group1>
    ## <id2> <sequence2> <group2>...
    ## 
    ## Mated reads, if supplied, are kept in-sync
    ## 
    ## Options:
    ## 
    ## -o FIL1     Output files (one per input, required)
    ## -g SEQFIL   Determine barcodes from the indexed read SEQFIL
    ## -l BCFIL    Determine barcodes from any read, using BCFIL as a master list
    ## -L BCFIL    Determine barcodes from <read1.fq>, using BCFIL as a master list
    ## -B BCFIL    Use barcodes from BCFIL, no determination step, codes in <read1.fq>
    ## -H          Use barcodes from illumina's header, instead of a read
    ## -b          Force beginning of line (5') for barcode matching
    ## -e          Force end of line (3') for batcode matching
    ## -t NUM      Divide threshold for auto-determine by factor NUM (1), > 1 = more sensitive
    ## -G NAME     Use group(s) matching NAME only
    ## -x          Don't trim barcodes off before writing out destination
    ## -n          Don't execute, just print likely barcode list
    ## -v C        Verify that mated id's match up to character C (Use ' ' for illumina)
    ## -m N        Allow up to N mismatches, as long as they are unique (1)
    ## -d N        Require a minimum distance of N between the best and next best (2)
    ## -q N        Require a minimum phred quality of N to accept a barcode base (0)

## Barcode Table

The `fastq-multx` help tells us that we can supply it with a
tab-separated file specifying the sample ID in the first column and the
barcode sequence that corresponds with it in the second column. We can
easily generate a file like this from our map file using a bit of
tidyverse magic. First we load the map file into a dataframe (skipping
the extra header line that starts with “#q2”).

``` r
read_tsv(map_file, comment= "#q2") ->
    atacama_metadata
```

    ## Rows: 75 Columns: 21
    ## ── Column specification ────────────────────────────────────────────────────────
    ## Delimiter: "\t"
    ## chr  (6): sample-id, barcode-sequence, extract-group-no, transect-name, site...
    ## dbl (15): elevation, extract-concen, amplicon-concentration, depth, ph, toc,...
    ## 
    ## ℹ Use `spec()` to retrieve the full column specification for this data.
    ## ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.

``` r
atacama_metadata
```

    ## # A tibble: 75 × 21
    ##    `sample-id` `barcode-sequence` elevation `extract-concen`
    ##    <chr>       <chr>                  <dbl>            <dbl>
    ##  1 BAQ1370.1.2 GCCCAAGTTCAC            1370            0.019
    ##  2 BAQ1370.3   GCGCCGAATCTT            1370            0.124
    ##  3 BAQ1370.1.3 ATAAAGAGGAGG            1370            1.2  
    ##  4 BAQ1552.1.1 ATCCCAGCATGC            1552            0.722
    ##  5 BAQ1552.2   GCTTCCAGACAA            1552            0.017
    ##  6 BAQ2420.1.1 ACACAGTCCTGA            2420            0.35 
    ##  7 BAQ2420.1.2 ATTATACGGCGC            2420            0.108
    ##  8 BAQ2420.2   TAAACGCGACTC            2420            0.089
    ##  9 BAQ2420.3   CCTCGGGTACTA            2420            0.083
    ## 10 BAQ2420.1.3 ATTCAGATGGCA            2420            0.132
    ## # ℹ 65 more rows
    ## # ℹ 17 more variables: `amplicon-concentration` <dbl>,
    ## #   `extract-group-no` <chr>, `transect-name` <chr>, `site-name` <chr>,
    ## #   depth <dbl>, ph <dbl>, toc <dbl>, ec <dbl>,
    ## #   `average-soil-relative-humidity` <dbl>,
    ## #   `relative-humidity-soil-high` <dbl>, `relative-humidity-soil-low` <dbl>,
    ## #   `percent-relative-humidity-soil-100` <dbl>, …

Now we want to output the `#SampleID` and `BarcodeSequence` columns to a
new barcode file (without column names)

``` r
atacama_metadata %>% # read in map file, dropping the line that starts with "#q2"
  select(Sample = `sample-id`, 
         BarcodeSequence=`barcode-sequence`) %>%
  write_delim(barcode_table,        # output barcodes to a file 
              delim="\t", 
              col_names=FALSE)
```

## Running fastq-multx

We now have everything we need to run `fastq-multx`. Here is an
explanation for the command line options that we use

-m : number of mismatches to allow in barcode (relative to the barcode
supplied in the barcode table) -x : don’t trim barcodes (this isn’t
necessary) -B BARCODE_FILE : a list of known barcodes, and the
associated sample names -d : minimum edit distance between the best and
next best match BARCODE_FASTQ : the index read FASTQ, which will be used
to demultiplex other reads R1_FASTQ : the R1 raw data to demultiplex
R2_FASTQ : (optional) if data is paired-end, the R2 raw data to
demultiplex

-o OUTPUT_FILE(s) : fastq-multx will produce a separate file for each
barcode (two files when paired-end reads are input, three files when
barcodes are in a separate I1 FASTQ). This option provides a template
for naming the output file - the program will fill in the “%” with the
barcode.

Because of the way `fastq-multx` is designed (to allow demuxing of
FASTQs that have the barcode embeded in the R1 sequence), it will
actually demux the I1 FASTQ. Since we are only interesed in the R1 and
R2, we can ignore the demuxed I1 files.

``` bash
set -u
fastq-multx \
  -m 3 \
  -d 2 \
  -x \
  -B $BARCODE_TABLE \
  $BARCODE_FASTQ \
  $R1_FASTQ \
  $R2_FASTQ \
  -o ${DEMUX_DIR}/'%_I1.fastq.gz' \
  -o ${DEMUX_DIR}/'%.forward.fastq.gz' \
  -o ${DEMUX_DIR}/'%.reverse.fastq.gz' > \
  $DEMUX_STDOUT
```

    ## Using Barcode File: /work/josh/mic2023/demux/barcodes_for_fastqmultx.tsv
    ## End used: start
    ## Skipped because of distance < 2 : 78

``` r
demux_stdout %>%
  read_tsv(file=.,
           skip=1, 
           col_names=c("Id", "Count", "I1_path", "R1_path", "R2_path")) ->
  reads_per_sample
```

    ## Warning: One or more parsing issues, call `problems()` on your data frame for details,
    ## e.g.:
    ##   dat <- vroom(...)
    ##   problems(dat)

    ## Rows: 77 Columns: 5
    ## ── Column specification ────────────────────────────────────────────────────────
    ## Delimiter: "\t"
    ## chr (4): Id, I1_path, R1_path, R2_path
    ## dbl (1): Count
    ## 
    ## ℹ Use `spec()` to retrieve the full column specification for this data.
    ## ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.

``` r
reads_per_sample
```

    ## # A tibble: 77 × 5
    ##    Id          Count I1_path                                     R1_path R2_path
    ##    <chr>       <dbl> <chr>                                       <chr>   <chr>  
    ##  1 BAQ1370.1.2     3 /work/josh/mic2023/demux/BAQ1370.1.2_I1.fa… /work/… /work/…
    ##  2 BAQ1370.3     640 /work/josh/mic2023/demux/BAQ1370.3_I1.fast… /work/… /work/…
    ##  3 BAQ1370.1.3    28 /work/josh/mic2023/demux/BAQ1370.1.3_I1.fa… /work/… /work/…
    ##  4 BAQ1552.1.1     5 /work/josh/mic2023/demux/BAQ1552.1.1_I1.fa… /work/… /work/…
    ##  5 BAQ1552.2      12 /work/josh/mic2023/demux/BAQ1552.2_I1.fast… /work/… /work/…
    ##  6 BAQ2420.1.1     5 /work/josh/mic2023/demux/BAQ2420.1.1_I1.fa… /work/… /work/…
    ##  7 BAQ2420.1.2     1 /work/josh/mic2023/demux/BAQ2420.1.2_I1.fa… /work/… /work/…
    ##  8 BAQ2420.2       8 /work/josh/mic2023/demux/BAQ2420.2_I1.fast… /work/… /work/…
    ##  9 BAQ2420.3       4 /work/josh/mic2023/demux/BAQ2420.3_I1.fast… /work/… /work/…
    ## 10 BAQ2420.1.3    53 /work/josh/mic2023/demux/BAQ2420.1.3_I1.fa… /work/… /work/…
    ## # ℹ 67 more rows

``` r
reads_per_sample %>%
    mutate(groupval = 
           ifelse(str_detect(Id,"unmatched|total"),
                 Id,
                 "sample")) %>%
    group_by(groupval) %>%
    summarise(Count=sum(Count))
```

    ## # A tibble: 3 × 2
    ##   groupval   Count
    ##   <chr>      <dbl>
    ## 1 sample      5603
    ## 2 total     135487
    ## 3 unmatched 129884

## The Unmatched Problem

### Reverse Complement the barcodes

``` r
# clean up the output from the previous demultiplexing step
if (dir_exists(demux_dir)) {dir_delete(demux_dir)}
dir_create(demux_dir)
```

``` r
read_tsv(map_file, comment= "#q2") %>% # read in map file, dropping the line that starts with "#q2"
  select(Sample = `sample-id`, 
         BarcodeSequence=`barcode-sequence`) %>%          # select only the columns with sample ID (renamed to get rid of "#") and the barcode itself
  deframe %>%                          # convert to a named vector (expected input for DNAStringSet constructor)
  DNAStringSet %>%                     # convert to DNAStringSet
  reverseComplement %>%                # reverse complement the barcodes
  as.data.frame %>%                    # convert to dataframe for write_delim
  rownames_to_column %>% 
  write_delim(rc_barcode_table,        # output barcodes to a file 
              delim="\t", 
              col_names=FALSE)
```

    ## Rows: 75 Columns: 21
    ## ── Column specification ────────────────────────────────────────────────────────
    ## Delimiter: "\t"
    ## chr  (6): sample-id, barcode-sequence, extract-group-no, transect-name, site...
    ## dbl (15): elevation, extract-concen, amplicon-concentration, depth, ph, toc,...
    ## 
    ## ℹ Use `spec()` to retrieve the full column specification for this data.
    ## ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.

### Run Demux with RC barcodes

``` bash
set -u
fastq-multx \
  -m 3 \
  -d 2 \
  -x \
  -B $RC_BARCODE_TABLE \
  $BARCODE_FASTQ \
  $R1_FASTQ \
  $R2_FASTQ \
  -o ${DEMUX_DIR}/"%_I1.fastq.gz" \
  -o ${DEMUX_DIR}/"%.forward.fastq.gz" \
  -o ${DEMUX_DIR}/"%.reverse.fastq.gz" > \
  $RC_DEMUX_STDOUT
```

    ## Using Barcode File: /work/josh/mic2023/demux/rc_barcodes_for_fastqmultx.tsv
    ## End used: start
    ## Skipped because of distance < 2 : 205

``` r
rc_demux_stdout %>%
  read_tsv(file=.,
           skip=1, 
           col_names=c("Id", "Count", "I1_path", "R1_path", "R2_path")) ->
    reads_per_sample_rc_barcode
```

    ## Warning: One or more parsing issues, call `problems()` on your data frame for details,
    ## e.g.:
    ##   dat <- vroom(...)
    ##   problems(dat)

    ## Rows: 77 Columns: 5
    ## ── Column specification ────────────────────────────────────────────────────────
    ## Delimiter: "\t"
    ## chr (4): Id, I1_path, R1_path, R2_path
    ## dbl (1): Count
    ## 
    ## ℹ Use `spec()` to retrieve the full column specification for this data.
    ## ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.

``` r
reads_per_sample_rc_barcode
```

    ## # A tibble: 77 × 5
    ##    Id          Count I1_path                                     R1_path R2_path
    ##    <chr>       <dbl> <chr>                                       <chr>   <chr>  
    ##  1 BAQ1370.1.2     2 /work/josh/mic2023/demux/BAQ1370.1.2_I1.fa… /work/… /work/…
    ##  2 BAQ1370.3       2 /work/josh/mic2023/demux/BAQ1370.3_I1.fast… /work/… /work/…
    ##  3 BAQ1370.1.3    10 /work/josh/mic2023/demux/BAQ1370.1.3_I1.fa… /work/… /work/…
    ##  4 BAQ1552.1.1     5 /work/josh/mic2023/demux/BAQ1552.1.1_I1.fa… /work/… /work/…
    ##  5 BAQ1552.2       7 /work/josh/mic2023/demux/BAQ1552.2_I1.fast… /work/… /work/…
    ##  6 BAQ2420.1.1   806 /work/josh/mic2023/demux/BAQ2420.1.1_I1.fa… /work/… /work/…
    ##  7 BAQ2420.1.2   686 /work/josh/mic2023/demux/BAQ2420.1.2_I1.fa… /work/… /work/…
    ##  8 BAQ2420.2     668 /work/josh/mic2023/demux/BAQ2420.2_I1.fast… /work/… /work/…
    ##  9 BAQ2420.3     723 /work/josh/mic2023/demux/BAQ2420.3_I1.fast… /work/… /work/…
    ## 10 BAQ2420.1.3   733 /work/josh/mic2023/demux/BAQ2420.1.3_I1.fa… /work/… /work/…
    ## # ℹ 67 more rows

``` r
reads_per_sample_rc_barcode %>%
    mutate(groupval = 
           ifelse(str_detect(Id,"unmatched|total"),
                 Id,
                 "sample")) %>%
    group_by(groupval) %>%
    summarise(Count=sum(Count))
```

    ## # A tibble: 3 × 2
    ##   groupval   Count
    ##   <chr>      <dbl>
    ## 1 sample     48071
    ## 2 total     135487
    ## 3 unmatched  87416

# Session Info

Always print `sessionInfo` for reproducibility!

``` r
sessionInfo()
```

    ## R version 4.3.0 (2023-04-21)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 22.04.2 LTS
    ## 
    ## Matrix products: default
    ## BLAS:   /usr/lib/x86_64-linux-gnu/openblas-pthread/libblas.so.3 
    ## LAPACK: /usr/lib/x86_64-linux-gnu/openblas-pthread/libopenblasp-r0.3.20.so;  LAPACK version 3.10.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## time zone: Etc/UTC
    ## tzcode source: system (glibc)
    ## 
    ## attached base packages:
    ## [1] tools     stats4    stats     graphics  grDevices utils     datasets 
    ## [8] methods   base     
    ## 
    ## other attached packages:
    ##  [1] here_1.0.1          stringr_1.5.0       Biostrings_2.68.0  
    ##  [4] GenomeInfoDb_1.36.0 XVector_0.40.0      IRanges_2.34.0     
    ##  [7] S4Vectors_0.38.1    BiocGenerics_0.46.0 tibble_3.2.1       
    ## [10] dplyr_1.1.0         fs_1.6.2            readr_2.1.4        
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] bit_4.0.5               compiler_4.3.0          crayon_1.5.2           
    ##  [4] tidyselect_1.2.0        bitops_1.0-7            parallel_4.3.0         
    ##  [7] yaml_2.3.7              fastmap_1.1.1           R6_2.5.1               
    ## [10] generics_0.1.3          knitr_1.42              rprojroot_2.0.3        
    ## [13] GenomeInfoDbData_1.2.10 pillar_1.9.0            tzdb_0.3.0             
    ## [16] rlang_1.1.1             utf8_1.2.3              stringi_1.7.12         
    ## [19] xfun_0.39               bit64_4.0.5             cli_3.6.1              
    ## [22] withr_2.5.0             magrittr_2.0.3          zlibbioc_1.46.0        
    ## [25] digest_0.6.31           vroom_1.6.1             rstudioapi_0.14        
    ## [28] hms_1.1.2               lifecycle_1.0.3         vctrs_0.6.2            
    ## [31] evaluate_0.20           glue_1.6.2              RCurl_1.98-1.10        
    ## [34] fansi_1.0.4             rmarkdown_2.21          pkgconfig_2.0.3        
    ## [37] ellipsis_0.3.2          htmltools_0.5.5
