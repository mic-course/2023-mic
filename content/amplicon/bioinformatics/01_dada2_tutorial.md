Here we walk through version 1.6 of the DADA2 pipeline on a small
multi-sample dataset. Our starting point is a set of Illumina-sequenced
paired-end fastq files that have been split (or “demultiplexed”) by
sample and from which the barcodes/adapters have already been removed.
The end product is an **amplicon sequence variant (ASV) table**, a
higher-resolution analogue of the traditional “OTU table”, which records
the number of times each amplicon sequence variant was observed in each
sample. We also assign taxonomy to the output sequences, and demonstrate
how the data can be imported into the popular
[phyloseq](https://joey711.github.io/phyloseq/) R package for the
analysis of microbiome data.

------------------------------------------------------------------------

# Starting point

This workflow assumes that your sequencing data meets certain criteria:

-   Samples have been demultiplexed, i.e. split into individual
    per-sample fastq files.
-   Non-biological nucleotides have been removed, e.g. primers,
    adapters, linkers, etc.
-   If paired-end sequencing data, the forward and reverse fastq files
    contain reads in matched order.

If these criteria are not true for your data (**are you sure there
aren’t any primers hanging around?**) you need to remedy those issues
before beginning this workflow. See DADA2 FAQ for some recommendations
for common issues.

# Getting ready

## Load Libraries

First we load libraries.

``` r
library(dada2)
```

    ## Loading required package: Rcpp

``` r
library(readr)
library(stringr)
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(tibble)
library(magrittr)
library(phyloseq)
library(ggplot2)
library(fs)
library(tidyr)
```

    ## 
    ## Attaching package: 'tidyr'

    ## The following object is masked from 'package:magrittr':
    ## 
    ##     extract

``` r
library(here)
```

    ## here() starts at /hpc/home/josh/project_repos/mic_course/2023-mic

``` r
library(tools)
```

``` r
"content/config.R" %>%
    here() %>%
    source()
```

We will work with are the Matson amplicon data. We have downloaded this
data from SRA which requires that data have been demultiplexed already.
We also subsampled this dataset to 3% of its original number of records
for the purpose of demonstrating the dada2 pipeline quickly.

If the package successfully loaded and your listed files match those
here, you are ready to go through the DADA2 pipeline.

## Check Data Provenance

``` r
amplicon_subsample_md5_file %>%
  read_delim(delim="  ", col_names = c("true_md5sum", "filepath")) %>%
  mutate(filename=basename(filepath))->
  true_amplicon_subset_md5sums
```

    ## Rows: 84 Columns: 2
    ## ── Column specification ────────────────────────────────────────────────────────
    ## Delimiter: "  "
    ## chr (2): true_md5sum, filepath
    ## 
    ## ℹ Use `spec()` to retrieve the full column specification for this data.
    ## ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.

``` r
amplicon_subsample_dir %>%
  list.files(full.names = TRUE ) %>%
  md5sum %>%
  enframe(name="filepath", value="observed_md5sum") %>%
  mutate(filename=basename(filepath))->
  observed_amplicon_subset_md5sums

full_join(true_amplicon_subset_md5sums, observed_amplicon_subset_md5sums, by="filename") %>%
  mutate(md5sum_match=observed_md5sum==true_md5sum) ->
  compare_md5sums

compare_md5sums %>%
  pull(md5sum_match) %>%
  all ->
  all_md5sums_match

stopifnot(all_md5sums_match)
all_md5sums_match
```

    ## [1] TRUE

## Set Up Paths

We need to set up a “scratch” directory for saving files that we
generate while running dada2, but don’t need to save long term.

``` r
# make directory for output
if (dir_exists(dada_out_dir)) {
  dir_delete(dada_out_dir)
}
dir_create(dada_out_dir)

dada_out_dir # this is the name of our scratch directory
```

    ## [1] "/work/josh/mic2023/amplicon/dada2"

 

## Check Data

``` r
list.files(amplicon_subsample_dir) # matson data
```

    ##  [1] "SRR6000872_1.fastq.gz" "SRR6000872_2.fastq.gz" "SRR6000875_1.fastq.gz"
    ##  [4] "SRR6000875_2.fastq.gz" "SRR6000876_1.fastq.gz" "SRR6000876_2.fastq.gz"
    ##  [7] "SRR6000879_1.fastq.gz" "SRR6000879_2.fastq.gz" "SRR6000880_1.fastq.gz"
    ## [10] "SRR6000880_2.fastq.gz" "SRR6000881_1.fastq.gz" "SRR6000881_2.fastq.gz"
    ## [13] "SRR6000882_1.fastq.gz" "SRR6000882_2.fastq.gz" "SRR6000883_1.fastq.gz"
    ## [16] "SRR6000883_2.fastq.gz" "SRR6000884_1.fastq.gz" "SRR6000884_2.fastq.gz"
    ## [19] "SRR6000885_1.fastq.gz" "SRR6000885_2.fastq.gz" "SRR6000886_1.fastq.gz"
    ## [22] "SRR6000886_2.fastq.gz" "SRR6000887_1.fastq.gz" "SRR6000887_2.fastq.gz"
    ## [25] "SRR6000888_1.fastq.gz" "SRR6000888_2.fastq.gz" "SRR6000889_1.fastq.gz"
    ## [28] "SRR6000889_2.fastq.gz" "SRR6000897_1.fastq.gz" "SRR6000897_2.fastq.gz"
    ## [31] "SRR6000898_1.fastq.gz" "SRR6000898_2.fastq.gz" "SRR6000904_1.fastq.gz"
    ## [34] "SRR6000904_2.fastq.gz" "SRR6000913_1.fastq.gz" "SRR6000913_2.fastq.gz"
    ## [37] "SRR6000914_1.fastq.gz" "SRR6000914_2.fastq.gz" "SRR6000915_1.fastq.gz"
    ## [40] "SRR6000915_2.fastq.gz" "SRR6000916_1.fastq.gz" "SRR6000916_2.fastq.gz"
    ## [43] "SRR6000917_1.fastq.gz" "SRR6000917_2.fastq.gz" "SRR6000918_1.fastq.gz"
    ## [46] "SRR6000918_2.fastq.gz" "SRR6000919_1.fastq.gz" "SRR6000919_2.fastq.gz"
    ## [49] "SRR6000922_1.fastq.gz" "SRR6000922_2.fastq.gz" "SRR6000923_1.fastq.gz"
    ## [52] "SRR6000923_2.fastq.gz" "SRR6000924_1.fastq.gz" "SRR6000924_2.fastq.gz"
    ## [55] "SRR6000925_1.fastq.gz" "SRR6000925_2.fastq.gz" "SRR6000926_1.fastq.gz"
    ## [58] "SRR6000926_2.fastq.gz" "SRR6000927_1.fastq.gz" "SRR6000927_2.fastq.gz"
    ## [61] "SRR6000928_1.fastq.gz" "SRR6000928_2.fastq.gz" "SRR6000929_1.fastq.gz"
    ## [64] "SRR6000929_2.fastq.gz" "SRR6000930_1.fastq.gz" "SRR6000930_2.fastq.gz"
    ## [67] "SRR6000933_1.fastq.gz" "SRR6000933_2.fastq.gz" "SRR6000934_1.fastq.gz"
    ## [70] "SRR6000934_2.fastq.gz" "SRR6000935_1.fastq.gz" "SRR6000935_2.fastq.gz"
    ## [73] "SRR6000936_1.fastq.gz" "SRR6000936_2.fastq.gz" "SRR6000937_1.fastq.gz"
    ## [76] "SRR6000937_2.fastq.gz" "SRR6000938_1.fastq.gz" "SRR6000938_2.fastq.gz"
    ## [79] "SRR6000939_1.fastq.gz" "SRR6000939_2.fastq.gz" "SRR6000941_1.fastq.gz"
    ## [82] "SRR6000941_2.fastq.gz" "SRR6000948_1.fastq.gz" "SRR6000948_2.fastq.gz"

> Note: Sometimes the dataset will include “garbage” files that add no
> value to our analysis. These are often files that represent data that
> couldn’t be demultiplexed. It is a good idea to look at the files in a
> dataset to be sure that none of these files are included in your
> analysis.

# Filter and Trim

## Subset list of forward FASTQs

First we read in the names of the fastq files, and perform some string
manipulation to get lists of the forward and reverse fastq files in
matched order.

For the in-class tutorial session we are using a subset of the Matson
amplicon data. We are using this subset so that analysis will run
quickly during class. The data is subsetted in two ways: 1. We have
randomly subsampled reads from the FASTQ files 2. We are only working
with a subset of the samples

``` r
# Forward and reverse fastq filenames have format: 
# SAMPLENAME_1.fastq.gz
# SAMPLENAME_2.fastq.gz
amplicon_subsample_dir %>%
    list.files(pattern="_1.fastq.gz",
               full.names=TRUE) %>%
    str_subset(pattern="SRR60009") %>% # subset to samples with SRA numbers in the 900s
    sort ->
    fnFs
print(fnFs)
```

    ##  [1] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000904_1.fastq.gz"
    ##  [2] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000913_1.fastq.gz"
    ##  [3] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000914_1.fastq.gz"
    ##  [4] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000915_1.fastq.gz"
    ##  [5] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000916_1.fastq.gz"
    ##  [6] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000917_1.fastq.gz"
    ##  [7] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000918_1.fastq.gz"
    ##  [8] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000919_1.fastq.gz"
    ##  [9] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000922_1.fastq.gz"
    ## [10] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000923_1.fastq.gz"
    ## [11] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000924_1.fastq.gz"
    ## [12] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000925_1.fastq.gz"
    ## [13] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000926_1.fastq.gz"
    ## [14] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000927_1.fastq.gz"
    ## [15] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000928_1.fastq.gz"
    ## [16] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000929_1.fastq.gz"
    ## [17] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000930_1.fastq.gz"
    ## [18] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000933_1.fastq.gz"
    ## [19] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000934_1.fastq.gz"
    ## [20] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000935_1.fastq.gz"
    ## [21] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000936_1.fastq.gz"
    ## [22] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000937_1.fastq.gz"
    ## [23] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000938_1.fastq.gz"
    ## [24] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000939_1.fastq.gz"
    ## [25] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000941_1.fastq.gz"
    ## [26] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000948_1.fastq.gz"

## Subset list of reverse FASTQs

Let’s do the same thing for the “reverse” read FASTQs

``` r
amplicon_subsample_dir %>%
    list.files(pattern="_2.fastq.gz",
               full.names=TRUE) %>%
    str_subset(pattern="SRR60009") %>% # subset to samples with SRA numbers in the 900s
    sort ->
    fnRs
print(fnRs)
```

    ##  [1] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000904_2.fastq.gz"
    ##  [2] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000913_2.fastq.gz"
    ##  [3] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000914_2.fastq.gz"
    ##  [4] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000915_2.fastq.gz"
    ##  [5] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000916_2.fastq.gz"
    ##  [6] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000917_2.fastq.gz"
    ##  [7] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000918_2.fastq.gz"
    ##  [8] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000919_2.fastq.gz"
    ##  [9] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000922_2.fastq.gz"
    ## [10] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000923_2.fastq.gz"
    ## [11] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000924_2.fastq.gz"
    ## [12] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000925_2.fastq.gz"
    ## [13] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000926_2.fastq.gz"
    ## [14] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000927_2.fastq.gz"
    ## [15] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000928_2.fastq.gz"
    ## [16] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000929_2.fastq.gz"
    ## [17] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000930_2.fastq.gz"
    ## [18] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000933_2.fastq.gz"
    ## [19] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000934_2.fastq.gz"
    ## [20] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000935_2.fastq.gz"
    ## [21] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000936_2.fastq.gz"
    ## [22] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000937_2.fastq.gz"
    ## [23] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000938_2.fastq.gz"
    ## [24] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000939_2.fastq.gz"
    ## [25] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000941_2.fastq.gz"
    ## [26] "/hpc/group/mic-2023/subset_data/matson_amplicon/SRR6000948_2.fastq.gz"

## List of Sample Names

``` r
# Extract sample names, assuming filenames have format: SAMPLENAME.X.fastq, where X is reverse or forward

forward_fastq_suffix = "_1.fastq.gz"

fnFs %>% 
    basename %>%
    str_remove(forward_fastq_suffix) ->
    sample_names

print(sample_names)
```

    ##  [1] "SRR6000904" "SRR6000913" "SRR6000914" "SRR6000915" "SRR6000916"
    ##  [6] "SRR6000917" "SRR6000918" "SRR6000919" "SRR6000922" "SRR6000923"
    ## [11] "SRR6000924" "SRR6000925" "SRR6000926" "SRR6000927" "SRR6000928"
    ## [16] "SRR6000929" "SRR6000930" "SRR6000933" "SRR6000934" "SRR6000935"
    ## [21] "SRR6000936" "SRR6000937" "SRR6000938" "SRR6000939" "SRR6000941"
    ## [26] "SRR6000948"

### Double Check Sample Names

and just to be sure, let’s check that we get the same result when we
generate samples names from the list of reverse FASTQs

``` r
reverse_fastq_suffix = "_2.fastq.gz"

fnRs %>% 
    basename %>%
    str_remove(reverse_fastq_suffix) ->
    rev_sample_names

identical(sample_names,rev_sample_names)
```

    ## [1] TRUE

**<span style="color:red">If using this workflow on your own
data:</span>** The string manipulations may have to be modified if using
a different filename format.

 

## Examine quality profiles of forward and reverse reads

### Forward Read QC

We start by visualizing the quality profiles of the forward reads:

``` r
plotQualityProfile(fnFs, aggregate = T) # warning: this will take longer if you ask for more samples
```

![](01_dada2_tutorial_files/figure-markdown_github/see-quality-F-all-1.png)

-   This is a grey-scale heatmap; dark regions correspond to higher
    frequency
-   Green line = mean
-   Orange line = median
-   Dashed orange lines are the 25th and 75th quantiles

The forward reads are decent quality (i.e. the median quality is
typically \> 30 over all sequencing cycles). We generally advise
trimming the last few nucleotides to avoid less well-controlled errors
that can arise there. These quality profiles do not suggest that any
additional trimming is needed, so we will truncate the forward reads at
position 145 (trimming the last 5 nucleotides).

### Reverse Read QC

Now let’s look at quality of reverse reads

``` r
plotQualityProfile(fnRs, aggregate = T)
```

![](01_dada2_tutorial_files/figure-markdown_github/see-quality-R-all-1.png)

The reverse reads are of significantly worse quality, and it drops off a
little at the end, which is common in Illumina sequencing. This isn’t
too worrisome, as DADA2 incorporates quality information into its error
model which makes the algorithm [robust to lower quality
sequences](https://twitter.com/bejcal/status/771010634074820608), but
trimming as the average qualities drop off will improve the algorithm’s
sensitivity to rare sequence variants. Based on these profiles, we will
truncate the reverse reads at position 143 where the quality
distribution dips down. As with the forward reads, the first \~5bp are
somewhat lower quality so we will trim 5bp from the left also.

**<span style="color:red">If using this workflow on your own
data:</span>** **Your reads must still overlap after truncation in order
to merge them later!** The tutorial is using 150bp PE V4 sequence data,
so the forward and reverse reads overlap by about 50bp. When using data
with limited overlap `truncLen` must be large enough to maintain
`20 + biological.length.variation` nucleotides of overlap between them.
When using data that overlap more (e.g. 250 PE V4) trimming can be
completely guided by the quality scores.

Non-overlapping primer sets are supported as well with
`mergePairs(..., justConcatenate=TRUE)` when performing merging.

If we were working with 250bp reads of the V4 amplicon, we would
definitely want to trim at least 5bp at the 3’ end to be sure we remove
potential adapter contamination.

 

## Perform filtering and trimming

Assign the filenames for the filtered fastq.gz files.

``` r
filt_path <- file.path(dada_out_dir, "filtered") # Place filtered files in filtered/ subdirectory
filtFs <- file.path(filt_path, paste0(sample_names, "_F_filt.fastq.gz"))
filtRs <- file.path(filt_path, paste0(sample_names, "_R_filt.fastq.gz"))
```

We’ll use standard filtering parameters: `maxN=0` (DADA2 requires no
Ns), `truncQ=2`, `rm.phix=TRUE` and `maxEE=2`. The `maxEE` parameter
sets the maximum number of “expected errors” allowed in a read, which is
[a better filter than simply averaging quality
scores](http://www.drive5.com/usearch/manual/expected_errors.html).

### Filter the forward and reverse reads

Filtering (e.g. removing a read because it has overall bad quality) must
be done in such a way that forward and reverse reads are kept in sync:
if a reverse read is filtered out because of low quality, its partner
forward read must *also* be removed, even if it passes. `filterAndTrim`
does this if you pass it the forward and reverse FASTQs.

The first \~5bp of R1 and R2 are somewhat lower quality, which is very
common for Illumina data. Let’s trim this with `trimLeft=5` (note: this
will result in shorter amplicons, where trimming on right end of a read
should not change amplicon length after it is filtered).

``` r
filt_out <- filterAndTrim(fnFs, filtFs, fnRs, filtRs, trimLeft=5, truncLen=c(145,143),
              maxN=0, maxEE=c(2,2), truncQ=2, rm.phix=TRUE,
              compress=TRUE, multithread=num_cpus) # On Windows set multithread=FALSE
```

    ## Creating output directory: /work/josh/mic2023/amplicon/dada2/filtered

``` r
# finished in ~ 1 min (longest step after assignTaxonomy)
```

Make sure to follow-up on any warning messages:

The output of `filterAndTrim` has a table summary so we can quickly
check the number of reads in and out for each sample like this.

``` r
filt_out %>%
  data.frame()%>%
  arrange(reads.out) %>%
  head(n = 10)
```

    ##                       reads.in reads.out
    ## SRR6000935_1.fastq.gz      421       404
    ## SRR6000923_1.fastq.gz      462       441
    ## SRR6000927_1.fastq.gz      540       517
    ## SRR6000936_1.fastq.gz      584       556
    ## SRR6000917_1.fastq.gz      622       599
    ## SRR6000915_1.fastq.gz      643       612
    ## SRR6000939_1.fastq.gz      665       623
    ## SRR6000933_1.fastq.gz      663       627
    ## SRR6000948_1.fastq.gz      670       639
    ## SRR6000937_1.fastq.gz      698       674

**<span style="color:red">If using this workflow on your own
data:</span>** The standard filtering parameters are starting points,
not set in stone. For example, if too few reads are passing the filter,
considering relaxing `maxEE`, perhaps especially on the reverse reads
(eg. `maxEE=c(2,5)`). If you want to speed up downstream computation,
consider tightening `maxEE`. For paired-end reads consider the length of
your amplicon when choosing `truncLen` as your reads must overlap after
truncation in order to merge them later.

**<span style="color:red">If using this workflow on your own
data:</span>** For common ITS amplicon strategies, it is undesirable to
truncate reads to a fixed length due to the large amount of length
variation at that locus. That is OK, just leave out `truncLen`. Make
sure you removed the forward and reverse primers from both the forward
and reverse reads though!

 

# Learn the Error Rates

The DADA2 algorithm depends on a parametric error model (`err`) and
every amplicon dataset has a different set of error rates. The
`learnErrors` method learns the error model from the data, by
alternating estimation of the error rates and inference of sample
composition until they converge on a jointly consistent solution. As in
many optimization problems, the algorithm must begin with an initial
guess, for which the maximum possible error rates in this data are used
(the error rates if only the most abundant sequence is correct and all
the rest are errors).

An important note: If some samples ended up with zero reads after
`filterAndTrim`, the resulting fastq will not exist because the function
won’t’t create empty filtered FASTQs. This can cause errors downstream
if you try to feed `learnErrors` the name of a file that does not exist.
Here is a quick way to check that we have all the files we expect:

``` r
filtFs %>%
  file_exists %>%
  all
```

    ## [1] TRUE

If there are “missing” filtered FASTQs, those were probably ones with
zero reads after filtering.  
Here is how we can subset the vectors of filtered FASTQs to remove any
non-existant files.

``` r
filtFs = filtFs[file_exists(filtFs)]
filtRs = filtRs[file_exists(filtRs)]
```

Now let’s learn errors from these.

``` r
errF <- learnErrors(filtFs, multithread=num_cpus) 
```

    ## 2536800 total bases in 18120 reads from 26 samples will be used for learning the error rates.

``` r
errR <- learnErrors(filtRs, multithread=num_cpus)
```

    ## 2500560 total bases in 18120 reads from 26 samples will be used for learning the error rates.

It is always worthwhile, as a reality check if nothing else, to
visualize the estimated error rates:

``` r
plotErrors(errF, nominalQ=TRUE)
```

    ## Warning: Transformation introduced infinite values in continuous y-axis
    ## Transformation introduced infinite values in continuous y-axis

![](01_dada2_tutorial_files/figure-markdown_github/plot-errors-1.png)

The error rates for each possible transition (eg. A-\>C, A-\>G, …) are
shown. In general, you expect the frequency of sequencing error to
decrease where you have higher quality scores.

Points are the observed error rates for each consensus quality score.
The red line shows the error rates expected under the nominal definition
of the Q-value **Is the Q-value introduced in the lecture?**. The black
line shows the estimated error rates after convergence of the model we
just fit using learnErrors(). Here the black line (the estimated rates)
fits the observed rates well, and the error rates drop with increased
quality as expected. Everything looks reasonable and we proceed with
confidence.

**<span style="color:red">If using this workflow on your own
data:</span>** Parameter learning is computationally intensive, so by
default the `learnErrors` function uses only a subset of the data (the
first 1M reads). If the plotted error model does not look like a good
fit, try increasing the `nreads` parameter to see if the fit improves.

 

# Dereplication

Dereplication combines all identical sequencing reads into into “unique
sequences” with a corresponding “abundance”: the number of reads with
that unique sequence. Dereplication substantially reduces computation
time by eliminating redundant comparisons.

Dereplication in the DADA2 pipeline has one crucial addition from other
pipelines: **DADA2 retains a summary of the quality information
associated with each unique sequence**. The consensus quality profile of
a unique sequence is the average of the positional qualities from the
dereplicated reads. These quality profiles inform the error model of the
subsequent denoising step, significantly increasing DADA2’s accuracy.

**Dereplicate the filtered fastq files** An important note: Remember
that we generated our “sample_names” from the original list of FASTQs.
Just as above, it is possible that some samples dropped out during
filtering, so it is safest to re-generate our vector of sample names to
be sure we exclude names of samples that have dropped out. We can
regenerate the sample list based on the list of filtered FASTQs.

``` r
filtFs %>% 
  basename %>%
  str_replace("_F_filt.fastq.gz","") ->
  sample_names
```

Now let’s dereplicate the filtered FASTQs

``` r
derepFs <- derepFastq(filtFs, verbose=TRUE)
```

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000904_F_filt.fastq.gz

    ## Encountered 425 unique sequences from 806 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000913_F_filt.fastq.gz

    ## Encountered 366 unique sequences from 723 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000914_F_filt.fastq.gz

    ## Encountered 471 unique sequences from 890 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000915_F_filt.fastq.gz

    ## Encountered 302 unique sequences from 612 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000916_F_filt.fastq.gz

    ## Encountered 412 unique sequences from 899 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000917_F_filt.fastq.gz

    ## Encountered 300 unique sequences from 599 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000918_F_filt.fastq.gz

    ## Encountered 364 unique sequences from 739 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000919_F_filt.fastq.gz

    ## Encountered 352 unique sequences from 715 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000922_F_filt.fastq.gz

    ## Encountered 437 unique sequences from 818 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000923_F_filt.fastq.gz

    ## Encountered 208 unique sequences from 441 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000924_F_filt.fastq.gz

    ## Encountered 420 unique sequences from 815 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000925_F_filt.fastq.gz

    ## Encountered 482 unique sequences from 858 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000926_F_filt.fastq.gz

    ## Encountered 346 unique sequences from 711 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000927_F_filt.fastq.gz

    ## Encountered 247 unique sequences from 517 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000928_F_filt.fastq.gz

    ## Encountered 348 unique sequences from 754 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000929_F_filt.fastq.gz

    ## Encountered 311 unique sequences from 691 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000930_F_filt.fastq.gz

    ## Encountered 376 unique sequences from 743 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000933_F_filt.fastq.gz

    ## Encountered 338 unique sequences from 627 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000934_F_filt.fastq.gz

    ## Encountered 362 unique sequences from 790 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000935_F_filt.fastq.gz

    ## Encountered 195 unique sequences from 404 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000936_F_filt.fastq.gz

    ## Encountered 232 unique sequences from 556 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000937_F_filt.fastq.gz

    ## Encountered 395 unique sequences from 674 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000938_F_filt.fastq.gz

    ## Encountered 447 unique sequences from 795 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000939_F_filt.fastq.gz

    ## Encountered 316 unique sequences from 623 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000941_F_filt.fastq.gz

    ## Encountered 376 unique sequences from 681 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000948_F_filt.fastq.gz

    ## Encountered 331 unique sequences from 639 total sequences read.

``` r
derepRs <- derepFastq(filtRs, verbose=TRUE)
```

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000904_R_filt.fastq.gz

    ## Encountered 494 unique sequences from 806 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000913_R_filt.fastq.gz

    ## Encountered 404 unique sequences from 723 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000914_R_filt.fastq.gz

    ## Encountered 523 unique sequences from 890 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000915_R_filt.fastq.gz

    ## Encountered 340 unique sequences from 612 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000916_R_filt.fastq.gz

    ## Encountered 554 unique sequences from 899 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000917_R_filt.fastq.gz

    ## Encountered 342 unique sequences from 599 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000918_R_filt.fastq.gz

    ## Encountered 399 unique sequences from 739 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000919_R_filt.fastq.gz

    ## Encountered 399 unique sequences from 715 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000922_R_filt.fastq.gz

    ## Encountered 495 unique sequences from 818 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000923_R_filt.fastq.gz

    ## Encountered 242 unique sequences from 441 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000924_R_filt.fastq.gz

    ## Encountered 443 unique sequences from 815 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000925_R_filt.fastq.gz

    ## Encountered 493 unique sequences from 858 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000926_R_filt.fastq.gz

    ## Encountered 383 unique sequences from 711 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000927_R_filt.fastq.gz

    ## Encountered 276 unique sequences from 517 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000928_R_filt.fastq.gz

    ## Encountered 402 unique sequences from 754 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000929_R_filt.fastq.gz

    ## Encountered 330 unique sequences from 691 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000930_R_filt.fastq.gz

    ## Encountered 430 unique sequences from 743 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000933_R_filt.fastq.gz

    ## Encountered 393 unique sequences from 627 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000934_R_filt.fastq.gz

    ## Encountered 434 unique sequences from 790 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000935_R_filt.fastq.gz

    ## Encountered 229 unique sequences from 404 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000936_R_filt.fastq.gz

    ## Encountered 288 unique sequences from 556 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000937_R_filt.fastq.gz

    ## Encountered 423 unique sequences from 674 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000938_R_filt.fastq.gz

    ## Encountered 488 unique sequences from 795 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000939_R_filt.fastq.gz

    ## Encountered 339 unique sequences from 623 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000941_R_filt.fastq.gz

    ## Encountered 384 unique sequences from 681 total sequences read.

    ## Dereplicating sequence entries in Fastq file: /work/josh/mic2023/amplicon/dada2/filtered/SRR6000948_R_filt.fastq.gz

    ## Encountered 375 unique sequences from 639 total sequences read.

``` r
# Name the derep-class objects by the sample names
names(derepFs) <- sample_names
names(derepRs) <- sample_names
```

**<span style="color:red">If using this workflow on your own
data:</span>** The tutorial dataset is small enough to easily load into
memory. If your dataset exceeds available RAM, it is preferable to
process samples one-by-one in a streaming fashion: see the [DADA2
Workflow on Big Data](bigdata.html) for an example.

 

# Sample Inference

We are now ready to apply the core sequence-variant inference algorithm
to the dereplicated data.

**Infer the sequence variants in each sample**

``` r
dadaFs <- dada(derepFs, err=errF, multithread=num_cpus)
```

    ## Sample 1 - 806 reads in 425 unique sequences.
    ## Sample 2 - 723 reads in 366 unique sequences.
    ## Sample 3 - 890 reads in 471 unique sequences.
    ## Sample 4 - 612 reads in 302 unique sequences.
    ## Sample 5 - 899 reads in 412 unique sequences.
    ## Sample 6 - 599 reads in 300 unique sequences.
    ## Sample 7 - 739 reads in 364 unique sequences.
    ## Sample 8 - 715 reads in 352 unique sequences.
    ## Sample 9 - 818 reads in 437 unique sequences.
    ## Sample 10 - 441 reads in 208 unique sequences.
    ## Sample 11 - 815 reads in 420 unique sequences.
    ## Sample 12 - 858 reads in 482 unique sequences.
    ## Sample 13 - 711 reads in 346 unique sequences.
    ## Sample 14 - 517 reads in 247 unique sequences.
    ## Sample 15 - 754 reads in 348 unique sequences.
    ## Sample 16 - 691 reads in 311 unique sequences.
    ## Sample 17 - 743 reads in 376 unique sequences.
    ## Sample 18 - 627 reads in 338 unique sequences.
    ## Sample 19 - 790 reads in 362 unique sequences.
    ## Sample 20 - 404 reads in 195 unique sequences.
    ## Sample 21 - 556 reads in 232 unique sequences.
    ## Sample 22 - 674 reads in 395 unique sequences.
    ## Sample 23 - 795 reads in 447 unique sequences.
    ## Sample 24 - 623 reads in 316 unique sequences.
    ## Sample 25 - 681 reads in 376 unique sequences.
    ## Sample 26 - 639 reads in 331 unique sequences.

``` r
dadaRs <- dada(derepRs, err=errR, multithread=num_cpus)
```

    ## Sample 1 - 806 reads in 494 unique sequences.
    ## Sample 2 - 723 reads in 404 unique sequences.
    ## Sample 3 - 890 reads in 523 unique sequences.
    ## Sample 4 - 612 reads in 340 unique sequences.
    ## Sample 5 - 899 reads in 554 unique sequences.
    ## Sample 6 - 599 reads in 342 unique sequences.
    ## Sample 7 - 739 reads in 399 unique sequences.
    ## Sample 8 - 715 reads in 399 unique sequences.
    ## Sample 9 - 818 reads in 495 unique sequences.
    ## Sample 10 - 441 reads in 242 unique sequences.
    ## Sample 11 - 815 reads in 443 unique sequences.
    ## Sample 12 - 858 reads in 493 unique sequences.
    ## Sample 13 - 711 reads in 383 unique sequences.
    ## Sample 14 - 517 reads in 276 unique sequences.
    ## Sample 15 - 754 reads in 402 unique sequences.
    ## Sample 16 - 691 reads in 330 unique sequences.
    ## Sample 17 - 743 reads in 430 unique sequences.
    ## Sample 18 - 627 reads in 393 unique sequences.
    ## Sample 19 - 790 reads in 434 unique sequences.
    ## Sample 20 - 404 reads in 229 unique sequences.
    ## Sample 21 - 556 reads in 288 unique sequences.
    ## Sample 22 - 674 reads in 423 unique sequences.
    ## Sample 23 - 795 reads in 488 unique sequences.
    ## Sample 24 - 623 reads in 339 unique sequences.
    ## Sample 25 - 681 reads in 384 unique sequences.
    ## Sample 26 - 639 reads in 375 unique sequences.

Inspecting the dada-class object returned by dada:

``` r
dadaFs[[1]]
```

    ## dada-class: object describing DADA2 denoising results
    ## 36 sequence variants were inferred from 425 input unique sequences.
    ## Key parameters: OMEGA_A = 1e-40, OMEGA_C = 1e-40, BAND_SIZE = 16

The DADA2 algorithm inferred 36 real sequence variants from the 425
unique sequences in the first sample. There is much more to the
`dada-class` return object than this (see `help("dada-class")` for some
info), including multiple diagnostics about the quality of each inferred
sequence variant, but that is beyond the scope of an introductory
tutorial.

**<span style="color:red">If using this workflow on your own
data:</span>** All samples are simultaneously loaded into memory in the
tutorial. If you are dealing with datasets that approach or exceed
available RAM, it is preferable to process samples one-by-one in a
streaming fashion: see the **[DADA2 Workflow on Big
Data](bigdata.html)** for an example.

**<span style="color:red">If using this workflow on your own
data:</span>** By default, the `dada` function processes each sample
independently, but pooled processing is available with `pool=TRUE` and
that may give better results for low sampling depths at the cost of
increased computation time. See our [discussion about pooling samples
for sample inference](pool.html).

**<span style="color:red">If using this workflow on your own
data:</span>** DADA2 also supports 454 and Ion Torrent data, but [we
recommend some minor parameter
changes](faq.html#can-i-use-dada2-with-my-454-or-ion-torrent-data) for
those sequencing technologies. The adventurous can explore `?setDadaOpt`
for other adjustable algorithm parameters.

 

# Merge paired reads

Spurious sequence variants are further reduced by merging overlapping
reads. The core function here is `mergePairs`, which depends on the
forward and reverse reads being in matching order at the time they were
dereplicated.

**Merge the denoised forward and reverse reads**:

``` r
mergers <- mergePairs(dadaFs, derepFs, dadaRs, derepRs, verbose=TRUE)
```

    ## 571 paired-reads (in 20 unique pairings) successfully merged out of 726 (in 69 pairings) input.

    ## 614 paired-reads (in 24 unique pairings) successfully merged out of 665 (in 46 pairings) input.

    ## 620 paired-reads (in 26 unique pairings) successfully merged out of 799 (in 87 pairings) input.

    ## 465 paired-reads (in 16 unique pairings) successfully merged out of 543 (in 39 pairings) input.

    ## 652 paired-reads (in 15 unique pairings) successfully merged out of 767 (in 44 pairings) input.

    ## 458 paired-reads (in 18 unique pairings) successfully merged out of 535 (in 50 pairings) input.

    ## 594 paired-reads (in 15 unique pairings) successfully merged out of 670 (in 38 pairings) input.

    ## 606 paired-reads (in 20 unique pairings) successfully merged out of 681 (in 50 pairings) input.

    ## 626 paired-reads (in 21 unique pairings) successfully merged out of 723 (in 60 pairings) input.

    ## 357 paired-reads (in 9 unique pairings) successfully merged out of 378 (in 20 pairings) input.

    ## 714 paired-reads (in 30 unique pairings) successfully merged out of 768 (in 66 pairings) input.

    ## 659 paired-reads (in 28 unique pairings) successfully merged out of 768 (in 60 pairings) input.

    ## 567 paired-reads (in 21 unique pairings) successfully merged out of 642 (in 53 pairings) input.

    ## 362 paired-reads (in 15 unique pairings) successfully merged out of 457 (in 44 pairings) input.

    ## 687 paired-reads (in 10 unique pairings) successfully merged out of 715 (in 24 pairings) input.

    ## 608 paired-reads (in 7 unique pairings) successfully merged out of 658 (in 19 pairings) input.

    ## 540 paired-reads (in 25 unique pairings) successfully merged out of 655 (in 58 pairings) input.

    ## 416 paired-reads (in 14 unique pairings) successfully merged out of 561 (in 40 pairings) input.

    ## 712 paired-reads (in 20 unique pairings) successfully merged out of 755 (in 44 pairings) input.

    ## 300 paired-reads (in 7 unique pairings) successfully merged out of 358 (in 14 pairings) input.

    ## 491 paired-reads (in 12 unique pairings) successfully merged out of 528 (in 26 pairings) input.

    ## 456 paired-reads (in 30 unique pairings) successfully merged out of 595 (in 77 pairings) input.

    ## 534 paired-reads (in 18 unique pairings) successfully merged out of 698 (in 63 pairings) input.

    ## 524 paired-reads (in 23 unique pairings) successfully merged out of 574 (in 49 pairings) input.

    ## 500 paired-reads (in 22 unique pairings) successfully merged out of 619 (in 59 pairings) input.

    ## 475 paired-reads (in 24 unique pairings) successfully merged out of 592 (in 69 pairings) input.

We now have a `data.frame` for each sample with the merged `$sequence`,
its `$abundance`, and the indices of the merged `$forward` and
`$reverse` denoised sequences. Paired reads that did not exactly overlap
were removed by `mergePairs`.

**<span style="color:red">If using this workflow on your own
data:</span>** Most of your **reads** should successfully merge. If that
is not the case upstream parameters may need to be revisited: Did you
trim away the overlap between your reads?

 

# Construct sequence table

We can now construct a sequence table of our mouse samples, a
higher-resolution version of the OTU table produced by traditional
methods.

``` r
seqtab <- makeSequenceTable(mergers)
dim(seqtab)
```

    ## [1]  26 206

``` r
# Inspect distribution of sequence lengths
table(nchar(getSequences(seqtab)))
```

    ## 
    ## 242 243 244 
    ##  38 166   2

The sequence table is a `matrix` with rows corresponding to (and named
by) the samples, and columns corresponding to (and named by) the
sequence variants. The lengths of our merged sequences all fall within
the expected range for this V4 amplicon.

**<span style="color:red">If using this workflow on your own
data:</span>** Sequences that are much longer or shorter than expected
may be the result of non-specific priming, and may be worth removing
(eg. `seqtab2 <- seqtab[,nchar(colnames(seqtab)) %in% seq(250,256)]`).
This is analogous to “cutting a band” in-silico to get amplicons of the
targeted length.

 

# Remove chimeras

The core `dada` method removes substitution and indel errors, but
chimeras remain. Fortunately, the accuracy of the sequences after
denoising makes identifying chimeras simpler than it is when dealing
with fuzzy OTUs: all sequences which can be exactly reconstructed as a
bimera (two-parent chimera) from more abundant sequences.

**Remove chimeric sequences**:

``` r
seqtab_nochim <- removeBimeraDenovo(seqtab, method="consensus", multithread=num_cpus, verbose=TRUE)
```

    ## Identified 1 bimeras out of 206 input sequences.

``` r
dim(seqtab_nochim)
```

    ## [1]  26 205

``` r
sum(seqtab_nochim)/sum(seqtab)
```

    ## [1] 0.9977318

The fraction of chimeras varies based on factors including experimental
procedures and sample complexity, but can be substantial. Here chimeras
make up about 0% of the inferred sequence variants, but those variants
account for only about 0% of the total sequence reads.

**<span style="color:red">If using this workflow on your own
data:</span>** Most of your **reads** should remain after chimera
removal (it is not uncommon for a majority of **sequence variants** to
be removed though). If most of your reads were removed as chimeric,
upstream processing may need to be revisited. In almost all cases this
is caused by primer sequences with ambiguous nucleotides that were not
removed prior to beginning the DADA2 pipeline.

 

# Track reads through the pipeline

As a final check of our progress, we’ll look at the number of reads that
made it through each step in the pipeline:

## Collect reads per sample remaining after each step

``` r
getN <- function(x) sum(getUniques(x))
filt_out %>%
  as_tibble(rownames = "filename") %>%
  mutate(sample=str_replace(filename, forward_fastq_suffix,"")) %>%
  select(sample, input=reads.in, filtered=reads.out) ->
  track

sapply(dadaFs, getN) %>%
  enframe(name="sample", value="denoised") ->
  denoised
track %<>% full_join(denoised, by=c("sample"))

sapply(mergers, getN) %>%
  enframe(name="sample", value="merged") ->
  merged
track %<>% full_join(merged, by=c("sample"))

rowSums(seqtab) %>%
  enframe(name="sample", value="tabled") ->
  tabled
track %<>% full_join(tabled, by=c("sample"))

rowSums(seqtab_nochim) %>%
  enframe(name="sample", value="nonchim") ->
  nonchim
track %<>% full_join(nonchim, by=c("sample"))

track
```

    ## # A tibble: 26 × 7
    ##    sample     input filtered denoised merged tabled nonchim
    ##    <chr>      <dbl>    <dbl>    <int>  <int>  <dbl>   <dbl>
    ##  1 SRR6000904   844      806      765    571    571     571
    ##  2 SRR6000913   748      723      674    614    614     614
    ##  3 SRR6000914   921      890      829    620    620     620
    ##  4 SRR6000915   643      612      554    465    465     465
    ##  5 SRR6000916   943      899      879    652    652     652
    ##  6 SRR6000917   622      599      561    458    458     458
    ##  7 SRR6000918   775      739      687    594    594     594
    ##  8 SRR6000919   758      715      688    606    606     606
    ##  9 SRR6000922   857      818      741    626    626     626
    ## 10 SRR6000923   462      441      409    357    357     357
    ## # ℹ 16 more rows

## Plot reads counts through pipeline

``` r
track %>%
    gather(key="stage", value="counts", -c("sample")) %>%
    replace_na(list(counts = 0)) %>%
    mutate(stage=factor(stage, levels = c('input','filtered','denoised','merged','tabled','nonchim'))) %>%
    ggplot(mapping=aes(x=stage, y=counts, by=sample, group = sample)) +
    geom_line(alpha=0.5) +
        theme_classic()
```

![](01_dada2_tutorial_files/figure-markdown_github/unnamed-chunk-10-1.png)

**<span style="color:red">If using this workflow on your own
data:</span>** This is a great place to do a last **sanity check**.
Outside of filtering (depending on how stringent you want to be) there
should no step in which a majority of reads are lost. If a majority of
reads failed to merge, you may need to revisit the `truncLen` parameter
used in the filtering step and make sure that the truncated reads span
your amplicon. If a majority of reads failed to pass the chimera check,
you may need to revisit the removal of primers, as the ambiguous
nucleotides in unremoved primers interfere with chimera identification.

 

# Assign taxonomy

It is common at this point, especially in 16S/18S/ITS amplicon
sequencing, to classify sequence variants taxonomically. The DADA2
package provides a native implementation of [the RDP’s naive Bayesian
classifier](http://www.ncbi.nlm.nih.gov/pubmed/17586664) for this
purpose. The `assignTaxonomy` function takes a set of sequences and a
training set of taxonomically classified sequences, and outputs the
taxonomic assignments with at least `minBoot` bootstrap confidence.

The DADA2 developers maintain [formatted training fastas for the RDP
training set, GreenGenes clustered at 97% identity, and the Silva
reference database](training.html). For fungal taxonomy, the General
Fasta release files from the [UNITE ITS
database](https://unite.ut.ee/repository.php) can be used as is.

We have downloaded a shared MIC Course copy of of the DADA2 formated
Silva database and assigned its path to the `silva_ref` variable in the
[Set Up Paths](#set-up-paths) section above, so you don’t need to
download it!

> This step can be memory intensive, be sure you have allocated enough
> RAM

``` r
taxa <- assignTaxonomy(seqtab_nochim, silva_ref, multithread=num_cpus)
```

**Optional:** The dada2 package also implements a method to make
[species level assignments based on **exact
matching**](assign.html#species-assignment) between ASVs and sequenced
reference strains. Currently species-assignment training fastas are
available for the Silva and RDP 16S databases.

``` r
taxa <- addSpecies(taxa, silva_species_ref)
```

Let’s inspect the taxonomic assignments:

``` r
taxa_print <- taxa # Removing sequence rownames for display only
rownames(taxa_print) <- NULL
head(taxa_print)
```

    ##      Kingdom    Phylum            Class                 Order               
    ## [1,] "Bacteria" "Bacteroidetes"   "Bacteroidia"         "Bacteroidales"     
    ## [2,] "Bacteria" "Bacteroidetes"   "Bacteroidia"         "Bacteroidales"     
    ## [3,] "Bacteria" "Bacteroidetes"   "Bacteroidia"         "Bacteroidales"     
    ## [4,] "Bacteria" "Verrucomicrobia" "Verrucomicrobiae"    "Verrucomicrobiales"
    ## [5,] "Bacteria" "Proteobacteria"  "Gammaproteobacteria" "Enterobacteriales" 
    ## [6,] "Bacteria" "Firmicutes"      "Clostridia"          "Clostridiales"     
    ##      Family               Genus                  Species      
    ## [1,] "Bacteroidaceae"     "Bacteroides"          "vulgatus"   
    ## [2,] "Bacteroidaceae"     "Bacteroides"          NA           
    ## [3,] "Bacteroidaceae"     "Bacteroides"          "uniformis"  
    ## [4,] "Akkermansiaceae"    "Akkermansia"          "muciniphila"
    ## [5,] "Enterobacteriaceae" "Escherichia/Shigella" NA           
    ## [6,] "Ruminococcaceae"    "Faecalibacterium"     NA

**<span style="color:red">If using this workflow on your own
data:</span>** If your reads do not seem to be appropriately assigned,
for example lots of your bacterial 16S sequences are being assigned as
`Eukaryota NA NA NA NA NA`, your reads may be in the opposite
orientation as the reference database. Tell dada2 to try the
reverse-complement orientation with `assignTaxonomy(..., tryRC=TRUE)`
and see if this fixes the assignments.

 

------------------------------------------------------------------------

# Bonus: Handoff to phyloseq

The [phyloseq R package is a powerful framework for further analysis of
microbiome data](https://joey711.github.io/phyloseq/). We now
demonstrate how to straightforwardly import the tables produced by the
DADA2 pipeline into phyloseq. We’ll also add the sample metadata.

## Make Phyloseq Object

### Prepare Metadata Table

We can construct a simple sample data.frame based on the filenames.
Usually this step would instead involve reading the sample data in from
a file.

``` r
md <- read_csv(matson_sample_csv)
```

    ## Rows: 81 Columns: 18
    ## ── Column specification ────────────────────────────────────────────────────────
    ## Delimiter: ","
    ## chr (17): SampleID, Study, PatientID, Cancer, Timept, DataType, OriginalResp...
    ## dbl  (1): Total Clean Seqs for Analysis (16S) or Raw WGS Fragments
    ## 
    ## ℹ Use `spec()` to retrieve the full column specification for this data.
    ## ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.

``` r
head(md)
```

    ## # A tibble: 6 × 18
    ##   SampleID   Study  PatientID Cancer  Timept DataType OriginalResponse `R or NR`
    ##   <chr>      <chr>  <chr>     <chr>   <chr>  <chr>    <chr>            <chr>    
    ## 1 SRR6000938 Matson P01       Melano… T0     16SrRNA  NR               NR       
    ## 2 SRR6000937 Matson P02       Melano… T0     16SrRNA  R                R        
    ## 3 SRR6000948 Matson P03       Melano… T0     16SrRNA  NR               NR       
    ## 4 SRR6000941 Matson P04       Melano… T0     16SrRNA  NR               NR       
    ## 5 SRR6000935 Matson P05       Melano… T0     16SrRNA  NR               NR       
    ## 6 SRR6000934 Matson P06       Melano… T0     16SrRNA  NR               NR       
    ## # ℹ 10 more variables:
    ## #   `Total Clean Seqs for Analysis (16S) or Raw WGS Fragments` <dbl>,
    ## #   Age <chr>, Sex <chr>, AbxExposure <chr>, PFS3Mo <chr>, PFS6Mo <chr>,
    ## #   PFSMo <chr>, Cohort <chr>,
    ## #   `Use To Evaluate Original Publication Reported Result R vs NR used for confirmation of original bacterial signals` <chr>,
    ## #   `Use for Primary Meta-analysis of R vs NR used to derive composite index` <chr>

``` r
nrow(md) # 81 sequence "runs" in the metadata table
```

    ## [1] 81

``` r
md %>%
  filter(DataType=="16SrRNA") %>%
  nrow # 42 of these are amplicon data
```

    ## [1] 42

``` r
nrow(seqtab_nochim) # we subsetted to 26 samples
```

    ## [1] 26

``` r
md %>%
  filter(SampleID %in% rownames(seqtab_nochim)) %>% # subset metadata table to only contain samples we analyzed
  column_to_rownames("SampleID") %>% # phyloseq wants a data frame with sample ids as rownames
  as.data.frame() -> meta_df
nrow(meta_df) # confirm that final metadata table numbers are as expected
```

    ## [1] 26

### Prepare Taxonomy Table

``` r
# Taxonomy table -- each row corresponds to an ASV in our count matrix
# create a shortname for each unique ASV so we don't have to use the unique ASV sequence
asv_id <- paste0("ASV_", seq(1:nrow(taxa)))
asv_sequences <- row.names(taxa)
taxa_reorg <- cbind(taxa, asv_id, asv_sequences)
colnames(taxa_reorg)
```

    ## [1] "Kingdom"       "Phylum"        "Class"         "Order"        
    ## [5] "Family"        "Genus"         "Species"       "asv_id"       
    ## [9] "asv_sequences"

``` r
row.names(taxa_reorg) <- asv_id 
tax <- tax_table(taxa_reorg)
```

### Prepare ASV Table

``` r
# Count matrix -- these are really ASVs but in phyloseq-terminology we are going to store these in an "otu_table"
rownames(seqtab_nochim) # samples are rows
```

    ##  [1] "SRR6000904" "SRR6000913" "SRR6000914" "SRR6000915" "SRR6000916"
    ##  [6] "SRR6000917" "SRR6000918" "SRR6000919" "SRR6000922" "SRR6000923"
    ## [11] "SRR6000924" "SRR6000925" "SRR6000926" "SRR6000927" "SRR6000928"
    ## [16] "SRR6000929" "SRR6000930" "SRR6000933" "SRR6000934" "SRR6000935"
    ## [21] "SRR6000936" "SRR6000937" "SRR6000938" "SRR6000939" "SRR6000941"
    ## [26] "SRR6000948"

``` r
colnames(seqtab_nochim) # columns are asv_sequences
```

    ##   [1] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGATGGATGTTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGATATCTTGAGTGCAGTTGAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCCTGCTAAGCTGCAACTGACATTGAGGCTCGAAAGTGTGGGTATCAA" 
    ##   [2] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGATGGATGTTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGATGTCTTGAGTGCAGTTGAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCCTGCTAAGCTGCAACTGACATTGAGGCTCGAAAGTGTGGGTATCAA" 
    ##   [3] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGACGCTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGGTGTCTTGAGTACAGTAGAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCCTGCTGGACTGTAACTGACGCTGATGCTCGAAAGTGTGGGTATCAA" 
    ##   [4] "AGGTCTCAAGCGTTGTTCGGAATCACTGGGCGTAAAGCGTGCGTAGGCGGTTTCGTAAGTCGTGTGTGAAAGGCGGGGGCTCAACCCCCGGACTGCACATGATACTGCGAGACTAGAGTAATGGAGGGGGAACCGGAATTCTCGGTGTAGCAGTGAAATGCGTAGATATCGAGAGGAACACTCGTGGCGAAGGCGGGTTCCTGGACATTAACTGACGCTGAGGCACGAAGGCCAGGGGAGCGA" 
    ##   [5] "AGGGTGCAAGCGTTAATCGGAATTACTGGGCGTAAAGCGCACGCAGGCGGTTTGTTAAGTCAGATGTGAAATCCCCGGGCTCAACCTGGGAACTGCATCTGATACTGGCAAGCTTGAGTCTCGTAGAGGGGGGTAGAATTCCAGGTGTAGCGGTGAAATGCGTAGAGATCTGGAGGAATACCGGTGGCGAAGGCGGCCCCCTGGACGAAGACTGACGCTCAGGTGCGAAAGCGTGGGGAGCAA" 
    ##   [6] "AGGTCACAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGCAGGCGGGAAGACAAGTTGGAAGTGAAATCTATGGGCTCAACCCATAAACTGCTTTCAAAACTGTTTTTCTTGAGTAGTGCAGAGGTAGGCGGAATTCCCGGTGTAGCGGTGGAATGCGTAGATATCGGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCACCAACTGACGCTGAGGCTCGAAAGTGTGGGTAGCAA" 
    ##   [7] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGTGGTGATTTAAGTCAGCGGTGAAAGTTTGTGGCTCAACCATAAAATTGCCGTTGAAACTGGGTTACTTGAGTGTGTTTGAGGTAGGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACTCCGATTGCGAAGGCAGCTTACTAAACCATAACTGACACTGAAGCACGAAAGCGTGGGGATCAA" 
    ##   [8] "AGGATTCAAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGTTTGATAAGTTAGAGGTGAAATCCCGGGGCTTAACTCCGGAACTGCCTCTAATACTGTTAGACTAGAGAGTAGTTGCGGTAGGCGGAATGTATGGTGTAGCGGTGAAATGCTTAGAGATCATACAGAACACCGATTGCGAAGGCAGCTTACCAAACTATATCTGACGTTGAGGCACGAAAGCGTGGGGAGCAA" 
    ##   [9] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGTGGACAGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGCTGTCTTGAGTACAGTAGAGGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTGGACTGCAACTGACACTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [10] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGACGCTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGGTGTCTTGAGTACAGTAGAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTTGCTGGACTGTAACTGACGCTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [11] "ATGGTGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGCAGGCGGTGCGGCAAGTCTGATGTGAAAGCCCGGGGCTCAACCCCGGTACTGCATTGGAAACTGTCGTACTAGAGTGTCGGAGGGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGATAACTGACGCTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [12] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGGTTGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGCGACCTTGAGTGCAACAGAGGTAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTTACTGGATTGTAACTGACGCTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [13] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGTGTGGCAAGTCTGATGTGAAAGGCATGGGCTCAACCTGTGGACTGCATTGGAAACTGTCATACTTGAGTGCCGGAGGGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [14] "AGGTGGCAAGCGTTGTCCGGAATTATTGGGCGTAAAGCGCGCGCAGGCGGCTTCCCAAGTCCCTCTTAAAAGTGCGGGGCTTAACCCCGTGATGGGAAGGAAACTGGGAAGCTGGAGTATCGGAGAGGAAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGAGATTAGGAAGAACACCGGTGGCGAAGGCGACTTTCTGGACGAAAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA"  
    ##  [15] "AGGTCACAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGCAGGCGGGAAGACAAGTTGGAAGTGAAATCCATGGGCTCAACCCATGAACTGCTTTCAAAACTGTTTTTCTTGAGTAGTGCAGAGGTAGGCGGAATTCCCGGTGTAGCGGTGGAATGCGTAGATATCGGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCACCAACTGACGCTGAGGCTCGAAAGTGTGGGTAGCAA" 
    ##  [16] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGTGGTAATTTAAGTCAGCGGTGAAAGTTTGTGGCTCAACCATAAAATTGCCGTTGAAACTGGGTTACTTGAGTGTGTTTGAGGTAGGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACTCCAATTGCGAAGGCAGCTTACTAAACCATAACTGACACTGAAGCACGAAAGCGTGGGTATCAA" 
    ##  [17] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGATTGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGCAGTCTTGAGTGCAGTAGAGGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTGGAGTGTAACTGACGCTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [18] "AAGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGGCTTTTAAGTCAGCGGTCAAATGCCACGGCTCAACCGTGGCCAGCCGTTGAAACTGCAAGCCTTGAGTCTGCACAGGGCACATGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATCGCGAAGGCATTGTGCCGGGGCAGCACTGACGCTGAGGCTCGAAAGTGCGGGTATCAA"  
    ##  [19] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGATGGGTTGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAATTGATACTGGCAGTCTTGAGTACAGTTGAGGTAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTTACTAACCTGTAACTGACATTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [20] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGACTATTAAGTCAGCTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGTCGTCTTGAGTGCAGTAGAGGTAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTTACTGGACTGTAACTGACGCTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [21] "AGGATTCAAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGTTTGATAAGTTAGAGGTGAAATTTCGGGGCTCAACCCTGAACGTGCCTCTAATACTGTTGAGCTAGAGAGTAGTTGCGGTAGGCGGAATGTATGGTGTAGCGGTGAAATGCTTAGAGATCATACAGAACACCGATTGCGAAGGCAGCTTACCAAACTATATCTGACGTTGAGGCACGAAAGCGTGGGGAGCAA" 
    ##  [22] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGTGGATTGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGAAACTGGCAGTCTTGAGTACAGTAGAGGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTAGACTGTTACTGACACTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [23] "AGGGTGCGAGCGTTAATCGGAATTACTGGGCGTAAAGGGTGCGCAGGCGGTTGAGTAAGACAGATGTGAAATCCCCGAGCTTAACTCGGGAATGGCATATGTGACTGCTCGACTAGAGTGTGTCAGAGGGAGGTGGAATTCCACGTGTAGCAGTGAAATGCGTAGATATGTGGAAGAACACCGATGGCGAAGGCAGCCTCCTGGGACATAACTGACGCTCAGGCACGAAAGCGTGGGGAGCAA" 
    ##  [24] "AGGTGGCGAGCGTTATCCGGAATTATTGGGCGTAAAGAGGGAGCAGGCGGCACTAAGGGTCTGTGGTGAAAGATCGAAGCTTAACTTCGGTAAGCCATGGAAACCGTAGAGCTAGAGTGTGTGAGAGGATCGTGGAATTCCATGTGTAGCGGTGAAATGCGTAGATATATGGAGGAACACCAGTGGCGAAGGCGACGATCTGGCGCATAACTGACGCTCAGTCCCGAAAGCGTGGGGAGCAA"  
    ##  [25] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGTGGATTGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGAAACTGGCAGTCTTGAGTACAGTAGAGGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTAGACTGCAACTGACACTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [26] "AGGTCCCGAGCGTTGTCCGGATTTATTGGGCGTAAAGCGAGCGCAGGCGGTTTGATAAGTCTGAAGTTAAAGGCTGTGGCTCAACCATAGTTCGCTTTGGAAACTGTCAAACTTGAGTGCAGAAGGGGAGAGTGGAATTCCATGTGTAGCGGTGAAATGCGTAGATATATGGAGGAACACCGGTGGCGAAAGCGGCTCTCTGGTCTGTAACTGACGCTGAGGCTCGAAAGCGTGGGGAGCGA"  
    ##  [27] "AGGTCTCAAGCGTTGTTCGGAATCACTGGGCGTAAAGCGTGCGTAGGCTGTTTCGTAAGTCGTGTGTGAAAGGCGCGGGCTCAACCCGCGGACGGCACATGATACTGCGAGACTAGAGTAATGGAGGGGGAACCGGAATTCTCGGTGTAGCAGTGAAATGCGTAGATATCGAGAGGAACACTCGTGGCGAAGGCGGGTTCCTGGACATTAACTGACGCTGAGGCACGAAGGCCAGGGGAGCGA" 
    ##  [28] "AGGGTGCAAGCGTTAATCGGAATTACTGGGCGTAAAGCGCACGCAGGCGGTCTGTCAAGTCGGATGTGAAATCCCCGGGCTCAACCTGGGAACTGCATTCGAAACTGGCAGGCTAGAGTCTTGTAGAGGGGGGTAGAATTCCAGGTGTAGCGGTGAAATGCGTAGAGATCTGGAGGAATACCGGTGGCGAAGGCGGCCCCCTGGACAAAGACTGACGCTCAGGTGCGAAAGCGTGGGGAGCAA" 
    ##  [29] "AGGGAGCGAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGTAGGCGGGATCGCAAGTCAGATGTGAAAACTATGGGCTTAACCCATAAACTGCATTTGAAACTGTGGTTCTTGAGTGAAGTAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACATCAGTGGCGAAGGCGGCTTACTGGGCTTTAACTGACGCTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [30] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGTGGATTGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGAAACTGGCAGTCTTGAGTACAGTAGAGGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTAGACTGTCACTGACACTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [31] "AGGGTGCAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGCAGGCGGACCGGCAAGTTGGAAGTGAAAACTATGGGCTCAACCCATAAATTGCTTTCAAAACTGCTGGCCTTGAGTAGTGCAGAGGTAGGTGGAATTCCCGGTGTAGCGGTGGAATGCGTAGATATCGGGAGGAACACCAGTGGCGAAGGCGACCTACTGGGCACCAACTGACGCTGAGGCTCGAAAGCATGGGTAGCAA" 
    ##  [32] "AGGGTGCAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGCAGGCGGGAAGACAAGTTGGAAGTGAAAACCATGGGCTCAACCCATGAATTGCTTTCAAAACTGTTTTTCTTGAGTAGTGCAGAGGTAGATGGAATTCCCGGTGTAGCGGTGGAATGCGTAGATATCGGGAGGAACACCAGTGGCGAAGGCGGTCTACTGGGCACCAACTGACGCTGAGGCTCGAAAGCATGGGTAGCAA" 
    ##  [33] "ATGGTGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGAGTGGCAAGTCTGATGTGAAAACCCGGGGCTCAACCCCGGGACTGCATTGGAAACTGTCAATCTAGAGTACCGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [34] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCACGGCAAGCCAGATGTGAAAGCCCGGGGCTCAACCCCGGGACTGCATTTGGAACTGCTGAGCTAGAGTGTCGGAGAGGCAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTGCTGGACGATGACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [35] "AGGGTGCAAGCGTTATCCGGAATTATTGGGCGTAAAGGGCTCGTAGGCGGTTCGTCGCGTCCGGTGTGAAAGTCCATCGCTTAACGGTGGATCTGCGCCGGGTACGGGCGGGCTGGAGTGCGGTAGGGGAGACTGGAATTCCCGGTGTAACGGTGGAATGTGTAGATATCGGGAAGAACACCAATGGCGAAGGCAGGTCTCTGGGCCGTTACTGACGCTGAGGAGCGAAAGCGTGGGGAGCGA" 
    ##  [36] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGATGGACAAGTCTGATGTGAAAGGCTGGGGCTCAACCCCGGGACTGCATTGGAAACTGCCCGTCTTGAGTGCCGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [37] "ATGGTGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGCAGGCGGAAGGCTAAGTCTGATGTGAAAGCCCGGGGCTCAACCCCGGTACTGCATTGGAAACTGGTCATCTAGAGTGTCGGAGGGGTAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGATAACTGACGCTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [38] "AGGGGGCAAGCGTTATCCGGAATTACTGGGTGTAAAGGGTGCGTAGGTGGTATGGCAAGTCAGAAGTGAAAACCCAGGGCTTAACTCTGGGACTGCTTTTGAAACTGTCAGACTGGAGTGCAGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACATCAGTGGCGAAGGCGGCTTACTGGACTGAAACTGACACTGAGGCACGAAAGCGTGGGGAGCAA" 
    ##  [39] "AAGGTCCGGGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCCGGAGATTAAGCGTGTTGTGAAATGTAGATGCTCAACATCTGAACTGCAGCGCGAACTGGTTTCCTTGAGTACGCACAAAGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTGGAGCGCAACTGACGCTGAAGCTCGAAAGTGCGGGTATCGA" 
    ##  [40] "AGGTGGCAAGCGTTGTCCGGATTTATTGGGCGTAAAGCGAGCGCAGGCGGTTTCTTAAGTCTGATGTGAAAGCCCCCGGCTCAACCGGGGAGGGTCATTGGAAACTGGGAGACTTGAGTGCAGAAGAGGAGAGTGGAATTCCATGTGTAGCGGTGAAATGCGTAGATATATGGAGGAACACCAGTGGCGAAGGCGGCTCTCTGGTCTGTAACTGACGCTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [41] "AGGGTGCAAGCGTTATCCGGAATTATTGGGCGTAAAGGGCTCGTAGGCGGTTCGTCGCGTCCGGTGTGAAAGTCCATCGCTTAACGGTGGATCCGCGCCGGGTACGGGCGGGCTTGAGTGCGGTAGGGGAGACTGGAATTCCCGGTGTAACGGTGGAATGTGTAGATATCGGGAAGAACACCAATGGCGAAGGCAGGTCTCTGGGCCGTTACTGACGCTGAGGAGCGAAAGCGTGGGGAGCGA" 
    ##  [42] "ATGGTGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGCAGGCGGTGCGGCAAGTCTGATGTGAAAGCCCGGGGCTCAACCCCGGTACTGCATTGGAAACTGTCGTACTAGAGTGTCGGAGGGGTAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGATAACTGACGCTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [43] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGTGGATTGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGAAACTGGGAGTCTTGAGTACAGTAGAGGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTAGACTGTCACTGACACTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [44] "AGGTCACAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGCAGGCGGGAGAACAAGTTGGAAGTGAAATCCATGGGCTCAACCCATGAACTGCTTTCAAAACTGTTTTTCTTGAGTAGTGCAGAGGTAGGCGGAATTCCCGGTGTAGCGGTGGAATGCGTAGATATCGGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCACCAACTGACGCTGAGGCTCGAAAGTGTGGGTAGCAA" 
    ##  [45] "AGGATCCAAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGTTTGATAAGTTAGAGGTGAAATACCGGGGCTCAACTCCGGAACTGCCTCTAATACTGTTGAACTAGAGAGTAGTTGCGGTAGGCGGAATGTATGGTGTAGCGGTGAAATGCTTAGAGATCATACAGAACACCGATTGCGAAGGCAGCTTACCAAACTATATCTGACGTTGAGGCACGAAAGCGTGGGGAGCAA" 
    ##  [46] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGATGCTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGGTGTCTTGAGTACAGTAGAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTTGCTGGACTGTAACTGACGCTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [47] "AGGGAGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGTGCGTAGGCGGCTTTGCAAGTCAGATGTGAAATCTATGGGCTCAACCCATAAACTGCATTTGAAACTGTAGAGCTTGAGTGAAGTAGAGGCAGGCGGAATTCCCCGTGTAGCGGTGAAATGCGTAGAGATGGGGAGGAACACCAGTGGCGAAGGCGGCCTGCTGGGCTTTAACTGACGCTGAGGCACGAAAGCGTGGGTAGCAA" 
    ##  [48] "AGGATCCAAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGTTTTATAAGTTAGAGGTTAAATATCGGAGCTTAACTCCGTTATGCCTCTAATACTGTAGGACTAGAGAATAGTTGCGGTAGGCGGAATGTATGGTGTAGCGGTGAAATGCTTAGAGATCATACAGAACACCGATTGCGAAGGCAGCTTACCAAACTATGACTGACGTTGAGGCACGAAAGCGTGGGGAGCAA"  
    ##  [49] "AGGGGGCGAGCGTTATCCGGATTCATTGGGCGTAAAGCGCGCGTAGGCGGCCCGGCAGGCCGGGGGTCGAAGCGGGGGGCTCAACCCCCCGAAGCCCCCGGAACCTCCGCGGCTTGGGTCCGGTAGGGGAGGGTGGAACACCCGGTGTAGCGGTGGAATGCGCAGATATCGGGTGGAACACCGGTGGCGAAGGCGGCCCTCTGGGCCGAGACCGACGCTGAGGCGCGAAAGCTGGGGGAGCGA" 
    ##  [50] "AGGTGGCAAGCGTTGTCCGGAATTATTGGGCGTAAAGAGCATGTAGGCGGGCTTTTAAGTCTGACGTGAAAATGCGGGGCTTAACCCCGTATGGCGTTGGATACTGGAAGTCTTGAGTGCAGGAGAGGAAAGGGGAATTCCCAGTGTAGCGGTGAAATGCGTAGATATTGGGAGGAACACCAGTGGCGAAGGCGCCTTTCTGGACTGTGTCTGACGCTGAGATGCGAAAGCCAGGGTAGCAA"  
    ##  [51] "ATGGAGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGTGTAGGTGGCCATGCAAGTCAGAAGTGAAAATCCGGGGCTCAACCCCGGAACTGCTTTTGAAACTGTAAGGCTAGAGTGCAGGAGGGGTGAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTCACTGGACTGTAACTGACACTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [52] "AAGGTCCGGGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCCGGAGATTAAGCGTGTTGTGAAATGTAGACGCTCAACGTCTGCACTGCAGCGCGAACTGGTTTCCTTGAGTACGCACAAAGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTGGAGCGCAACTGACGCTGAAGCTCGAAAGTGCGGGTATCGA" 
    ##  [53] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGGTGCTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGGTACCTTGAGTGCAGCATAGGTAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTTACTGGACTGTAACTGACGCTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [54] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCGCAGCAAGTCTGATGTGAAAGGCAGGGGCTTAACCCCTGGACTGCATTGGAAACTGCTGTGCTTGAGTGCCGGAGGGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [55] "AGGTGGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGTGCAGCCGGGAATGCAAGTCAGATGTGAAATCCATGGGCTTAACCCATGAACTGCATTTGAAACTGTATTTCTTGAGTACTGGAGAGGCAATCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGATTGCTGGACAGCAACTGACGGTGAGGCGCGAAAGTGTGGGGAGCAA" 
    ##  [56] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGCAGACGGGAGATTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGTTTCCTTGAGTGCAGTTGAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACCCCGATTGCGAAGGCAGCTTGCTAAACTGTAACTGACGTTCATGCTCGAAAGTGTGGGTATCAA" 
    ##  [57] "AGGTGGCAAGCGTTGTCCGGAATTATTGGGCGTAAAGCGCGCGCAGGCGGCTTCTTAAGTCCATCTTAAAAGTGCGGGGCTTAACCCCGTGATGGGATGGAAACTGGGAGGCTGGAGTATCGGAGAGGAAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGAGATTAGGAAGAACACCGGTGGCGAAGGCGACTTTCTGGACGACAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA"  
    ##  [58] "AGGGTGCAAGCGTTATCCGGAATTATTGGGCGTAAAGGGCTCGTAGGCGGTTCGTCGCGTCCGGTGTGAAAGTCCATCGCTTAACGGTGGATCCGCGCCGGGTACGGGCGGGCTTGAGTGCGGTAGGGGAGACTGGAATTCCCGGTGTAACGGTGGAATGTGTAGATATCGGGAAGAACACCAATGGCGAAGGCAGGTCTCTGGGCCGTCACTGACGCTGAGGAGCGAAAGCGTGGGGAGCGA" 
    ##  [59] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCAAGGCAAGTCTGATGTGAAAACCCAGGGCTTAACCCTGGGACTGCATTGGAAACTGTCTGGCTCGAGTGCCGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAAGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [60] "AGGTGGCGAGCGTTGTCCGGAATTATTGGGCGTAAAGAGCATGTAGGCGGCTTAATAAGTCGAGCGTGAAAATGCGGGGCTCAACCCCGTATGGCGCTGGAAACTGTTAGGCTTGAGTGCAGGAGAGGAAAGGGGAATTCCCAGTGTAGCGGTGAAATGCGTAGATATTGGGAGGAACACCAGTGGCGAAGGCGCCTTTCTGGACTGTGTCTGACGCTGAGATGCGAAAGCCAGGGTAGCGA"  
    ##  [61] "AGGATCCAAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGAAGCTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGGTTTCTTGAGTGCAGTAGAGGTAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTTACTGGACTGTAACTGACGCTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [62] "ATGGAGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGTGTAGGTGGCCATGCAAGTCAGAAGTGAAAATCCGGGGCTCAACCCCGGAACTGCTTTTGAAACTGTAAGGCTGGAGTGCAGGAGGGGTGAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTCACTGGACTGTAACTGACACTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [63] "AGGTGGCGAGCGTTATCCGGAATCATTGGGCGTAAAGAGGGAGCAGGCGGCAACTAGGGTCTGCGGTAAAAGACCGAAGCTAAACTTCGGTAAGCCGTGGAAACCGAGGAGCTAGAGTGCAGTAGAGGATCGTGGAATTCCATGTGTAGCGGTGAAATGCGTAGATATATGGAGGAACACCAGTGGCGAAGGCGACGATCTGGGCTGCAACTGACGCTCAGTCCCGAAAGCGTGGGGAGCAA"  
    ##  [64] "AAGGTCCGGGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGCAGGCGGTTGGGAAAGCGTGCCGTCAAATGTCGGGGCTCAACCCCGGAAAGCGGCGCGAACTGTCCGACTTGAGTACGCGGAAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCATAGATATCACGAAGAACCCCGATTGCGAAGGCAGCCTGCCGCAGCGCAACTGACGCTGAGGCTCGAAAGCGCGGGTATCGA"  
    ##  [65] "AGGGAGCGAGCGTTGTCCGGATTTACTGGGTGTAAAGGGTGCGTAGGCGGATTGGCAAGTCAGAAGTGAAATCCATGGGCTTAACCCATGAACTGCTTTTGAAACTGTTAGTCTTGAGTGAAGTAGAGGTAGGCGGAATTCCCGGTGTAGCGGTGAAATGCGTAGAGATCGGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCTTTAACTGACGCTGAGGCACGAAAGTGTGGGTAGCAA" 
    ##  [66] "AGGGAGCGAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGTAGGCGGGATCTTAAGTCAGGTGTGAAAACTATGGGCTCAACCCATAGACTGCACTTGAAACTGAGGTTCTTGAGTGAAGTAGAGGCAGGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACATCAGTGGCGAAGGCGGCCTGCTGGGCTTTTACTGACGCTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [67] "AGGATTCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGGTTGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGCGACCTTGAGTGCAACAGAGGTAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTTACTGGATTGTAACTGACGCTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [68] "AGGGAGCGAGCGTTGTCCGGATTTACTGGGTGTAAAGGGTGCGTAGGCGGCGAGGCAAGTCAGGCGTGAAATCTATGGGCTTAACCCATAAACTGCGCTTGAAACTGTCTTGCTTGAGTGAAGTAGAGGTAGGCGGAATTCCCGGTGTAGCGGTGAAATGCGTAGAGATCGGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCTTTAACTGACGCTGAAGCACGAAAGCATGGGTAGCAA" 
    ##  [69] "AGGTCTCAAGCGTTGTTCGGAATCACTGGGCGTAAAGCGTGCGTAGGCTGTTTCGTAAGTCGTGTGTGAAAGGCAGGGGCTCAACCCCTGGATTGCACATGATACTGCGAGACTAGAGTAATGGAGGGGGAACCGGAATTCTCGGTGTAGCAGTGAAATGCGTAGATATCGAGAGGAACACTCGTGGCGAAGGCGGGTTCCTGGACATTAACTGACGCTGAGGCACGAAGGCCAGGGGAGCGA" 
    ##  [70] "AGGGGGCGAGCGTTATCCGGAATTATTGGGCGTAAAGAGTGCGTAGGTGGTAACTTAAGCGCGGGGTTTAAGGCAATGGCTTAACCATTGTTCGCCCTGCGAACTGGGATACTTGAGTGCAGGAGAGGAAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTTCTGGACTGAAACTGACACTGAGGCACGAAAGTGTGGGGAGCAA"  
    ##  [71] "AAGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGGCTTTTAAGTCAGCGGTCAAATGTCACGGCTCAACCGTGGCCAGCCGTTGAAACTGTAAGCCTTGAGTCTGCACAGGGCACATGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATCGCGAAGGCATTGTGCCGGGGCAGCACTGACGCTGAGGCTCGAAAGTGCGGGTATCAA"  
    ##  [72] "AGGTGGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGTGTAGCCGGGAAGGCAAGTCAGATGTGAAATCCACGGGCTCAACTCGTGAACTGCATTTGAAACTGTTTTTCTTGAGTATCGGAGAGGCAATCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGATTGCTGGACGACAACTGACGGTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ##  [73] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGCACGCCAAGTCAGCGGTGAAATTTCCGGGCTCAACCCGGAGTGTGCCGTTGAAACTGGCGAGCTAGAGTGCACAAGAGGCAGGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACCCCGATTGCGAAGGCAGCCTGCTAGGGTGCGACAGACGCTGAGGCACGAAAGCGTGGGTATCGA" 
    ##  [74] "AGGTGGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGCGTAGGCGGAGAGACAAGTCAGATGTGAAATCTATGGGCTTAACCCATAAACTGCATTTGAAACTATCTCCCTTGAGTGATGGAGAGGCAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTGCTGGACATTAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ##  [75] "ATGGAGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGTGCGTAGGTGGCAGTGCAAGTCAGATGTGAAAGGCCGGGGCTCAACCCCGGAGCTGCATTTGAAACTGCTCGGCTAGAGTACAGGAGAGGCAGGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCCTGCTGGACTGTTACTGACACTGAGGCACGAAAGCGTGGGGAGCAA" 
    ##  [76] "AGGGAGCGAGCGTTGTCCGGAATTACTGGGCGTAAAGGGCGCGTAGGCGGCGCTTCAAGTCGTCTGTCAAAAGCCGAGGCTCAACCTCGGCGCGCAGACGAAACTGGAGAGCTTGAGAAGCAGAGAGGCAAACAGAATTCCTGGTGTAGCGGTGAAATGCGTAGATATCAGGAAGAATACCAGTGGCGAAGGCGGTTTGCTGGCTGCATACTGACGCTGAAGCGCGAAAGCCAGGGGAGCAA"  
    ##  [77] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGTGGATTGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGCAGTCTTGAGTACAGTAGAGGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTGGACTGCAACTGACACTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [78] "AGGGGGCGAGCGTTGTTCGGAATTACTGGGCGTAAAGGGTGTGTAGGCGGTTATGTAAGATAGCGGTGAAATCCCGGGGCTTAACCTCGGAATAGCCGTTATAACTATGTAGCTAGAGTTATGGAGAGGATAGCGGAATACCCAGTGTAGAGGTGAAATTCGTAGATATTGGGTAGAACACCGGTGGCGAAGGCGGCTATCTGGCCATATACTGACGCTGAGGCACGAAAGCATGGGGATCAA" 
    ##  [79] "ATGGAGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGTGTAGGTGGCCAGGCAAGTCAGAAGTGAAAGCCCGGGGCTCAACCCCGGGACTGCTTTTGAAACTGCAGGGCTAGAGTGCAGGAGGGGCAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTGCTGGACTGTAACTGACACTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [80] "AGGTGGCAAGCGTTATCCGGAATTACTGGGTGTAAAGGGAGCGCAGGCGGGATAGCAAGTCAGCTGTGAAAACTATGGGCTCAACCCATAAACTGCAGTTGAAACTGTTATTCTTGAGTGGAGTAGAGGCAAGCGGAATTCCGAGTGTAGCGGTGAAATGCGTAGATATTCGGAGGAACACCAGTGGCGAAGGCGGCTTGCTGGGCTCTAACTGACGCTGAGGCTCGAAAGTGTGGGGAGCAA" 
    ##  [81] "AGGGGGCTAGCGTTATCCGGAATTACTGGGCGTAAAGGGTGCGTAGGTGGTTTCTTAAGTCAGAGGTGAAAGGCTACGGCTCAACCGTAGTAAGCCTTTGAAACTGGGAAACTTGAGTGCAGGAGAGGAGAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTTGCGAAGGCGGCTCTCTGGACTGTAACTGACACTGAGGCACGAAAGCGTGGGGAGCAA"  
    ##  [82] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCAAGGCAAGTCTGAAGTGAAAGCCCGGTGCTTAACGCCGGGACTGCTTTGGAAACTGTTTAGCTGGAGTGCCGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAAGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [83] "ATGGTGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCTGTGTAAGTCTGAAGTGAAAGCCCGGGGCTCAACCCCGGGACTGCTTTGGAAACTATGCAGCTAGAGTGTCGGAGAGGTAAGTGGAATTCCCAGTGTAGCGGTGAAATGCGTAGATATTGGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGATGACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [84] "ATGGAGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGTGCGTAGGTGGCAGTGCAAGTCAGATGTGAAAGGCCGGGGCTCAACCCCGGAGCTGCATTTGAAACTGCATAGCTAGAGTACAGGAGAGGCAGGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCCTGCTGGACTGTTACTGACACTGAGGCACGAAAGCGTGGGGAGCAA" 
    ##  [85] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGAGCAGCAAGTCTGATGTGAAAGGCGGGGGCTCAACCCCTGGACTGCATTGGAAACTGTTGATCTTGAGTACCGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [86] "ATGGAGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGTGTAGGTGGTATCACAAGTCAGAAGTGAAAGCCCGGGGCTCAACCCCGGGACTGCTTTTGAAACTGTGGAACTGGAGTGCAGGAGAGGTAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACTGTAACTGACACTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [87] "AGGGGGCGAGCGTTGTTCGGAATTACTGGGCGTAAAGCGCACGCAGGCGGACCAGTAAGTCTGTCGTCAAAGGCGGAGGCTCAACCTTCGTTCCACGATAGATACTGCGGGTCTAGAGTATGTGAGAGGGAAGTGGAATTCCCGGTGTAGCGGTGAAATGCGTAGATATCGGGAGGAACACCAGTGGCGAAGGCGGCTTCCTGGCACACAACTGACGCTCATGTGCGAAAGCCAGGGCAGCGA" 
    ##  [88] "AGGATGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGCAGGCGGGACTGCAAGTTGGGTGTGAAATACCGTGGCTTAACCACGGAACTGCATCCAAAACTGTAGTTCTTGAGTGAAGTAGAGGCAAGCGGAATTCCGAGTGTAGCGGTGAAATGCGTAGATATTCGGAGGAACACCAGTGGCGAAGGCGGCTTGCTGGGCTTTAACTGACGCTGAGGCTCGAAAGTGTGGGGAGCAA" 
    ##  [89] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCGAAGCAAGTCTGGTGTGAAAACCCGGGGCTCAACCCCGGGCCTGCATTGGAAACTGTTTTGCTCGAGTGCCGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [90] "AGGTGGCAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGTGTAGGCGGGATATCAAGTCAGAAGTGAAAATTACGGGCTCAACTCGTAACCTGCTTTTGAAACTGACATTCTTGAGTGAAGTAGAGGCAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTGCTGGGCTTTTACTGACGCTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [91] "AGGTGGCAAGCGTTGTCCGGAATTATTGGGCGTAAAGGGCGCGCAGGCGGCATCGCAAGTCGGTCTTAAAAGTGCGGGGCTTAACCCCGTGAGGGGACCGAAACTGTGAAGCTCGAGTGTCGGAGAGGAAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAAGCGGCTTTCTGGACGACAACTGACGCTGAGGCGCGAAAGCCAGGGGAGCAA"  
    ##  [92] "ATGGTGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGCAGGCGGTACGGCAAGTCTGATGTGAAAGCCCGGGGCTCAACCCCGGTACTGCATTGGAAACTGTCGGACTAGAGTGTCGGAGGGGTAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGATTACTGACGCTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [93] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGTGGACATGTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGTGTGTCTTGAGTACAGTAGAGGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTGGACTGTTACTGACACTGAGGCTCGAAAGTGTGGGTATCAA" 
    ##  [94] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGACTGGCAAGTCTGATGTGAAAGGCGGGGGCTCAACCCCTGGACTGCATTGGAAACTGTTAGTCTTGAGTGCCGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [95] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGATTATTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGTAGTCTTGAGTGCAGCAGAGGTAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTTACTGGACTGTAACTGACGCTGATGCTCGAAAGTGTGGGTATCAA" 
    ##  [96] "AGGATGCAAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGCACGCCAAGTCAGCGGTGAAATTTCCGGGCTCAACCCGGACTGTGCCGTTGAAACTGGCGAGCTAGAGTACACAAGAGGCAGGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACCCCGATTGCGAAGGCAGCCTGCTAGGGTGAAACAGACGCTGAGGCACGAAAGCGTGGGTATCGA" 
    ##  [97] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGTTTATTAAGTTAGTGGTTAAATATTTGAGCTAAACTCAATTGTGCCATTAATACTGGTAAACTGGAGTACAGACGAGGTAGGCGGAATAAGTTAAGTAGCGGTGAAATGCATAGATATAACTTAGAACTCCGATAGCGAAGGCAGCTTACCAGACTGTAACTGACGCTGATGCACGAGAGCGTGGGTAGCGA"  
    ##  [98] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGAGCAGCAAGTCTGATGTGAAAGGCAGGGGCTCAACCCCTGGACTGCATTGGAAACTGTTGATCTTGAGTACCGGAGGGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ##  [99] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCATGGCAAGCCAGATGTGAAAGCCCGGGGCTCAACCCCGGGACTGCATTTGGAACTGTCAGGCTAGAGTGTCGGAGAGGAAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTTCTGGACGATGACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ## [100] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGTTTTGTAAGTTAGTGGTCAAATGTCATGGCTTAACCTTGGCTTGCCATTAAAACTGCAGGACTCGAGTACAGACGAGGTAGGCGGAATAAGTTAAGTAGCGGTGAAATGCATAGATATAACTTAGAACTCCGATAGCGAAGGCAGCTTACCAGACTGTAACTGACGCTGATGCACGAGAGCATGGGTAGCGA"  
    ## [101] "AGGGTGCAAGCGTTAATCGGAATCACTGGGCGTAAAGCGCACGTAGGCGGCTTGGTAAGTCAGGGGTGAAATCCCACAGCCCAACTGTGGAACTGCCTTTGATACTGCCAGGCTTGAGTACCGGAGAGGGTGGCGGAATTCCAGGTGTAGGAGTGAAATCCGTAGATATCTGGAGGAACACCGGTGGCGAAGGCGGCCACCTGGACGGTAACTGACGCTGAGGTGCGAAAGCGTGGGTAGCAA" 
    ## [102] "AGGTCACAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGCAGGCGGGAGAACAAGTTGGAAGTGAAATCCATGGGCTCAACCTATGAACTGCTTTCAAAACTGTTTTTCTTGAGTAGTGCAGAGGTAGGCGGAATTCCCGGTGTAGCGGTGGAATGCGTAGATATCGGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCACCAACTGACGCTGAGGCTCGAAAGTGTGGGTAGCAA" 
    ## [103] "AGGTGGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGTGTAGGCGGAGAAGCAAGTCAGAAGTGAAATCCATGGGCTTAACCCATGAACTGCTTTTGAAACTGTTTCCCTTGAGTATCGGAGAGGCAGGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCCTGCTGGACGACAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [104] "ATGGTGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGTGCGTAGGTGGTATGGCAAGTCAGAAGTGAAAGGCTGGGGCTCAACCCCGGGACTGCTTTTGAAACTGTCAAACTAGAGTACAGGAGAGGAAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTTCTGGACTGAAACTGACACTGAGGCACGAAAGCGTGGGGAGCAA" 
    ## [105] "AGGGAGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGTGCGTAGGCGGCAATGCAAGTCAGATGTGAAATGCACGGGCTCAACCCGTGAGCTGCATTTGAAACTGTGTTGCTTGAGTGAGGTAGAGGCAGGCGGAATTCCCGGTGTAGCGGTGAAATGCGTAGAGATCGGGAGGAACACCAGTGGCGAAGGCGGCCTGCTGGGCCTTAACTGACGCTGATGCACGAAAGCGTGGGTAGCAA" 
    ## [106] "AGGTGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGCGTGTAGGCGGGATTGCAAGTCAGATGTGAAAACTGGGGGCTCAACCTCCAGCCTGCATTTGAAACTGTAGTTCTTGAGTGCTGGAGAGGCAATCGGAATTCCGTGTGTAGCGGTGAAATGCGTAGATATACGGAGGAACACCAGTGGCGAAGGCGGATTGCTGGACAGTAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [107] "AGGTGGCAAGCGTTATCCGGAATTATTGGGCGTAAAGAGGGAGCAGGCGGCAGCAAGGGTCTGTGGTGAAAGCCTGAAGCTTAACTTCAGTAAGCCATAGAAACCAGGCAGCTAGAGTGCAGGAGAGGATCGTGGAATTCCATGTGTAGCGGTGAAATGCGTAGATATATGGAGGAACACCAGTGGCGAAGGCGACGATCTGGCCTGCAACTGACGCTCAGTCCCGAAAGCGTGGGGAGCAA"  
    ## [108] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCATGGCAAGTCTGAAGTGAAAGCCCGGGGCTCAACCCCGGGACTGCTTTGGAAACTGTCAGGCTAGAGTGCTGGAGAGGTAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACAGTGACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ## [109] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGTGGACTGGTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGTCAGTCTTGAGTACAGTAGAGGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTGGACTGCAACTGACACTGATGCTCGAAAGTGTGGGTATCAA" 
    ## [110] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGCAGACGGGTCTTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGAGACCTTGAGTGCGGTATAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTTGCTGGACCGTAACTGACGTTGAGGCTCGAAAGTGCGGGTATCAA" 
    ## [111] "AGGTGGCAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGCAGGCGGGAGAGCAAGTTGGAAGTGAAATCTGTGGGCTCAACTCACAAATTGCTTTCAAAACTGTTTTTCTTGAGTGGTGTAGAGGTAGGCGGAATTCCCGGTGTAGCGGTGGAATGCGTAGATATCGGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCACTAACTGACGCTGAGGCTCGAAAGCATGGGTAGCAA" 
    ## [112] "AGGGGGCAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGCAGGCGGAGAAGCAAGTCAGTGGTGAAAACGATGGGCTTAACTCATCGACTGCCATTGAAACTGTTTCCCTTGAGTGAAGTAGAGGCAGGCGGAATTCCGAGTGTAGCGGTGAAATGCGTAGATATTCGGAGGAACACCAGTGGCGAAGGCGGCCTGCTGGGCTTTAACTGACGCTGAGGCTCGAAAGTGTGGGGAGCAA" 
    ## [113] "AGGGAGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGTGCGTAGGCGGATTGGCAAGTCAGAAGTGAAATCCATGGGCTTAACCCATGAACTGCTTTTGAAACTGTTAGTCTTGAGTGAAGTAGAGGTAGGCGGAATTCCCGGTGTAGCGGTGAAATGCGTAGAGATCGGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCTTTAACTGACGCTGAGGCACGAAAGTGTGGGTAGCAA" 
    ## [114] "ATGTTCCAAGCGTTATCCGGATTTATTGGGCGTAAAGCGAGCGCAGACGGTTATTTAAGTCTGAAGTGAAAGCCCTCAGCTCAACTGAGGAATTGCTTTGGAAACTGGATGACTTGAGTGCAGTAGAGGAAAGTGGAACTCCATGTGTAGCGGTGAAATGCGTAGATATATGGAAGAACACCAGTGGCGAAGGCGGCTTTCTGGACTGTAACTGACGTTGAGGCTCGAAAGTGTGGGTAGCAA" 
    ## [115] "AGGGAGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGTGCGTAGGCGGCTTTGCAAGTCAGATGTGAAATCTATGGGCTCAACCTATAAACTGCATTTGAAACTGTAGAGCTTGAGTGAAGTAGAGGCAGGCGGAATTCCCCGTGTAGCGGTGAAATGCGTAGAGATGGGGAGGAACACCAGTGGCGAAGGCGGCCTGCTGGGCTTTAACTGACGCTGAGGCACGAAAGCGTGGGTAGCAA" 
    ## [116] "AGGGTGCAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGCAGGCGGATTGGCAAGTTGGGAGTGAAATCTATGGGCTCAACCCATAAATTGCTTTCAAAACTGTCAGTCTTGAGTGGTGTAGAGGTAGGCGGAATTCCCGGTGTAGCGGTGGAATGCGTAGATATCGGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCACTAACTGACGCTGAGGCTCGAAAGCATGGGTAGCAA" 
    ## [117] "AAGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGGCAGTCAAGTCAGCGGTCAAATGGCGCGGCTCAACCGCGTTCCGCCGTTGAAACTGGCAGCCTTGAGTATGCACAGGGTACATGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAGGAACTCCGATCGCGCAGGCATTGTACCGGGGCATTACTGACGCTGAGGCTCGAAGGTGCGGGTATCAA"  
    ## [118] "AGGGGGCGAGCGTTGTCCGGAATTACTGGGCGTAAAGGGTGCGTAGGCGGTTAATTAAGTTGGATGTGAAATTCCCGGGCTTAACTTGGGAGCTGCATTCAAAACTGGTTAACTAGAGTTCAGGAGAGGGAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCGGTGGCGAAGGCGGCTTTCTGGACTGACACTGACGCTGAGGCACGAAAGCGTGGGGAGCAA" 
    ## [119] "AGGTGGCGAGCGTTATCCGGAATTACTGGGTGTAAAGGGTGTGTAGGCGGGACTTCAAGTCAGATGTGAAAATTGCGGGCTCAACCCGCAACCTGCATTTGAAACTGAGGTTCTTGAGAGTCGGAGAGGTAAATGGAATTCCCGGTGTAGCGGTGAAATGCGTAGATATCGGGAGGAACACCAGTGGCGAAGGCGATTTACTGGACGACAACTGACGCTGAGACACGAAAGCGTGGGGAGCAA" 
    ## [120] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGTGGTTAATTAAGTCAGCGGTGAAAGTTTGTGGCTCAACCATAAAATTGCCGTTGAAACTGGTTGACTTGAGTATATTTGAGGTAGGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACTCCGATTGCGAAGGCAGCTTACTAAACTATAACTGACACTGAAGCACGAAAGCGTGGGGATCAA" 
    ## [121] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCGCTGCAAGTCTGAAGTGAAAGCCCGGGGCTCAACCCCGGGACTGCTTTGGAAACTGTAATGCTAGAGTGCTGGAGAGGTAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACAGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ## [122] "AGGTGGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGTGTAGCCGGGAGGGCAAGTCAGATGTGAAATCCACGGGCTCAACTCGTGAACTGCATTTGAAACTACTCTTCTTGAGTATCGGAGAGGCAATCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGATTGCTGGACGACAACTGACGGTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [123] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGATGGGATGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGAAACTGGCATTCTTGAGTGCAGTTGAGGTGGATGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGTTCACTAAACTGTAACTGACATTGAAGCTCGAAAGTGTGGGTATCAA" 
    ## [124] "AGGGGGCAAGCGTTATCCGGAATTATTGGGCGTAAAGCGTGCGTAGGCGGCCACTTAAGTCTGTCGTGAAAGCCCACAGCTTAACTGTGGAGGGTCGATGGAAACTGAGAGGCTTGAATCTAGGAGAGGTAAGTGGAATTCCATGTGTAGCGGTAAAATGCTTAGATATATGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGTCTAGAATTGACGCTGATGCACGAAAGCGTGGGTAGCAA" 
    ## [125] "AGGTGGCGAGCGTTATCCGGAATGATTGGGCGTAAAGGGTGCGTAGGCGGCCCGTTAAGTCTGGAGTCAAAGCCGGCAGCTCAACTGTCGTACGCTTTGGAAACTGGCGGGCTGGAGTGCAGAAGAGGGCGATGGAACTCCATGTGTAGCGGTAAAATGCGTAGATATATGGAAGAACACCAGTGGCGAAGGCGGTCGCCTGGTCTGCAACTGACGCTGAAGCACGAAAGCGTGGGGAGCAA"  
    ## [126] "AGGGTGCAAGCGTTATCCGGAATTATTGGGTGTAAAGGGTGCGTAGACGGGAAAACAAGTTAGTTGTGAAATCCCTCGGCTCAACTGAGGAACTGCAACTAAAACTATTTTTCTTGAGTGTCGGAGAGGAAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGACTTTCTGGACGATAACTGACGTTGAGGCACGAAAGTGTGGGGAGCAA" 
    ## [127] "AGGTGGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGTGCAGCCGGGCCGGCAAGTCAGATGTGAAATCTGGAGGCTTAACCTCCAAACTGCATTTGAAACTGTAGGTCTTGAGTACCGGAGAGGTTATCGGAATTCCTTGTGTAGCGGTGAAATGCGTAGATATAAGGAAGAACACCAGTGGCGAAGGCGGATAACTGGACGGCAACTGACGGTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [128] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGCACGCCAAGTCAGCGGTGAAATTTTCGGGCTCAACCCGGACTGTGCCGTTGAAACTGGCGAGCTAGAGTGCACAAGAGGCAGGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACCCCGATTGCGAAGGCAGCCTGCTAGGGTGCGACAGACGCTGAGGCACGAAAGCGTGGGTATCGA" 
    ## [129] "AGGTGGCGAGCGTTATCCGGAATGATTGGGCGTAAAGGGTGCGCAGGCGGCATATCAAGTCTGAAGTGAAAGGTACGGGCTCAACCTGTACAGGCTTTGGAAACTGGTATGCTCGAGGACAGGAGAGGGCGGTGGAACTCCACGTGTAGCGGTAAAATGCGTAGAGATGTGGAAGAACACCAGTGGCGAAGGCGGCCGCCTGGCCTGTAACTGACGCTCAGGCACGAAAGCGTGGGGAGCAA"  
    ## [130] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGTTTTGCAAGTCTGAAGTGAAAGCCCGGGGCTTAACCCCGGGACTGCTTTGGAAACTGTAGAACTAGAGTGCAGGAGAGGTAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACTGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ## [131] "AGGATCCAAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGTTTGATAAGTTAGAGGTGAAATACCGGTGCTTAACACCGGAACTGCCTCTAATACTGTTGAACTAGAGAGTAGTTGCGGTAGGCGGAATGTATGGTGTAGCGGTGAAATGCTTAGAGATCATACAGAACACCGATTGCGAAGGCAGCTTACCAAACTATATCTGACGTTGAGGCACGAAAGCGTGGGGAGCAA" 
    ## [132] "AGGGGGCTAGCGTTATCCGGATTTACTGGGCGTAAAGGGTGCGTAGGCGGTCTTTTAAGTCAGGAGTGAAAGGCTACGGCTCAACCGTAGTAAGCTCTTGAAACTGGAGGACTTGAGTGCAGGAGAGGAGAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTAGCGAAGGCGGCTCTCTGGACTGTAACTGACGCTGAGGCACGAAAGCGTGGGGAGCAA"  
    ## [133] "AGGGGGCAAGCGTTGTTCGGAATTACTGGGCGTAAAGGGTGTGTAGGCGGTTATGTAAGATAGCGGTGAAATGCCGGGGCTCAACCTCGGAATTGCCTTTATGACTATATAACTAGAATAATGGAGAGGATATCGGAATACCCAGTGTAGAGGTGAAATTCGTAGATATTGGGTAGAACACCAGTGGCGAAGGCGGGTATCTGGCCATTTATTGACGCTGAGGCACGAAAGCATGGGGATCAA" 
    ## [134] "GCAGCTCTAGTGGTAGCAGTTTTTATTGGGCCTAAAGCGTCCGTAGCCGGTTTAATAAGTCTCTGGTGAAATCCTGCAGCTTAACTGTGGGAATTGCTGGAGATACTATTAGACTTGAGATCGGGAGAGGTTAGAGGTACTCCCAGGGTAGAGGTGAAATTCTGTAATCCTGGGAGGACCGCCTGTTGCGAAGGCGTCTGACTGGAACGATTCTGACGGTGAGGGACGAAAGCTAGGGGCGCGA"
    ## [135] "AGGGTGCGAGCGTTAATCGGAATTACTGGGCGTAAAGCGTGCGCAGGCGGTTAAGTAAGACAGATGTGAAATCCCTGAGCTTAACTTAGGAATTGCATTTGTGACTGCTTGACTGGAGTGTGTCAGAGGGAGGTGGAATTCCAAGTGTAGCAGTGAAATGCGTAGATATTTGGAAGAACACCGATGGCGAAGGCAGCCTCCTGGGATAACACTGACGCTCATGCACGAAAGCGTGGGGAGCAA" 
    ## [136] "AGGGGGCAAGCGTTGTCCGGAATCACTGGGCGTAAAGGGCGCGTAGGTGGTCTGTTAAGTCAGATGTGAAATGTAAGGGCTCAACCCTTAACGTGCATCTGATACTGGCAGACTTGAGTGCGGAAGAGGCAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGACTTGCTGGGCCGTAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [137] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGTGGACATGTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGCGTGTCTTGAGTACAGTAGAGGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTGGACTGCAACTGACACTGATGCTCGAAAGTGTGGGTATCAA" 
    ## [138] "AGGTGGCGAGCGTTATCCGGAATGATTGGGCGTAAAGGGTGCGTAGGTGGCAGATCAAGTCTGGAGTAAAAGGTATGGGCTCAACCCGTACTGGCTCTGGAAACTGATCAGCTAGAGAACAGAAGAGGACGGCGGAACTCCATGTGTAGCGGTAAAATGCGTAGATATATGGAAGAACACCGGTGGCGAAGGCGGCCGTCTGGTCTGGATTCTGACACTGAAGCACGAAAGCGTGGGGAGCAA" 
    ## [139] "AGGGAGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGTGCGTAGGCGGCTTTGCAAGTCAGATGTGAAATCTATGGGCTCAACCCATAGCCTGCATTTGAAACTGCAGAGCTTGAGTGAAGTAGAGGCAGGCGGAATTCCCCGTGTAGCGGTGAAATGCGTAGAGATGGGGAGGAACACCAGTGGCGAAGGCGGCCTGCTGGGCTTTAACTGACGCTGAGGCACGAAAGCGTGGGTAGCAA" 
    ## [140] "AGGGTGCAAGCGTTAATCGGAATCACTGGGCGTAAAGCGCACGTAGGCGGCCTGGTAAGTCAGGGGTGAAATCCCACAGCCCAACTGTGGAACTGCCTTTGATACTGCCAGGCTTGAGTACCGGAGAGGGTGGCGGAATTCCAGGTGTAGGAGTGAAATCCGTAGATATCTGGAGGAACACCGGTGGCGAAGGCGGCCACCTGGACGGTAACTGACGCTGAGGTGCGAAAGCGTGGGTAGCAA" 
    ## [141] "AGGTCCCGAGCGTTGTCCGGATTTATTGGGCGTAAAGGGAGCGCAGGCGGTCAGGAAAGTCTGGAGTAAAAGGCTATGGCTCAACCATAGTGTGCTCTGGAAACTGTCTGACTTGAGTGCAGAAGGGGAGAGTGGAATTCCATGTGTAGCGGTGAAATGCGTAGATATATGGAGGAACACCAGTGGCGAAAGCGGCTCTCTGGTCTGTCACTGACGCTGAGGCTCGAAAGCGTGGGTAGCGA"  
    ## [142] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGATGCTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGGCGTCTTGAGTACAGTAGAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCCTGCTGGACTGTCACTGACGCTGATGCTCGAAAGTGTGGGTATCAA" 
    ## [143] "AGGGTGCAAGCGTTAATCGGAATTACTGGGCGTAAAGCGCACGTAGGCTGTTGTGTAAGTCAGGGGTGAAATCCCACGGCTCAACCGTGGAACTGCCCTTGATACTGCATGACTTGAATCCGGGAGAGGGTGGCGGAATTCCAGGTGTAGGAGTGAAATCCGTAGATATCTGGAGGAACATCAGTGGCGAAGGCGGCCACCTGGACCGGTATTGACGCTGAGGTGCGAAAGCGTGGGGAGCAA" 
    ## [144] "AGGGAGCGAGCGTTGTCCGGATTTACTGGGTGTAAAGGGTGCGTAGGCGGCTCTGCAAGTCAGAAGTGAAATCCATGGGCTTAACCCATGAACTGCTTTTGAAACTGTAGAGCTTGAGTGAAGTAGAGGTAGGCGGAATTCCCGGTGTAGCGGTGAAATGCGTAGAGATCGGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCTTTAACTGACGCTGAGGCACGAAAGCATGGGTAGCAA" 
    ## [145] "AGGTGGCGAGCGTTATCCGGAATTATTGGGCGTAAAGAGGGAGCAGGCGGCGGCAGAGGTCTGTGGTGAAAGACTGAAGCTTAACTTCAGTAAGCCATAGAAACCGGGCTGCTAGAGTGCAGGAGAGGATCGTGGAATTCCATGTGTAGCGGTGAAATGCGTAGATATATGGAGGAACACCAGTGGCGAAGGCGACGGTCTGGCCTGTAACTGACGCTCATTCCCGAAAGCGTGGGGAGCAA"  
    ## [146] "ATGGTGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGTGCGTAGGTGGCAAGGCAAGTCTGAAGTGAAAATCCGGGGCTCAACCCCGGAACTGCTTTGGAAACTGTTTAGCTGGAGTACAGGAGAGGTAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGACTTACTGGACTGCTACTGACACTGAGGCACGAAAGCGTGGGGAGCAA" 
    ## [147] "AGGGGGCGAGCGTTATCCGGATTCATTGGGCGTAAAGCGCGCGTAGGCGGCCCGGCAGGCAGGGGGTCAAATGGCGGGGCTCAACCCCGTCCCGCCCCCTGAACCGCCGGGCTCGGGTCCGGTAGGGGAGGGTGGAACACCCGGTGTAGCGGTGGAATGCGCAGATATCGGGTGGAACACCGGTGGCGAAGGCGGCCCTCTGGGCCGAGACCGACGCTGAGGCGCGAAAGCTGGGGGAGCGA"  
    ## [148] "AGGTGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGCGTGTAGGCGGGATCGCAAGTCAGATGTGAAAACTGGAGGCTCAACCTCCAGCCTGCATTTGAAACTGTGGTTCTTGAGTACTGGAGAGGCAGACGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGTCTGCTGGACAGCAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [149] "AGGTGGCGAGCGTTGTCCGGATTTATTGGGCGTAAAGGGAACGCAGGCGGTCTTTTAAGTCTGATGTGAAAGCCTTCGGCTTAACCGAAGTAGTGCATTGGAAACTGGAAGACTTGAGTGCAGAAGAGGAGAGTGGAACTCCATGTGTAGCGGTGAAATGCGTAGATATATGGAAGAACACCAGTGGCGAAAGCGGCTCTCTGGTCTGTAACTGACGCTGAGGTTCGAAAGCGTGGGTAGCAA" 
    ## [150] "AGGGGGCAAGCGTTGTCCGGAATGATTGGGCGTAAAGGGCGCGTAGGCGGTCTGGTAAGTTTGGAGTGAAAGTCCTTTTTTTAAGGAGGGAATTGCTTTGAAAACTGCCGGGCTTGAGTGCAGGAGAGGTAAGCGGAATTCCCGGTGTAGCGGTGAAATGCGTAGAGATCGGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACTGTGACTGACGCTGAGGCGCGAAAGTGTGGGGAGCAA" 
    ## [151] "AGGTGGCAAGCGTTATCCGGAATTATTGGGCGTAAAGAGGGAGCAGGCGGCGGCAAAGGTCTGTGGTGAAAGACTGAAGCTTAACTTCAGTAAGCCATAGAAACCGGGCGGCTAGAGTGCAGGAGAGGATCGTGGAATTCCATGTGTAGCGGTGAAATGCGTAGATATATGGAGGAACACCAGTGGCGAAGGCGACGGTCTGGCCTGTAACTGACGCTCATTCCCGAAAGCGTGGGGAGCAA"  
    ## [152] "AGGGGGCAAGCGTTGTCCGGAATAATTGGGCGTAAAGGGCGCGTAGGCGGCTCGGTAAGTCTGGAGTGAAAGTCCTGCTTTTAAGGTGGGAATTGCTTTGGATACTGTCGGGCTTGAGTGCAGGAGAGGTTAGTGGAATTCCCAGTGTAGCGGTGAAATGCGTAGAGATTGGGAGGAACACCAGTGGCGAAGGCGACTAACTGGACTGTAACTGACGCTGAGGCGCGAAAGTGTGGGGAGCAA" 
    ## [153] "AGGGAGCGAGCGTTATCCGGATTTACTGGGTGTAAAGGGCGTGTAGGCGGGGAAGCAAGTCAGATGTGAAAACCAGTGGCTCAACCACTGGCCTGCATTTGAAACTGTTTTTCTTGAGTGATGGAGAGGCAGGCGGAATTCCGTGTGTAGCGGTGAAATGCGTAGATATACGGAGGAACACCAGTGGCGAAGGCGGCCTGCTGGACATTAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [154] "AGGTGACAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGCGTAGGCGGACTGTCAAGTCAGTCGTGAAATACCGGGGCTTAACCCCGGGGCTGCGATTGAAACTGACAGCCTTGAGTATCGGAGAGGAAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTTCTGGACGACAACTGACGCTGAGGCGCGAAAGTGTGGGGAGCAA" 
    ## [155] "ATGGAGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGTGCAGGTGGCCATGCAAGTCAGAAGTGAAAATCCGGGGCTCAACCCCGGAACTGCTTTTGAAACTGTAAGGCTAGAGTGCAGGAGGGGTGAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTCACTGGACTGTAACTGACACTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ## [156] "ATGGTGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGAGAGGCAAGTCTGGAGTGAAAACCCGGGGCTCAACCCCGGGACTGCTTTGGAAACTGCAATTCTAGAGTGCCGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTGACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ## [157] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGCAGGCGGTGCGGCAAGTCTGATGTGAAAGCCCGGGGCTCAACCCCGGGACTGCATTGGAAACTGTCGTACTTGAGTATCGGAGAGGTAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGATAACTGACGCTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ## [158] "AAGGTCCGGGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCCGTGAGGTAAGCGTGTTGTGAAATGTAGGCGCCCAACGTCTGCACTGCAGCGCGAACTGCCCCACTTGAGTGCGCGCAACGCCGGCGGAACTCGTCGTGTAGCGGTGAAATGCTTAGATATGACGAAGAACCCCGATTGCGAAGGCAGCTGGCGGGAGCGTAACTGACGCTGAAGCTCGAAAGCGCGGGTATCGA" 
    ## [159] "AGGGAGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGTGCGTAGGCGGCTTGGTAAGTCAGATGTGAAATGTATGGGCTCAACCCATGCACTGCATTTGAAACTATTGAGCTTGAGTGAAGTAGAGGTAGGCGGAATTCCCTGTGTAGCGGTGAAATGCGTAGAGATAGGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCTTTAACTGACGCTGAGGCACGAAAGCGTGGGTAGCAA" 
    ## [160] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCACAGCAAGTCTGAAGTGAAATCCCCGGGCTCAACCCGGGAACTGCTTTGGAAACTGTTGGGCTAGAGTGCTGGAGAGGCAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTGCTGGACAGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ## [161] "AGGGGGCTAGCGTTATCCGGATTTACTGGGCGTAAAGGGTGCGTAGGCGGTCTTTCAAGTCAGGAGTGAAAGGCTACGGCTCAACCGTAGTAAGCTCTTGAAACTGGGAGACTTGAGTGCAGGAGAGGAGAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTTGCGAAGGCGGCTCTCTGGACTGTAACTGACGCTGAGGCACGAAAGCGTGGGGAGCAA"  
    ## [162] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGACGGGATGTTAAGTCAGCTGTGAAAGTTTGGGGCTCAACCTTAAAATTGCAGTTGAAACTGGCGTTCTTGAGTGCGGTAGAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCAATTGCGAAGGCAGCTTGCTGGAGCGTAACTGACGTTGATGCTCGAAAGTGTGGGTATCAA" 
    ## [163] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGCAGACGGGAGATTAAGTCAGCTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGTTTCCTTGAGTGCGGTTGAGGTGTGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACCCCGATTGCGAAGGCAGCACACTAAGCCGTAACTGACGTTCATGCTCGAAAGTGTGGGTATCAA" 
    ## [164] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGTGGACATGTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGAAACTGCGTGTCTTGAGTACAGTAGAGGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACTGGACTGCAACTGACACTGATGCTCGAAAGTGTGGGTATCAA" 
    ## [165] "AAGGTCCGGGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCCGTCTCAAAAGCGTGTTGTGAAATGTCCGGGCTCAACCGGGGCCGTGCAGCGCGAACTGTGAGACTTGAGTGTGCGGAAGGTATGCGGAATTCGTGGTGTAGCGGTGAAATGCATAGATATCACGAAGAACTCCGATTGCGAAGGCAGCATGCCGCAGCATTACTGACGCTGATGCTCGAAGGTGCGGGTATCGA" 
    ## [166] "GGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGCGCGTAGGCGGGACGCCAAGTCAGCGGTAAAAGACTGCAGCTAAACTGTAGCACGCCGTTGAAACTGGCGCCCTCGAGACGAGACGAGGGAGGCGGAACAAGTGAAGTAGCGGTGAAATGCTTAGATATCACTTGGAACCCCGATAGCGAAGGCAGCTTCCCAGGCTCGATCTGACGCTGATGCGCGAGAGCGTGGGTAGCGA"  
    ## [167] "AAGGTCCGGGCGTTATCCGGAATCATTGGGTTTAAAGGGAGCGTAGGCCGCATGTCAAGCGTGCTGTGAAATCCCGGGGCTCAACCCCGGAAGCGCAGCGCGAACTGGCGTGCTTGAGTTGCATCGAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACCCCGATTGCGAAGGCAGCCTGCCAGTTGCACACTGACGCTGATGCTCGAAGGCGCGGGTATCGA" 
    ## [168] "AGGGAGCGAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGTAGGCGGGATGGCAAGTCAGATGTGAAAACTATGGGCTCAACCCATAGACTGCATTTGAAACTGTTGTTCTTGAGTGAGGTAGAGGTAAGCGGAATTCCTGGTGTAGCGGTGAAATGCGTAGAGATCAGGAGGAACATCGGTGGCGAAGGCGGCTTACTGGGCCTTTACTGACGCTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ## [169] "AGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCTGTTTTTTAAGTTAGAGGTGAAAGCTCGACGCTCAACGTCGAAATTGCCTCTGATACTGAGAGACTAGAGTGTAGTTGCGGAAGGCGGAATGTGTGGTGTAGCGGTGAAATGCTTAGATATCACACAGAACACCGATTGCGAAGGCAGCTTTCCAAGCTATTACTGACGCTGAGGCACGAAAGCGTGGGGAGCGA" 
    ## [170] "AGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGTGCGTAGGTGGCAAGGCAAGTCAGATGTGAAAGCCCGGGGCTCAACCCCGGTATTGCATTTGAAACTGTTTAGCTAGAGTGCAGGAGAGGTAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACTGTAACTGACACTGAGGCACGAAAGCGTGGGGAGCAA" 
    ## [171] "AGGGGGCGAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCGTATCAAGTCTGATGTGAAAGGCAGGGGCTTAACCCCTGGACTGCATTGGAAACTGGTATGCTTGAGTGCCGGAGGGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ## [172] "AGGTGGCAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGCAGGCGGGAAGCCAAGTCAGCTGTGAAAACTACGAGCTTAACTTGTAGACTGCAGTTGAAACTGGTTTTCTTGAGTGAAGTAGAGGTAGGCGGAATTCCGAGTGTAGCGGTGAAATGCGTAGATATTCGGAGGAACACCGGTGGCGAAGGCGGCCTACTGGGCTTTAACTGACGCTGAGGCTCGAAAGTGTGGGGAGCAA" 
    ## [173] "AGGGTGCGAGCGTTAATCGGAATTACTGGGCGTAAAGCGTGCGCAGGCGGTTGGGTAAGACAGATGTGAAATCCCCGGGCTTAACCTGGGAACTGCATTTGTGACTGTCCGACTGGAGTATGTCAGAGGGGGGTGGAATTCCAAGTGTAGCAGTGAAATGCGTAGATATTTGGAAGAACACCGATGGCGAAGGCAGCCCCCTGGGGCAAAACTGACGCTCATGCACGAAAGCGTGGGGAGCAA" 
    ## [174] "AGGGGGCGAGCGTTGTCCGGAATTACTGGGCGTAAAGGGTGCGTAGGCGGTTTGGTAAGTTGGATGTGAAATACCCGGGCTTAACTTGGGGGCTGCATCCAATACTGTCGGACTTGAGTGCAGGAGAGGAAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCGGTGGCGAAGGCGGCTTTCTGGACTGTAACTGACGCTGAGGCACGAAAGCGTGGGGAGCAA" 
    ## [175] "AGGGGGCGAGCGTTGTCCGGAATGATTGGGCGTAAAGGGCGCGTAGGCGGCCTGCTAAGTCTGAAGTGAAAGTCCTGCTTTCAAGGTGGGAAGTGCTTTGGATACTGGTGGGCTGGAGTGCAGGAGAGGAAAGCGGAATTACCGGTGTAGCGGTGAAATGCGTAGAGATCGGTAGGAACACCAGTGGCGAAGGCGGCTTTCTGGACTGAAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [176] "AGGGGGCAAGCGTTATCCGGAATTACTGGGTGTAAAGGGAGCGTAGGCGGCATGGTAAGCCAGATGTGAAAGCCTTGGGCTTAACCCGAGGATTGCATTTGGAACTATCAAGCTAGAGTACAGGAGAGGAAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAAGAACACCAGTGGCGAAGGCGGCTTTCTGGACTGAAACTGACGCTGAGGCTCGAAAGCGTGGGGAGCAA" 
    ## [177] "ATGGTGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGTGAGTAGGCGGTCATGCAAGTCATATGTGAAATGTCAGGGCTTAACCTTGGCGCTGCATAAGAAACTGTATGACTAGAGTGCAGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAAGAACACCGGTGGCGAAGGCGGCTTACTGGACTGTTACTGACGCTGAGTCACGAAAGCGTGGGGAGCAA" 
    ## [178] "AGGGTGCAAGCGTTAATCGGAATTACTGGGCGTAAAGCGCGCGTAGGTTGTTTTGTAAGTCAGAGGTGTAATCCCACGGCTTAACCGTGGAACTGCCTTTGATACTGCATAACTTGGATCCGGGAGAGGACAGCGGAATTCCAGGTGTAGGAGTGAAATCCGTAGATATCTGGAAGAACATCAGTGGCGAAGGCGGCTGTCTGGACCGGTATTGACGCTGAGGCGCGAAAGCGTGGGTAGCAA" 
    ## [179] "AGGTGGCAAGCGTTATCCGGAATCATTGGGCGTAAAGGGTGCGTAGGTGGCACGATAAGTCTGAAGTAAAAGGCAACAGCTCAACTGTTGTATGCTTTGGAAACTGTCGAGCTAGAGTGCAGAAGAGGGCGATGGAATTCCATGTGTAGCGGTAAAATGCGTAGATATATGGAGGAACACCAGTGGCGAAGGCGGTCGCCTGGTCTGTAACTGACACTGATGCACGAAAGCGTGGGGAGCAA"  
    ## [180] "AGGGAGCAAGCGTTATCCGGATTTATTGGGTGTAAAGGGTGCGTAGACGGGAAATTAAGTTAGTTGTGAAATCCCTCGGCTTAACTGAGGAACTGCAACTAAAACTGATTTTCTTGAGTGCTGGAGAGGAAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGACTTTCTGGACAGTAACTGACGTTGAGGCACGAAAGTGTGGGGAGCAA" 
    ## [181] "AGGTGGCAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGCGTGTAGGCGGAGCTGCAAGTCAGATGTGAAATCTCCGGGCTTAACCCGGAAACTGCATTTGAAACTGTAGCCCTTGAGTATCGGAGAGGCAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTGCTGGACGACAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [182] "AGGGGGCAAGCGTTGTCCGGAATGACTGGGCGTAAAGGGTGTGTAGGCGGGCTCGCAAGTTGGATGTGTAATACCCAGAGCTTAACTCGGGTGCTGCATCTGAAACTACGAGTCTTGAGTGTCGGAGAGGTAAGTGGAATTCCTAGTGTAGCGGTGGAATGCGTAGATATTAGGAGGAACATCAGTGGCGAAGGCGACTTACTGGACGATAACTGACGCTGAGGCACGAAAGCGTGGGGAGCAA"
    ## [183] "AGGTGGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGTGCAGCCGGGTCTGCAAGTCAGATGTGAAATCCATGGGCTCAACCCATGAACTGCATTTGAAACTGTAGATCTTGAGTGTCGGAGGGGCAATCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGATTGCTGGACGATAACTGACGGTGAGGCGCGAAAGTGTGGGGAGCAA" 
    ## [184] "AGGGGGCGAGCGTTGTCCGGAATGATTGGGCGTAAAGGGCGCGTAGGCGGCCTGCTAAGTCTGGAGTGAAAGTCCTGCTTTCAAGGTGGGAATTGCTTTGGATACTGGTGGGCTGGAGTGCAGGAGAGGAAAGCGGAATTACCGGTGTAGCGGTGAAATGCGTAGAGATCGGTAGGAACACCAGTGGCGAAGGCGGCTTTCTGGACTGAAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [185] "AGGATCCAAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGTTTAGTAAGTCAGCGGTGAAATTTTGGTGCTTAACACCAAACGTGCCGTTGATACTGCTGGGCTAGAGAGTAGTTGCGGTAGGCGGAATGTATGGTGTAGCGGTGAAATGCTTAGAGATCATACAGAACACCGATTGCGAAGGCAGCTTACCAAACTATATCTGACGTTGAGGCACGAAAGCGTGGGGAGCAA" 
    ## [186] "AGGTGGCGAGCGTTATCCGGAATCATTGGGCGTAAAGAGGGAGCAGGCGGCCGCAAGGGTCTGTGGTGAAAGACCGAAGCTAAACTTCGGTAAGCCATGGAAACCGGGCGGCTAGAGTGCGGAAGAGGATCGTGGAATTCCATGTGTAGCGGTGAAATGCGTAGATATATGGAGGAACACCAGTGGCGAAGGCGACGGTCTGGGCCGCAACTGACGCTCATTCCCGAAAGCGTGGGGAGCAA"  
    ## [187] "AGGTGGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGTGTAGCCGGGAAGGCAAGTCAGATGTGAAATCCACGGGCTTAACTCGTGAACTGCATTTGAAACTACTTTTCTTGAGTATCGGAGAGGCAATCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGATTGCTGGACGACAACTGACGGTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [188] "AGGGAGCGAGCGTTATCCGGAATTATTGGGTGTAAAGGGTGCGTAGACGGGAGAACAAGTTAGTTGTGAAATACCTCGGCTCAACTGAGGAACTGCAACTAAAACTGTACTTCTTGAGTGCAGGAGAGGTAAGTGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGACTTACTGGACTGTAACTGACGTTGAGGCACGAAAGTGTGGGGAGCAA" 
    ## [189] "AGGGTGCAAGCGTTAATCGGAATTACTGGGCGTAAAGCGCACGCAGGCGGTCTGTCAAGTCGGATGTGAAATCCCCGGGCTCAACCTGGGAACTGCATCCGAAACTGGCAGGCTAGAGTCTTGTAGAGGGGGGTAGAATTCCAGGTGTAGCGGTGAAATGCGTAGAGATCTGGAGGAATACCGGTGGCGAAGGCGGCCCCCTGGACAAAGACTGACGCTCAGGTGCGAAAGCGTGGGGAGCAA" 
    ## [190] "AGGGGGCGAGCGTTATCCGGAATGATTGGGCGTAAAGCGCGCGCAGGCGGCCGCTCAAGCGGGACCTCTAACCCCGGGGCTCAACCCCGGGCCGGGTCCCGAACTGGGCGGCTCGAGTGCGGTAGGGGAGAGCGGAATTCCAAGTGTAGCGGTGAAATGCGCAGATATTTGGAAGAACACCGATGGCGAAGGCAGCTCTCTGGGCCGTCACTGACGCTGAGGCGCGAAAGCCGGGGGAGCGA"  
    ## [191] "AGGTGGCAAGCGTTATCCGGAATCATTGGGCGTAAAGGGTGCGTAGGTGGCGTACTAAGTCTGTAGTAAAAGGCAATGGCTCAACCATTGTAAGCTATGGAAACTGGTATGCTGGAGTGCAGAAGAGGGCGATGGAATTCCATGTGTAGCGGTAAAATGCGTAGATATATGGAGGAACACCAGTGGCGAAGGCGGTCGCCTGGTCTGTAACTGACACTGAGGCACGAAAGCGTGGGGAGCAA"  
    ## [192] "AGGGAGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGCGCGCAGGCGGGCCGGCAAGTTGGAAGTGAAATCTATGGGCTTAACCCATAAACTGCTTTCAAAACTGCTGGTCTTGAGTGATGGAGAGGCAGGCGGAATTCCGTGTGTAGCGGTGAAATGCGTAGATATACGGAGGAACACCAGTGGCGAAGGCGGCCTGCTGGACATTAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAA" 
    ## [193] "ATGTCGCAAGCGTTATCCGGATTTATTGGGCGTAAAGCGCGTCTAGGCGGCTTAGTAAGTCTGATGTGAAAATGCGGGGCTCAACCCCGTATTGCGTTGGAAACTGCTAAACTAGAGTACTGGAGAGGTAGGCGGAACTACAAGTGTAGAGGTGAAATTCGTAGATATTTGTAGGAATGCCGATGGGGAAGCCAGCCTACTGGACAGATACTGACGCTAAAGCGCGAAAGCGTGGGTAGCAA"  
    ## [194] "AGGGTGCAAGCGTTAATCGGAATTACTGGGCGTAAAGGGTGCGCAGGCGGCTGTGCAAGACAGATGTGAAATCCCCGGGCTTAACCTGGGAACTGCATTTGTGACTGCACGGCTAGAGTTTGTCAGAGGAGGGTGGAATTCCGCGTGTAGCAGTGAAATGCGTAGATATGCGGAAGAACACCAATGGCGAAGGCAGCCCTCTGGGACATGACTGACGCTCATGCACGAAAGCGTGGGGAGCAA" 
    ## [195] "AGGATGCAAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGCACGCCAAGTCAGCGGTGAAATTTCCGGGCTCAACCCGGACTGTGCCGTTGAAACTGGCGAGCTAGAGTGCACAAGAGGCAGGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACCCCGATTGCGAAGGCAGCCTGCTAGGGTGCGACAGACGCTGAGGCACGAAAGCGTGGGTATCGA" 
    ## [196] "AAGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGGCTTTTAAGTCAGCGGTCAAATGTCGTGGCTCAACCATGTCAAGCCGTTGAAACTGTAAGCCTTGAGTCTGCACAGGGCACATGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATCGCGAAGGCATTGTGCCGGGGCATAACTGACGCTGAGGCTCGAAAGTGCGGGTATCAA"  
    ## [197] "AGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGCAGGCGGCGCGGTAAGTCAGCGGTAAAAGCCCGGAGCTCAACTCCGGCGAGCCGTTGAAACTGCCGTGCTAGAGACAGATCGAGGTATGCGGAATGCGCAGTGTAGCGGTGAAATGCTTAGATATTGCGCAGAACTCCGATTGCGAAGGCAGCATACCGGGTCTGGTCTGACGCTCAGGCACGAAAGCGTGGGGATCGA"  
    ## [198] "AGGTGGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGTGCAGCCGGGCATGCAAGTCAGATGTGAAATCTCAGGGCTTAACCCTGAAACTGCATTTGAAACTGTATGTCTTGAGTGCCGGAGAGGTAATCGGAATTCCTTGTGTAGCGGTGAAATGCGTAGATATAAGGAAGAACACCAGTGGCGAAGGCGGATTACTGGACGGTAACTGACGGTGAGGCGCGAAAGCGTGGGGAGCGA" 
    ## [199] "AGGTGGCAAGCGTTGTCCGGAATTACTGGGTGTAAAGGGAGCGTAGGCGGGGAGGCAAGTTGAATGTCTAAACTATCGGCTCAACTGATAGTCGCGTTCAAAACTGCCACTCTTGAGTGCAGTAGAGGTAGGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCCTACTGGGCTGTAACTGACGCTGAGGCTCGAAAGCGTGGGTAGCAA"  
    ## [200] "AAGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGTAGGCGGAAGAATAAGTCAGCGGTGAAATGCTTCAGCTCAACTGGAGAATTGCCGATGAAACTGTTTTTCTAGAGTATAAAAGAGGTATGCGGAATGCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACCCCGATTGCGAAGGCAGCATACTGGGCTATAACTGACGCTGAAGCACGAAAGCGTGGGTATCGA" 
    ## [201] "AGGTGGCGAGCGTTGTCCGGAATTATTGGGCGTAAAGAGCATGTAGGCGGTTTTTTAAGTCTGGAGTGAAAATGCGGGGCTCAACCCCGTATGGCTCTGGATACTGGAAGACTTGAGTGCAGGAGAGGAAAGGGGAATTCCCAGTGTAGCGGTGAAATGCGTAGATATTGGGAGGAACACCAGTGGCGAAGGCGCCTTTCTGGACTGTGTCTGACGCTGAGATGCGAAAGCCAGGGTAGCGA"  
    ## [202] "AGGGGGCAAGCGTTGTTCGGAATTACTGGGCGTAAAGGGTGTGTAGGCGGATTTGTAAGATAGTGGTGAAATACCTGAGCTCAACTTAGGAATTGCCATTATAACTATAGATCTGGAGTGACAGAGAGGATATTGGAATACCCAGTGTAGAGGTGAAATTCGTAGATATTGGGTAGAACACCAGTGGCGAAGGCGAGTATCTGGCTGTCAACTGACGCTGAGGCACGAAAGCATGGGGATCAA" 
    ## [203] "AGGGAGCGAGCGTTATCCGGATTCATTGGGCGTAAAGCGCGCGTAGGCGGCCCGTCAAGCGGGGTTTCAAATCCAGGGGCTCAACCTCTGGCCGGACCCCGAACTGGCGGGCTCGAGTGCGGTAGAGGAAGGTGGAATTCCCAGTGTAGCGGTGAAATGCGCAGATATTGGGAAGAACACCGATGGCGAAGGCAGCCTTCTGGGCCGCCACTGACGCTGAGGCGCGAAAGCTAGGGGAGCGA"  
    ## [204] "AGGGTGCAAGCGTTGTCCGGAATCATTGGGCGTAAAGAGTTCGTAGGTGGTTTGTTAAGTTTGGTGTTAAATGCAGAGGCTCAACTTCTGTTCGGCATCGGATACTGGCAGACTAGAATGCGGTAGAGGTAAAGGGAATTCCTGGTGTAGCGGTGAAATGCGTAGATATCAGGAGGAACATCGGTGGCGTAAGCGCTTTACTGGGCCGTAATTGACACTGAGGAACGAAAGCCAGGGTAGCAA" 
    ## [205] "ATGTCCCGAGCGTTATCCGGATTTATTGGGCGTAAAGCGAGCGCAGACGGTTGATTAAGTCTGATGTGAAAGCCCGGAGCTCAACTCCGGAATGGCATTGGAAACTGGTTAACTTGAGTGTTGTAGAGGTAAGTGGAACTCCATGTGTAGCGGTGGAATGCGTAGATATATGGAAGAACACCAGTGGCGAAGGCGGCTTACTGGACAACAACTGACGTTGAGGCTCGAAAGTGTGGGTAGCAA"

``` r
identical(colnames(seqtab_nochim), asv_sequences) 
```

    ## [1] TRUE

``` r
colnames(seqtab_nochim) <- asv_id # switch rownames to ASV shortnames
otus <- otu_table(seqtab_nochim, taxa_are_rows=FALSE)
```

### Combine everything into phyloseq object

``` r
sd <- sample_data(meta_df)
ps <- phyloseq(otus,
               sd,
               tax)
ps
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 205 taxa and 26 samples ]
    ## sample_data() Sample Data:       [ 26 samples by 17 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 205 taxa by 9 taxonomic ranks ]

### Save Phyloseq as RDS

Any R object can be saved to an RDS file. It is a good idea to do this
for any object that is time consuming to generate and is reasonably
small in size. Even when the object was generated reproducibly, it can
be frustrating to wait minutes or hours to regenerate when you are ready
to perform downstream analyses.

We will do this for out phyloseq object to a file since it is quite
small (especially compared to the size of the input FASTQ files), and
there were several time consuming computational steps required to
generate it.

``` r
amplicon_ps_rds
```

    ## [1] "/work/josh/mic2023/amplicon/dada2/matson_amplicon.rds"

``` r
write_rds(ps, amplicon_ps_rds)
```

### Check Phyloseq RDS

We can now confirm that it worked!

``` r
loaded_ps = read_rds(amplicon_ps_rds)
print(loaded_ps)
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 205 taxa and 26 samples ]
    ## sample_data() Sample Data:       [ 26 samples by 17 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 205 taxa by 9 taxonomic ranks ]

We are now ready to use phyloseq!

## Visualize alpha-diversity

``` r
plot_richness(loaded_ps, x="OriginalResponse", measures=c("Shannon", "Simpson")) + theme_bw()
```

    ## Warning in estimate_richness(physeq, split = TRUE, measures = measures): The data you have provided does not have
    ## any singletons. This is highly suspicious. Results of richness
    ## estimates (for example) are probably unreliable, or wrong, if you have already
    ## trimmed low-abundance taxa from the data.
    ## 
    ## We recommended that you find the un-trimmed data and retry.

![](01_dada2_tutorial_files/figure-markdown_github/richness-1.png)

``` r
# Warning message:
# In estimate_richness(physeq, split = TRUE, measures = measures) :
#   The data you have provided does not have
# any singletons. This is highly suspicious. Results of richness
# estimates (for example) are probably unreliable, or wrong, if you have already
# trimmed low-abundance taxa from the data.
# 
# We recommended that you find the un-trimmed data and retry.
```

This was just a bare bones demonstration of how the data from DADA2 can
be easily imported into phyloseq and interrogated. For further examples
on the many analyses possible with phyloseq, see [the phyloseq web
site](https://joey711.github.io/phyloseq/)!

# Session Info

Always print `sessionInfo` for reproducibility!

``` r
sessionInfo()
```

    ## R version 4.3.0 (2023-04-21)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 22.04.2 LTS
    ## 
    ## Matrix products: default
    ## BLAS:   /usr/lib/x86_64-linux-gnu/openblas-pthread/libblas.so.3 
    ## LAPACK: /usr/lib/x86_64-linux-gnu/openblas-pthread/libopenblasp-r0.3.20.so;  LAPACK version 3.10.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## time zone: Etc/UTC
    ## tzcode source: system (glibc)
    ## 
    ## attached base packages:
    ## [1] tools     stats     graphics  grDevices utils     datasets  methods  
    ## [8] base     
    ## 
    ## other attached packages:
    ##  [1] here_1.0.1      tidyr_1.3.0     fs_1.6.2        ggplot2_3.4.1  
    ##  [5] phyloseq_1.44.0 magrittr_2.0.3  tibble_3.2.1    dplyr_1.1.0    
    ##  [9] stringr_1.5.0   readr_2.1.4     dada2_1.28.0    Rcpp_1.0.10    
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] bitops_1.0-7                deldir_1.0-6               
    ##  [3] permute_0.9-7               rlang_1.1.1                
    ##  [5] ade4_1.7-22                 matrixStats_0.63.0         
    ##  [7] compiler_4.3.0              mgcv_1.8-42                
    ##  [9] png_0.1-8                   vctrs_0.6.2                
    ## [11] reshape2_1.4.4              pkgconfig_2.0.3            
    ## [13] crayon_1.5.2                fastmap_1.1.1              
    ## [15] XVector_0.40.0              ellipsis_0.3.2             
    ## [17] labeling_0.4.2              utf8_1.2.3                 
    ## [19] Rsamtools_2.16.0            rmarkdown_2.21             
    ## [21] tzdb_0.3.0                  bit_4.0.5                  
    ## [23] purrr_1.0.1                 xfun_0.39                  
    ## [25] zlibbioc_1.46.0             GenomeInfoDb_1.36.0        
    ## [27] jsonlite_1.8.4              biomformat_1.28.0          
    ## [29] highr_0.10                  rhdf5filters_1.12.1        
    ## [31] DelayedArray_0.26.2         Rhdf5lib_1.22.0            
    ## [33] BiocParallel_1.34.1         jpeg_0.1-10                
    ## [35] parallel_4.3.0              cluster_2.1.4              
    ## [37] R6_2.5.1                    stringi_1.7.12             
    ## [39] RColorBrewer_1.1-3          GenomicRanges_1.52.0       
    ## [41] SummarizedExperiment_1.30.1 iterators_1.0.14           
    ## [43] knitr_1.42                  IRanges_2.34.0             
    ## [45] Matrix_1.5-4                splines_4.3.0              
    ## [47] igraph_1.4.1                tidyselect_1.2.0           
    ## [49] rstudioapi_0.14             yaml_2.3.7                 
    ## [51] vegan_2.6-4                 codetools_0.2-19           
    ## [53] hwriter_1.3.2.1             lattice_0.21-8             
    ## [55] plyr_1.8.8                  Biobase_2.60.0             
    ## [57] withr_2.5.0                 ShortRead_1.58.0           
    ## [59] evaluate_0.20               survival_3.5-5             
    ## [61] RcppParallel_5.1.7          Biostrings_2.68.0          
    ## [63] pillar_1.9.0                MatrixGenerics_1.12.0      
    ## [65] foreach_1.5.2               stats4_4.3.0               
    ## [67] generics_0.1.3              vroom_1.6.1                
    ## [69] rprojroot_2.0.3             RCurl_1.98-1.10            
    ## [71] S4Vectors_0.38.1            hms_1.1.2                  
    ## [73] munsell_0.5.0               scales_1.2.1               
    ## [75] glue_1.6.2                  interp_1.1-4               
    ## [77] data.table_1.14.8           GenomicAlignments_1.36.0   
    ## [79] rhdf5_2.44.0                grid_4.3.0                 
    ## [81] ape_5.7                     latticeExtra_0.6-30        
    ## [83] colorspace_2.1-0            nlme_3.1-162               
    ## [85] GenomeInfoDbData_1.2.10     cli_3.6.1                  
    ## [87] fansi_1.0.4                 S4Arrays_1.0.1             
    ## [89] gtable_0.3.1                digest_0.6.31              
    ## [91] BiocGenerics_0.46.0         farver_2.1.1               
    ## [93] htmltools_0.5.5             multtest_2.56.0            
    ## [95] lifecycle_1.0.3             bit64_4.0.5                
    ## [97] MASS_7.3-59

------------------------------------------------------------------------

This tutorial is based on the [Official DADA2 v1.6
Tutorial](https://raw.githubusercontent.com/benjjneb/dada2/gh-pages/tutorial_1_6.Rmd)
