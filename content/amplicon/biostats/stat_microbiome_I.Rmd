---
title: 'MIC 2023: Statistics for Microbiome Part I - Descriptive Analysis'
author: "Pixu Shi"
date: "`r Sys.Date()`"
output:
  html_document:
    toc: true
    toc_depth: 2
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Preparation

## R libraries

```{r message=FALSE, warning=FALSE}
# for data formatting
library(dplyr)
library(tidyverse)
library(reshape)
# for plotting
library(ggplot2)
library(gridExtra)
# for microbiome data
library(phyloseq)
library(vegan) # for dissimilarity matrices of beta diversity, and transformation
library(ape) # for phylogenetic tree
library(picante) # for faith phylogenetic diversity
library(microbiome) # for transformation
```


## Import data

This tutorial uses the full Matson amplicon data after it has been processed by dada2 and stored in a phyloseq object; i.e. output of `amplicon/bioinformatics/X1_dada2_fullDataset.Rmd`.


```{r}
dir_data <- '/hpc/group/mic-2023/results/amplicon/ps/'
ps <- readRDS(file.path(dir_data, "matson_amplicon_fulldata_filt_tree.rds"))
ps
```


# Data preprocessing

## Extract data from phyloseq object

```{r}
tab_count <- otu_table(ps)
dim(tab_count)
tab_meta <- sample_data(ps)
dim(tab_meta)
tab_tax <- tax_table(ps)
dim(tab_tax)
ntaxa(ps)
phy_tree(ps)
```


## Check metadata

+ Missing values? 
+ Wrong units?
+ Varying format of date and time?
+ Upper/lower cases?
+ What is the data type? Numerical, character, factor?

Summarize meta data:

```{r}
sample_variables(ps)
get_variable(ps, "R.or.NR")
var_type <- sapply(tab_meta, class)
apply(tab_meta[,var_type=="character" | var_type=="factor"], 2, table)
apply(tab_meta[,var_type=="numeric" | var_type=="integer"], 2, summary)
```

If you want metadata to be changed in the phyloseq object:

```{r}
tab_meta$Age <- as.numeric(tab_meta$Age) # this is possibly due to non-numeric input in some entries
ps_updated <- ps 
sample_data(ps_updated) <- tab_meta[, c("Age", "R.or.NR", "PatientID")]
```

Select samples by meta data:

```{r}
ps_filt <- subset_samples(ps, R.or.NR=="R")
ps_filt
```


## Check taxa table

Check taxa table column names. Sometimes they can be spelled differently.

```{r}
rank_names(ps)
```

Check number of taxa at each taxonomic level:

```{r}
tibble(rank = rank_names(ps)) %>%
  rowwise %>%
  mutate(num_taxa = length(get_taxa_unique(ps,rank)))
```

Check phylum names:

```{r}
get_taxa_unique(ps,"Phylum")
```

Aggregate to genus level

```{r}
length(get_taxa_unique(ps,"Genus"))
ps_genus <- tax_glom(ps, taxrank="Genus") # it can takes a while to run for large dataset
# NA genus are removed after tax_glom()
ps_genus
head(tax_table(ps_genus))
taxa_names(ps_genus) <- tax_table(ps_genus)[, 'Genus']
head(tax_table(ps_genus))
```


## Check read count table

### Sequencing depth

```{r}
seq_depth <- sample_sums(ps) # or rowSums(otu_table(ps))
summary(seq_depth)
barplot(sort(seq_depth), las=2, cex.names=0.5)
```

Filter by sequencing length

```{r}
ps_filt <- prune_samples(sample_sums(ps)>15000, ps)
ps_filt
```

### Absolute abundance

```{r}
plot_bar(ps, fill="Phylum") + 
  labs(y="Absolute Abundance") + 
  geom_bar(aes(color=Phylum, fill=Phylum), stat="identity", position="stack") +
  theme_bw() + # put theme_bw() before theme
  theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5))
```


Facet by NR.or.R

```{r}
library(RColorBrewer)
mycolors <- brewer.pal(11, "BrBG")
plot_bar(ps, fill="Phylum") + 
  labs(y="Relative Abundance") + 
  geom_bar(aes(color=Phylum, fill=Phylum), stat="identity", position="stack") +
  theme_bw() + # put theme_bw() before theme
  theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5)) +
  facet_grid(~R.or.NR, scale="free_x", space = "free_x") +
  scale_color_manual(values=mycolors) + 
  scale_fill_manual(values=mycolors)
```




### Relative abundance

Relative abundance is also known as compositional data, because they sum up to 1. One coordinate is redundant information. 

```{r}
ps_rela <- transform_sample_counts(ps, function(x){x/sum(x)})
summary(rowSums(otu_table(ps_rela)))
```

Plot relative abundance for all phyla:

```{r}
plot_bar(ps_rela, fill="Phylum") + 
  labs(y="Relative Abundance") + 
  geom_bar(aes(color=Phylum, fill=Phylum), stat="identity", position="stack") +
  theme_bw() + # put theme_bw() before theme
  coord_cartesian(ylim = c(0,1), expand=0) +
  theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5))
```

Reorder phyla:

```{r}
p0 <- plot_bar(ps_rela, fill="Phylum")
p0$data$Phylum <- as.factor(p0$data$Phylum)
levels(p0$data$Phylum)
p0$data$Phylum <- factor(p0$data$Phylum, levels=levels(p0$data$Phylum)[c(2,5,1,3,4,6:10)])
levels(p0$data$Phylum)
p0 +
  labs(y="Relative Abundance") + 
  geom_bar(aes(color=Phylum, fill=Phylum), stat="identity", position="stack") +
  theme_bw() + # put theme_bw() before theme
  coord_cartesian(ylim = c(0,1), expand=0) +
  theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5)) +
  facet_grid(~R.or.NR, scale="free_x", space = "free_x") 
```


**Exercise:** Try to facet by NR.or.R.


### Check an individual taxon

Plot relative abundance of one ASV for each sample:

```{r}
tax_sel <- c("ASV_1", "ASV_19")
tab_tax_sel <- ps_rela %>%
  otu_table() %>%
  data.frame() %>%
  rownames_to_column(., var="SampleID") %>%
  select(all_of(tax_sel), SampleID) %>%
  melt()
  
tab_tax_sel <-  ps_rela %>% 
  sample_data() %>%
  data.frame() %>%
  rownames_to_column(var="SampleID") %>%
  as_tibble() %>%
  select(R.or.NR, SampleID) %>%
  merge(., tab_tax_sel)
  
tab_tax_sel %>%
  ggplot() +
  geom_point(aes(x=SampleID, y=value, color=R.or.NR)) +
  labs(x=element_blank(), y="Relative Abundance", color="Responder or Not") + 
  facet_wrap(vars(variable), scales = "free_y", nrow=3) +
  theme_bw() + 
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
```

Sort subjects by R.or.NR:

```{r}
tab_tax_sel %>%
  dplyr::arrange(R.or.NR) %>%
  mutate(SampleID=factor(.data$SampleID, levels=unique(.data$SampleID))) %>%
  ggplot() +
  geom_point(aes(x=SampleID, y=value, color=R.or.NR)) +
  labs(x=element_blank(), y="Relative Abundance", color="Responder or Not") + 
  facet_wrap(vars(variable), scales = "free_y", nrow=3) +
  theme_bw() + 
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
```


### Taxa filtering

Taxa is often filtered based on their occurrence frequency or relative abundance. How many taxa will be filtered at each filtering criteria?

```{r}
tab_filt_check <- data.frame(relative_abundance = colMeans(otu_table(ps_rela)),
                             occur_count = colSums(otu_table(ps_rela)!=0),
                             occur_frequency = colMeans(otu_table(ps_rela)!=0))
summary(tab_filt_check)
tab_filt_check %>% 
  reshape::melt() %>% 
  ggplot() +
  geom_histogram(aes(x=value)) + 
  facet_wrap(vars(variable), scales="free_x") +
  theme_bw()
                          
```

Filter taxa appearing in at least 2 samples


```{r}
table(filter_taxa(ps, function(x){sum(x!=0)>=2}))
ps_filt <- ps %>% 
  filter_taxa(., function(x){sum(x!=0)>=2}, prune=TRUE)
ps_filt
```

Filter taxa appearing in at least 5% of the samples

```{r}
ps_filt <- ps %>%
  prune_taxa(colMeans(otu_table(.)==0)<=0.95, .)
ps_filt
```

**Exercise:** Filter taxa appearing in at least 5% of the samples using `filter_taxa()`.

Keep the 20 taxa with most average relative abundance:

```{r}
top20 <- names(sort(taxa_sums(ps_rela), decreasing=TRUE))[1:20]
ps_rela_top20 <- 
  ps_rela %>%
  prune_taxa(top20, .)

plot_bar(ps_rela_top20, fill="Genus") +
  labs(y = "Relative Abundance") +
  geom_bar(aes(color=Genus, fill=Genus), stat="identity", position="stack")
```


## Transformations

+ Transformation is generally applied to the relative abundance (compositional data), because methods modeling continuous variables mostly need the sequencing depth to be normalized out.

+ Methods that model counts directly do not need transformations.

+ Benefits of doing transformations:

    + Many statistical methods require normal distribution or continuous values of the data.
    + The impact of the super large values is diminished.
    + Compositional data are intrinsicly **colinear** because all features sum up to 1. Directly using compositional data as predictors in linear models causes problems.
    
+ Transformations listed here cannot fix the intrinsic correlation of compositional data. The increase of abundance for one taxon means some other taxa decrease in their abundance. Special consideration need to be made for modeling and interpreting multivariate analysis of taxa (e.g. regression with multivaraite taxa as predictors, network analysis of compositional data.)


Definition of symbols:

+ $n$: number of samples
+ $p$: number of taxa
+ $K_{ij}$ as the read count of the $j$th taxon in the $i$th sample.
+ $N_{i}=\sum_{j=1}^p K_{ij}$ as the sequencing depth of the $i$th sample.
+ $P_{ij}=K_{ij}/N_i$ as the relative abundance of the $j$th taxon in the $i$th sample. 
+ $S_{i}$ as the unique number of taxa in sample $i$.

### Log transformation

+ Definition: for the $j$th feature in the $i$th sample, the value after transformation is $\log P_{ij}$ or $\log (K_{ij}/N_i)$.

+ $\log(0)=-\infty$, so a pseudo count is often added to the zeros. 
    
    + 0.5 if starting with counts
    + 1/2 of the smallest non-zero value if starting with proportions.
    
+ After transformation, the data is no longer collinear.

+ **Alternative log transformation**: $\log (P_{ij}+1)$ as the value after transformation. The resulting values will be non negative. Zeros are kept as zeros allowing storage as sparse matrix, which makes it more popular for scRNA-Seq data.

### Additive log ratio (ALR)

+ Definition: suppose the $p$th taxon is chosen as the reference taxon, 
then for the $j$th feature in the $i$th sample, the value after transformation is $\log(P_{ij}/P_{ip}), j=1,\dots, p-1$. 

+ The ratio can be taken using counts or proportions because $\log(\frac{K_{ij}}{K_{ip}})=\log\left(\frac{K_{ij}/N_i}{K_{ip}/N_i}\right)=\log(\frac{P_{ij}}{P_{ip}})$. 

+ $K_{ij}$, $K_{ip}$, $P_{ij}$, $P_{ip}$ cannot be 0, so a pseudo count is often added to the zeros

    + 0.5 if starting with counts
    + 1/2 of the smallest non-zero value if starting with proportions.
    + Different packages may have different implementations.

+ The transformed abundance takes values between $(-\infty, +\infty)$.

+ The results from using ALR transformed data in many methods depend on which taxon is used as reference. A good reference taxon should not contain a lot of zeros.

+ Spike-in controls are good example of reference taxa.

+ It removes the abundance of reference taxon, so ALR transformed data is no longer collinear.



Check distribution before and after ALR transformation

```{r}
head(taxa_names(ps))
ps_alr <- microbiome::transform(ps, transform="alr", target="sample", shift=0.5) # by default the reference taxon is the first, which may not be a good choice
par(mfrow=c(1,2))
hist(otu_table(ps_rela)[, "ASV_1"], breaks=20, xlab="relative abundance ASV_1", main="")
hist(otu_table(ps_alr)[, "ASV_1"], breaks=20, xlab="alr transformed ASV_1", main="")
```

How was ALR implemented exactly in `microbiome::transform()`? 

```{r}
tax1 <- otu_table(ps)[, "ASV_1"]
tax_ref <- otu_table(ps)[, "ASV_19"] # this is actually a bad reference taxon because it has a lot of zeros
# only zeros have 0.5 added
tax1[tax1==0] <- 0.5
tax_ref[tax_ref==0] <- 0.5
plot(log(tax1/tax_ref), otu_table(ps_alr)[, "ASV_1"])
```

### Central log ratio (CLR)

+ Definition: For the $j$th feature in the $i$th sample, the value after transformation is $\log(P_{ij}/g_i), j=1,\dots, p$, where $g_i=\left(\prod_{j=1}^p P_{ij}\right)^{1/p}$ is the geometric mean of $P_{ij}$.

+ It is the same as $\log(P_{ij})-\frac{1}{p}\sum_{k=1}^p\log(P_{ik})$. 

+ $K_{ij}$, $K_{ip}$, $P_{ij}$, $P_{ip}$ cannot be 0, so a pseudo count is often added to the zeros

    + 0.5 if starting with counts
    + 1/2 of the smallest non-zero value if starting with proportions.
    + Different packages may have different implementations.

+ The transformed abundance takes values between $(-\infty, +\infty)$.

+ No reference taxon is needed. 

+ The sum of all features after CLR transformation is 0 by definition. Thus, CLR transformed data still has collinearity.

Check distribution before and after CLR transformation:

```{r}
head(taxa_names(ps))
ps_clr <- microbiome::transform(ps, transform="clr", target="OTU")
summary(rowSums(otu_table(ps_clr)))
par(mfrow=c(1,2))
hist(otu_table(ps_rela)[, "ASV_1"], breaks=20, xlab="relative abundance ASV_1", main="")
hist(otu_table(ps_clr)[, "ASV_1"], breaks=20, xlab="clr transformed ASV_1", main="")
```

How was CLR implemented exactly in `microbiome::transform()`? 

```{r}
tab_count <- otu_table(ps)
tab_rela <- tab_count / rowSums(tab_count)
pseudo <- tab_rela %>% 
  as.vector() %>% 
  (function(x){min(x[x!=0])/2}) # half of smallest nonzero is added to zeros
tab_rela[tab_rela==0] <- pseudo
tab_clr <- t(apply(tab_rela, 1, function(x) {
            log(x) - mean(log(x))
        }))
plot(tab_clr[, "ASV_1"], otu_table(ps_clr)[, "ASV_1"])
```


### Arcsin square-root transformation (AST)

Definition: $P_{ij}$ are transformed into $\text{arcsin}(\sqrt{P_{ij}})$.

When sample size is small and many samples have zero counts for the taxon of interest, the combination of pseudo count and log transformation may introduce artificial variation that affects the result. 
The AST transformation does not need pseudo count, thus does not introduce artificial variation into zero counts.
But interpretation is harder.

```{r echo=FALSE}
xx <- (0:100)/100
yy <- asin(sqrt(xx))
plot(xx, yy, type="l", lwd=2, xlab="x", ylab="AST(x)")
```

How to implement AST transformation?

```{r}
ps_ast <- ps_rela
otu_table(ps_ast) <- asin(sqrt(otu_table(ps_ast)))
par(mfrow=c(1,2))
hist(otu_table(ps_rela)[, "ASV_1"], breaks=20, xlab="relative abundance ASV_1", main="")
hist(otu_table(ps_ast)[, "ASV_1"], breaks=20, xlab="ast transformed ASV_1", main="")
```


### Logit transformation 


```{r echo=FALSE}
xx <- (1:100)/100
yy <- log(xx/(1-xx))
plot(xx, yy, type="l", lwd=2, xlab="x", ylab="logit(x)")
```


How to implement logit transformation?

```{r}
ps_logit <- ps_rela
otu_table(ps_logit) <- otu_table(ps_logit) %>% 
  (function(x){log(x/(1-x))}) %>%
  otu_table(., taxa_are_rows=FALSE)
par(mfrow=c(1,2))
hist(otu_table(ps_rela)[, "ASV_1"], breaks=20, xlab="relative abundance ASV_1", main="")
hist(otu_table(ps_logit)[, "ASV_1"], breaks=20, xlab="logit transformed ASV_1", main="")
```


### Effect of pseudo count and transformation

```{r}
ct <- c(0,0,0,0,1,10,50,200)
ct_pseudo <- ct
ct_pseudo[ct==0] <- 0.5
dpth <- c(5000, 10000, 30000, 50000, 5000, 10000, 30000, 50000)

par(mfrow=c(1,2))
plot(ct/dpth, log((ct_pseudo)/dpth/(1-(ct_pseudo)/dpth)), xlab="relative abundance", ylab="logit(relative abundance)", col=rep(c(1,2),each=4), pch=20)
plot(ct/dpth, asin(sqrt(ct/dpth)), xlab="relative abundance", ylab="AST(relative abundance)", col=rep(c(1,2),each=4), pch=20)
```



# Alpha diversity

Alpha diversity quantifies within sample diversity. There are many alpha diversity indices emphasizing on different aspect of the diversity.

## Types of alpha diversity

+ Richness indices: only looks at if a taxon is present/absent.

    + Species richness: number of unique taxa (doesn't have to be species).
    + Chao-1: Considers both the number of unique taxa in a sample and the number of samples a taxon appears. The diversity of one sample is affected by the other samples in the dataset.
    + Fisher: Similar to Chao-1.
    + Abundance-based coverage estimator (ACE): Similar to Chao-1.
    
+ Evenness indices: looks at the relative abundance of a given taxonomic level. $(1/p, \dots, 1/p)$ is the most even and $(1,0,\dots,0)$ is the least even.

    + Shannon index: $I_i=\sum_{j=1}^p P_{ij}\log(P_{ij})$. (Note: $0\log(0)=0$. $0\leq I_i\leq \log(p)$.)
    + Simpson index: $I_i=1 - \sum_{j=1}^p P_{ij}^2$. (Note: $0\leq I_i\leq 1-1/p$.)
    
+ Phylogenetic diversity (PD): look at the branch lengths of the phylogenetic tree.

    + Faith's PD: summation of branch length of the tree that span the features in the sample.

## Calculation and visualization

Obtain alpha diversity into a data frame.

```{r}
tab_richness <- estimate_richness(ps)
head(tab_richness)
tab_richness <- tab_richness[,c(1,2,9,4,6,7,8)]
tab_richness$Faith <- 
  pd(otu_table(ps), phy_tree(ps), include.root=FALSE)$PD
```

Compare different alpha diversity metrics:

```{r}
pairs(tab_richness)
```

Plot selected alpha diversity indices against a phenotype

```{r}
tab_meta_concise <- as.data.frame(sample_data(ps)[, c("PatientID", "R.or.NR")])

tab_richness_long <- tab_richness %>% 
  select(Observed, Shannon, Faith) %>%
  merge(tab_meta_concise, ., by=0) %>%
  column_to_rownames(var="Row.names") %>%
  reshape::melt()
tab_richness_long %>%
  ggplot() + 
  geom_boxplot(aes(x=R.or.NR, y=value, color=R.or.NR)) +
  geom_point(aes(x=R.or.NR, y=value, color=R.or.NR)) +
  labs(x=element_blank(), y="Alpha Diversity", color="Responder or Not") + 
  facet_wrap(vars(variable), scales = "free") +
  theme_bw()
```

Plot alpha diversity for each patient

```{r}
tab_richness_long %>%
  ggplot() +
  geom_point(aes(x=PatientID, y=value, color=R.or.NR)) +
  labs(x=element_blank(), y="Alpha Diversity", color="Responder or Not") + 
  facet_wrap(vars(variable), scales = "free_y", nrow=3) +
  theme_bw() + 
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
```


A simple way with `plot_richness` but doesn't include Faith phylogenetic diversity:

```{r}
plot_richness(ps, x="R.or.NR", 
              measures=c("Observed", "Shannon", "Simpson"), color="R.or.NR") + 
              geom_boxplot() +
              geom_point() +
              theme_bw() 
```


```{r}
plot_richness(ps, x="PatientID", 
              measures=c("Observed", "Shannon")) + 
  geom_point(aes(color=R.or.NR)) +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  labs(color="Responder or Not")
```



## Rarefication

Rarefication is not recommended for majority of analysis of microbiome data. It introduces unnecessary randomness in the data.

However, since larger sequencing depth means rare taxa are more likely to be found, alpha diversity is more comparable when sequencing depth is more comparable.

Rarefication curve plot the number of taxa against sequencing depth.

```{r}
rarecurve(as.data.frame(otu_table(ps)), step=100, cex=0.5)
```

Rarefication to even depth

```{r}
ps_rare <- rarefy_even_depth(ps, sample.size=5000, rngseed=1)
sample_sums(ps_rare)
```

Check the effect of rarefication on species richness:

```{r}
plot(estimate_richness(ps)$Observed, 
     estimate_richness(ps_rare)$Observed,
     xlab="Richness - no rarefication",
     ylab="Richness - rarefied at 5000",
     pch=20)
abline(0,1)
```

**Exercise:** Try to see the effect of rarefication on other alpha diversity indices.

**Question:** Should alpha diversity be calculated before or after taxa filtering?


# Beta Diversity

Beta diversity quantifies between sample diversity, i.e. how similar one sample is to another sample. It is NOT a index specific to a given sample.
It is quantified by a sample-by-sample distance/dissimilarity matrix $D$.

## Types of beta diversity

+ Single taxonomic level dissimilarity matrix
    
    + Jaccard: $D_{AB}=\dfrac{\text{Number of taxa shared by Sample $A$ and $B$}}{\text{Number of taxa in Sample $A$ and $B$ combined}}$.
    + Bray-Curtis: $D_{AB}=\dfrac{\sum_{j=1}^p |P_{Aj}-P_{Bj}|}{\sum_{j=1}^p (P_{Aj}+P_{Bj})}$.
    + Aitchison: The Euclidean distance of the CLR transformed relative abundance.

+ Tree based dissimilarity matrix

    + Unweighted Unifrac: $D_{AB}=\dfrac{\text{sum of branch length shared by Sample $A$ and $B$}}{\text{sum of all branch length}}$
    + Weighted Unifrac: $\dfrac{\sum_{k \text{ for all tree branches}}b_i|A_i-B_i|}{\sum_{k \text{ for all tree branches}}b_i(A_i+B_i)}$, where $A_i$ (or $B_i$) is the proportion of taxa descending from Branch $i$ in Sample $A$ (or $B$).

+ Unweighted Unifrac and Jaccard are presence/absence based. The others are abundance based.

Note for calculating the dissimilarity matrices:

+ `binary=TRUE` is needed for Jaccard index, but not the others.
+ Bray-Curtis is calculated based on relative abundance.
+ Tree based methods need phylogenetic tree to be rooted, otherwise a random root is chosen.

```{r}
# method="jaccard", "bray", "unifrac", "wunifrac"
dist_jaccard <- phyloseq::distance(ps_rela, method="jaccard", binary=TRUE)
dist_bray <- phyloseq::distance(ps_rela, method="bray")
dist_unifrac <- phyloseq::distance(ps_rela, method="unifrac")
dist_wunifrac <- phyloseq::distance(ps_rela, method="wunifrac")
class(dist_bray)
dim(as.matrix(dist_bray))
```

For Aitchison distance provided by `vegdist`, need to start from counts and specify pseudo count.

```{r}
# method="jaccard", "bray", "aitchison", "euclidean"
dist_aitchison <- vegdist(otu_table(ps), method="aitchison", pseudo=0.5) 
```


## Principle coordinate analysis

Principle coordinate analysis (PCoA) is also called classical multidimensional scaling (MDS). It transform a $n$-by-$n$ distance/dissimilarity matrix $D$ into an $n$-by-$k$ coordinate matrix $X$ such that the $\text{distance}(X)-D$ is very small.

$X$ is obtained by the eigenvalue decomposition of the matrix $B=-\frac{1}{2}(I-\frac{1}{n}1_n1^\top_n)D^{(2)}(I-\frac{1}{n}1_n1^\top_n)$, where $D^{(2)}$ is the element-wise square of $D$. The variance explained by each coordinate is calculated from the eigenvalues of $B$.


Plotting the first and third component as an example:

```{r}
# method="jaccard", "bray", "unifrac", "wunifrac"
ord_bray <- ordinate(ps_rela, method="PCoA", distance="bray")
plot_ordination(ps_rela, ord_bray, 
                axes=c(1,3), # specify which coordinates to plot
                type="samples", color="R.or.NR") +
  theme_bw()
```


Alternatively, you can plug in any other dissimilarity matrices.

```{r}
dist_bray <- vegdist(otu_table(ps_rela), method="bray")
ord_bray_pcoa <- cmdscale(dist_bray, eig=TRUE, k=3) # this can replace the outcome of ordinate()
var_explain <- round(ord_bray_pcoa$eig/sum(ord_bray_pcoa$eig), 3)
plot(ord_bray_pcoa$points[,c(1,3)], pch=20,
     xlab=paste0("Axis 1, ", var_explain[1]*100, "%"), 
     ylab=paste0("Axis 3, ", var_explain[3]*100, "%"))
plot(1:length(var_explain), var_explain, ylab="Variance Explained", xlab="Number of Components", type="b", lwd=2, pch=20)
```

Facet by `R.or.NR`:

```{r}
plot_ordination(ps_rela, ord_bray, axes=c(1,3),
                type="samples", color="R.or.NR") +
  theme_bw() + 
  facet_wrap(~R.or.NR)
```

Sometimes it is helpful to show all the points, but gray out the ones that are not the the focus

```{r}
p_old_pcoa <- plot_ordination(ps_rela, ord_bray, axes=c(1,3),
                type="samples", color="R.or.NR")
head(colnames(p_old_pcoa$data))
ggplot(p_old_pcoa$data, aes(Axis.1, Axis.3)) +
  theme_bw() +
  geom_point(data = mutate(p_old_pcoa$data, R.or.NR = NULL), color = "grey") +
  geom_point(aes(color = R.or.NR)) + 
facet_grid(~R.or.NR, labeller = "label_both")
```

You can add 95% confidence elipses to ordination plots by appending `+ stat_ellipse(type = "norm")` after the plotting function:

```{r}
plot_ordination(ps_rela, ord_bray_pcoa, axes=c(1,3),
                type="samples", color="R.or.NR") +
  stat_ellipse(type = "norm") +
  theme_bw()
```

Spider plot:

```{r}
# note: you have to highlight and run the two functions below at once for the plot to work properly
# cannot take the outcome of ordinate(, method="PCoA"), has to use cmdscale() to get ordination
ordiplot(ord_bray_pcoa, display = 'sites', type = 'n')
ordispider(ord = ord_bray_pcoa, display = "sites",
           choices=c(1,3), # specify which components to plot
            groups = get_variable(ps_rela, "R.or.NR"), 
            col = c(1,2))
```


**Exercise:** Try to see the effect of rarefication and taxa filtering on beta diversity metrics.

**Question:** Should we calculate beta diversity before or after taxa filtering?

## Nonmetric MDS

Similar to PCoA, it transform a $n$-by-$n$ distance/dissimilarity matrix $D$ into an $n$-by-$k$ coordinate matrix $X$, but this time $X$ is chosen such that the $\text{monotone transformation}(\text{distance}(X))-D$ is very small. 

The algorithm to implement NMDS is stochastic, which means you won't be getting exactly the same result with different runs.

```{r}
set.seed(1)
# method="jaccard", "bray", "unifrac", "wunifrac"
ord_bray_nmds <- ordinate(ps_rela, "NMDS", "bray", k=3)
plot_ordination(ps_rela, ord_bray_nmds, axes=c(1,3),
                type="samples", color="R.or.NR") +
  theme_bw()
set.seed(0)
ord_bray_nmds <- ordinate(ps_rela, "NMDS", "bray", k=3)
# the result of ordination is stored in ord_bray_nmds$points
plot_ordination(ps_rela, ord_bray_nmds, axes=c(1,3),
                type="samples", color="R.or.NR") +
  theme_bw()
```

Alternatively, you can plug in any other dissimilarity matrices by using `metaMDS()`.

```{r}
dist_bray <- vegdist(otu_table(ps_rela), method="bray")
set.seed(0)
res_nmds <- metaMDS(dist_bray, k=3)
plot(res_nmds$points[,c(1,3)])
```

There is no variance explained for NMDS. Instead, you can check the "stress", i.e. how big $\text{monotone transformation}(\text{distance}(X))-D$ is, for the number of components you ran. The more components you include, the smaller "stress" is. You can check where the biggest drop occurs.

```{r message=FALSE}
res_nmds$stress
ncomp <- 2:6
vec_stress <- ncomp*0
names(vec_stress) <- paste0("Num of Components=", ncomp)
for (ii in 1:length(ncomp)){
  set.seed(0)
  res_nmds_tmp <- metaMDS(dist_bray, k=ncomp[ii])
  vec_stress[ii] <- res_nmds_tmp$stress
}
plot(ncomp, vec_stress, xlab="Number of Components", ylab="Stress")
```

**Exercise:** Generate the same plots as PCoA.

# Dimension reduction for longitudinal microbiome data



TEMPoral TEnsor Decomposition [(TEMPTED) R package and vignette](https://github.com/pixushi/tempted)
