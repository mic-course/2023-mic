
## Amplicon Bioinformatic Analysis
1. [DADA2 Pipeline Lecture](bioinformatics/dada2_pipeline.pdf)
2. [Amplicon Details](bioinformatics/amplicon_details.pdf)
3. [Demultiplexing](bioinformatics/00_demultiplex_bash.md)
4. [DADA2: From FASTQs to ASVs](bioinformatics/01_dada2_tutorial.md)
5. [Build an ASV Phylogenetic Tree](bioinformatics/03_build_asv_tree.md)
6. [Amplicon Bioinformatics Workshop](bioinformatics/amplicon_bioinformatics_workshop.md)

## Appendix
1. [Configuration File](../config.R)
2. [Download Atacama FASTQs (for demux)](prep/atacama_download.Rmd)
2. [Download FASTQs](../prep/download_matson_data.Rmd)
3. [Download Taxonomic References](prep/download_dada_references.Rmd)
4. [Decontam tutorial](prep/02_decontam_tutorial.Rmd) -- Not yet complete
5. [DADA2: From FASTQs to OTUs -- Full dataset](prep/X1_dada2_tutorial_fullDataset.Rmd)
6. [Build phylogenetic tree using ASVs -- Full dataset](prep/X2_make-phylogenetic-tree_fullDataset.Rmd)
7. [Slurm script to build the tree using RAXML](prep/run-raxml.slurm)
8. [QC phyloseq object and add tree -- Full dataset](prep/X3_analyses_fullDataset.Rmd)

