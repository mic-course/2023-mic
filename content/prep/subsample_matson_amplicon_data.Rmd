---
title: "Subsample Matson Data"
output: html_document
date: "2023-02-02"
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(dplyr)
library(here)
library(readr)
library(purrr)
library(fs)
library(stringr)
library(R.utils)
library(tibble)
library(tools)

library(ShortRead)

```


```{r}
"content/config.R" %>%
  here %>%
  source

amplicon_subsample_dir
dir.create(amplicon_subsample_dir, recursive = TRUE)
```

Get the number of reads per fastq
```{r}
fastqs.df <- countFastq(amplicon_data_dir, pattern = "fastq.gz")
range(fastqs.df$records)
median(fastqs.df$records) # median of 26,310 reads per fastq.gz

```
How does this compare to the 1% atcoma data?
```{r}
fastqs.df.og <- countFastq(demux_dir, pattern = "fastq.gz")
range(fastqs.df.og$records)
median(fastqs.df.og$records)

```

Down-sample to 3%

```{r}
fastqs.df %>%
  rownames_to_column(var = "bn") %>%
  separate(bn, into = c("library","read"), sep = "_", remove = F) %>%
  filter(read == "1.fastq.gz") %>%
  mutate(records.3perc = round(records * 0.03, digits = 0)) -> lib.df

f1s <- list.files(amplicon_data_dir, pattern="1.fastq.gz", full.names=TRUE)
data.frame(f1s = f1s) %>%
  mutate(bn = basename(f1s),
         f2s = gsub("1.fastq.gz","2.fastq.gz", f1s)) %>%
  left_join(lib.df, by = "bn") -> lib.df.sorted

i <- 1
for (i in 1:nrow(lib.df.sorted)){
  
  curr.f1 <- lib.df.sorted[i,"f1s"]
  curr.f2 <- lib.df.sorted[i, "f2s"]
  curr.n <- lib.df.sorted[i,"records.3perc"]
  
  set.seed(123L)
  f1.sampler <- FastqSampler(con = curr.f1, n = curr.n, ordered = T)
  f1 <- yield(f1.sampler)
  
  set.seed(123L)
  f2.sampler <- FastqSampler(con = curr.f2, n = curr.n, ordered = T)
  f2 <- yield(f2.sampler)

  writeFastq(f1, file = file.path(amplicon_subsample_dir, basename(curr.f1)), mode="w")
  writeFastq(f2, file = file.path(amplicon_subsample_dir, basename(curr.f2)), mode="w")

}

dir(amplicon_subsample_dir)

```