#!/usr/bin/env bash

SLURM_ACCOUNT="mic-2023"
SLURM_PARTITION="chsi-high"

OUT_BASEDIR="/work/${USER}"
GROUP_DIR="/hpc/group/${SLURM_ACCOUNT}"
SINGULARITY_IMAGE="/opt/apps/community/od_chsi_rstudio/mic_2023_rstudio.sif"

mkdir -p $OUT_BASEDIR

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
RMD_DIR=$(dirname $SCRIPT_DIR)
echo $SCRIPT_DIR
echo $RMD_DIR

PROCESSORS="40"
RAM="180G"

PREPROCESS_RMD=${RMD_DIR}/00_preprocessing.Rmd
KRAKEN_RMD=${RMD_DIR}/01_kraken2-bracken.Rmd
PHYLOSEQ_RMD=${RMD_DIR}/02_make-phyloseq.Rmd

for CUR_RMD in $PREPROCESS_RMD $KRAKEN_RMD $PHYLOSEQ_RMD; do
  echo $CUR_RMD
  srun -A $SLURM_ACCOUNT -p $SLURM_PARTITION --mem=${RAM} --cpus-per-task=${PROCESSORS} singularity exec \
    --bind ${OUT_BASEDIR}:${OUT_BASEDIR} \
    --bind ${GROUP_DIR}:${GROUP_DIR} \
    $SINGULARITY_IMAGE \
    Rscript -e "rmarkdown::render('$CUR_RMD')"
done

#-----------------
# cd /work/$USER/mic2023/matson_shotgun
# Rscript -e "rmarkdown::render('$HOME/project_repos/mic_course/2023-mic/content/shotgun/01_kraken2-bracken.Rmd')"; diff -r bracken bracken_OLD
