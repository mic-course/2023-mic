## Shotgun Bioinformatic Analysis
1. [Shotgun Metagenomics Lecture](bioinformatics/shotgun_metagenomics.pdf)
2. [Preprocessing: QC, Trim and Filter](bioinformatics/00_preprocessing.Rmd)
3. [Taxonomic Classification](bioinformatics/01_kraken2-bracken.Rmd)
4. [Build a Phyloseq Object](bioinformatics/02_make-phyloseq.Rmd)
6. [Shotgun Bioinformatics Workshop](bioinformatics/shotgun_bioinformatics_workshop.md)

## Appendix
- [Kraken2 to Bracken](prep/01_kraken2-bracken.Rmd)
- [Assign NCBI taxon IDs phylogenetic tree](prep/XX_assign-taxa-to-tree.Rmd)
- [Create phyloseq objects](prep/XX_make-phyloseq.Rmd)

## References
- [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
- MultiQC
    - [Pubmed](https://pubmed.ncbi.nlm.nih.gov/27312411/)
    - [GitHub](https://github.com/ewels/MultiQC)
    - [Website](https://multiqc.info/)
- Trimmomatic
    - [Pubmed](https://pubmed.ncbi.nlm.nih.gov/24695404/)
    - [GitHub](https://github.com/usadellab/Trimmomatic)
- Kraken2
    - [Pubmed](https://pubmed.ncbi.nlm.nih.gov/31779668/)
    - [GitHub](https://github.com/DerrickWood/kraken2)
    - [Protocol](https://pubmed.ncbi.nlm.nih.gov/36171387/)
- Bracken
    - [Paper](https://doi.org/10.7717/peerj-cs.104)
    - [GitHub](https://github.com/jenniferlu717/Bracken)
    - [Why use Bracken?](https://microbe.net/2017/04/27/why-use-bracken-instead-of-kraken/)
- Kraken Tools
    - [Pubmed](https://pubmed.ncbi.nlm.nih.gov/36171387/)
    - [GitHub](https://github.com/jenniferlu717/KrakenTools)
