# Assignment
Today we analyzed shotgun metagenomic data from the Matson paper using Kraken2 and Bracken (preceded by preprocessing). We used a subset of the Matson shotgun data so that it would run quickly. For today's workshop you should repeat the analysis using more of the data. There are a few options for exactly what you should do:

1. Use the subsampled FASTQs from ALL the samples. The variable `shotgun_subsample_dir` (defined in config.R) has the path containing all of the subsampled shotgun FASTQs

2. Use the original FASTQs for a subset the samples. The variable `shotgun_data_dir` (defined in config.R) has the path containing all of the original FASTQs

3. Use the full shotgun dataset (all samples, original FASTQs). This may take several hours to run.


# Recommendations
Here are a few general approaches that you can take to this assignment. The best approach for you will depend on how you learn:

1. Open an empty Rmarkdown file and type in and run analysis code line-by-line, using tutorial material as a reference.

2. Open an empty Rmarkdown file and copy code line-by-line from the tutorial, modifying where necessary to adapt to the above assignment.

3. Copy the tutorial Rmarkdown file, modifying where necessary to adapt to the above assignment.


Whichever approach you take, we strongly recommend that you make sure you understand what each line of code is doing. First look at a chunk of code and try to figure out what it is doing and try to predict what its results will be, then run it to see if you are correct. If you can't figure out what results the chunk will produce, run the code, look at the results and try to understand how and why the code is producing the result. If the chunk has multiple lines and you can't figure out what it is doing, take it apart and run it line-by-line to see what each line is doing.

