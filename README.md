Shortened URL: https://duke.is/2tymv

# Schedule
- [Week 1 Schedule](admin/week1_schedule.md)
- [Week 2 Schedule](admin/week2_schedule.md)

# Computing Environments
- [DCC OnDemand](https://dcc-ondemand-01.oit.duke.edu/)
- [RStudio on DCC OnDemand](content/computing/dcc_ood/dcc_ood_rstudio.md)

# Workshop Content
- [Initial download of workshop content](content/computingreproducible/git_cloning.md)
- [Update workshop content](content/computing/reproducible/git_pull.md)

# Miscellaneous
- [Managing Notes](admin/managing_file_modifications.md)
- [After the MIC Course](admin/post_course_resources.md)
