
- [Fast and sensitive taxonomic classification for metagenomics with Kaiju](https://pubmed.ncbi.nlm.nih.gov/27071849/)
- [Benchmarking Metagenomics Tools for Taxonomic Classification](https://pubmed.ncbi.nlm.nih.gov/31398336/)
- [Taxonomic classification method for metagenomics based on core protein families with Core-Kaiju](https://pubmed.ncbi.nlm.nih.gov/32633756/)



- [Shallow shotgun sequencing of the microbiome recapitulates 16S amplicon results and provides functional insights](https://pubmed.ncbi.nlm.nih.gov/36112078/)
- [MetaFunc: Taxonomic and Functional Analyses of High Throughput Sequencing for Microbiomes](https://www.biorxiv.org/content/10.1101/2020.09.02.271098v3)
- [Metagenomic approaches in microbial ecology: an update on whole-genome and marker gene sequencing analyses](https://pubmed.ncbi.nlm.nih.gov/32706331/) - global overview of metagenomics. good flowcharts showing all the different steps and tools for analysis
- [Metagenomics Pipeline](https://rpubs.com/ednachiang/MetaG_Pipeline)
- [A review of computational tools for generating metagenome-assembled genomes from metagenomic sequencing data](https://pubmed.ncbi.nlm.nih.gov/34900140/)
- [Advanced metagenomic Sequence Analysis in R](https://askarbek-orakov.github.io/ASAR/)
- [Seq2Fun](https://www.seq2fun.ca) - can we use this for functional analysis (designed for RNA-Seq)
- [Benchmarking second and third-generation sequencing platforms for microbial metagenomics](https://pubmed.ncbi.nlm.nih.gov/36369227/)



# Less useful?
- [Sunbeam: an extensible pipeline for analyzing metagenomic sequencing experiments](https://pubmed.ncbi.nlm.nih.gov/30902113/)
- [The National Ecological Observatory Network's soil metagenomes: assembly and basic analysis.](https://pubmed.ncbi.nlm.nih.gov/35707452/)
- [Metagenome-assembled genomes provide new insight into the microbial diversity of two thermal pools in Kamchatka, Russia](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6395817/)

# Misc
- [Workflow for Microbiome Data Analysis: from raw reads to community analyses.](https://bioconductor.org/help/course-materials/2017/BioC2017/Day1/Workshops/Microbiome/MicrobiomeWorkflowII.html)
- [Kaiju Phylogenetic Tree to include in a Phyloseq object](https://github.com/bioinformatics-centre/kaiju/issues/215)
- [Carpentries Incubator: Data Processing and Visualization for Metagenomics](https://carpentries-incubator.github.io/metagenomics/)