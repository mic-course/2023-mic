# Computing
Here are [instructions on how to access the computing system](ondemand_howto.md)

If you don't change any settings, the container will run with the following defaults
  - Walltime (Hours): 6
  - CPUs (Threads): 2
  - Memory (RAM): 2GB

If you need more cores, more RAM, or longer runtime, you can adjust these values at launch up to the following maximums:
  - Walltime (Hours): 48
  - CPUs (Threads): 40
  - Memory (RAM): 300GB
  
If these maximums are insufficient, please discuss with Josh.

# Content
The git repo for content is here: <https://gitlab.oit.duke.edu/mic-course/2023-mic>.


## Code 
For information that needs to be shared across Rmarkdown files (e.g. shared data directory, RDS files), please use `content/config.R` and source it in your Rmarkdown files. You can see an example of this in `content/demux/demultiplex.Rmd`.

# Shared Data
If you have shared information (e.g. datasets, pre-generated RDS files) you can put them in /hpc/group/mic-2023. Please make an appropriate subdirectory. `content/config.R` defines an R variable `group_dir` with this path.

## Uploading Shared Data
### Command-line Upload
OOD is just a pretty front end for DCC, so you can use the same tools you would use for DCC (scp, sftp, rsync, Globus). RC has some details here: <https://oit-rc.pages.oit.duke.edu/rcsupportdocs/dcc/files/#cluster-shared-storage-resources-work-and-scratch>

### GUI Upload
OOD has a GUI-based mechanism for uploading and downloading data. You can get to the OOD file browser by clicking on “Files” in the OOD menu bar. In the OOD file browser you will see *Upload* and *Download* buttons. I do not know how well the GUI handles large amounts of data, so command-line tools may be better for large datasets.

# Output 
For output files participants should use a subdirectory named based on their Netid in /work. `content/config.R` defines an R variable `out_dir` and a bash variable `$OUTDIR` with this path.

