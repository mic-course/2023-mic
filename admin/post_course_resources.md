-   <a href="#high-priority" id="toc-high-priority">High Priority</a>
    -   <a href="#important-notes" id="toc-important-notes">Important Notes</a>
-   <a href="#maintaining-personal-files"
    id="toc-maintaining-personal-files">Maintaining Personal Files</a>
    -   <a href="#tar-and-download" id="toc-tar-and-download">Tar and
        Download</a>
        -   <a href="#tarring" id="toc-tarring">Tarring</a>
        -   <a href="#downloadng-the-tarball"
            id="toc-downloadng-the-tarball">Downloadng the tarball</a>
        -   <a href="#unpacking-the-tarball"
            id="toc-unpacking-the-tarball">Unpacking the tarball</a>
-   <a href="#accessing-course-material"
    id="toc-accessing-course-material">Accessing Course Material</a>
    -   <a href="#running-course-code" id="toc-running-course-code">Running
        Course Code</a>
-   <a href="#course-data" id="toc-course-data">Course Data</a>
-   <a href="#mic-computing-environment"
    id="toc-mic-computing-environment">MIC Computing Environment</a>
    -   <a href="#using-the-mic-computing-environment-after-the-course"
        id="toc-using-the-mic-computing-environment-after-the-course">Using the
        MIC Computing Environment After the Course</a>
        -   <a href="#duke-affiliates" id="toc-duke-affiliates">Duke Affiliates</a>
        -   <a href="#people-from-outside-duke"
            id="toc-people-from-outside-duke">People from outside Duke</a>
    -   <a href="#the-mic-course-singularity-image-and-recipe"
        id="toc-the-mic-course-singularity-image-and-recipe">The MIC Course
        Singularity Image and Recipe</a>

# High Priority

If you have **created or modified files** on DCC and you would like to
preserve any of them, we recommend that you follow the instructions for
[Maintaining Personal Files](#maintaining-personal-files). - We
recommend you do this before *as soon as possible*, if you are not a
Duke affiliate, you may lose access to your account as soon as the last
day of the course. - Keep in mind that you only need to worry about this
for files that you have **created or modified**. The course material
that we created and shared with you will continue to be publicly
available.

## Important Notes

See details below, but please keep the following in mind:

1.  The course material will remain in the [2023 MIC Course
    Content](https://gitlab.oit.duke.edu/mic-course/2023-mic) and will
    be publicly available, in perpetuity (or as long as
    <https://gitlab.oit.duke.edu/> continues to exist). This content is
    *publicly available*, so it does not require Duke affiliation to
    access. For details see [Accessing Course
    Material](#accessing-course-material) below.

2.  The configuration and build information for the course Docker
    container will remain in the [2023 MIC RStudio Singularity
    Image](https://gitlab.oit.duke.edu/mic-course/2023-mic-singularity-image)
    and will be publicly available, in perpetuity (or as long as
    <https://gitlab.oit.duke.edu/> continues to exist). This content is
    *publicly available*, so it does not require Duke affiliation to
    access. For details see [The MIC Course Singularity Image and
    Recipe](#the-mic-course-singularity-image-and-recipe) below.

3.  The 2023 MIC RStudio Singularity Image will remain in the Duke
    Singularity Registry and will be publicly available, in perpetuity
    (as long as the Duke Singularity Registry continues to exist and
    freely host images). This image is *publicly available*, so it does
    not require Duke affiliation to access. For details see [The MIC
    Course Singularity Image and
    Recipe](#the-mic-course-singularity-image-and-recipe) below.

# Maintaining Personal Files

## Tar and Download

### Tarring

#### Tarring: Only Notebooks

If you only want to get *Rmarkdown notebooks*, you could use the
following to only grab the notebooks from 2023-mic. This will skip a lot
of stuff you probably don’t want. This archive file will be in your home
directory and will be named 2023-mic-rmarkdowns.tar.gz
<!-- #endregion -->

``` r
tarfile_path=path.expand("~/2023-mic-rmarkdowns.tar.gz")
here() %>%
  setwd

here() %>%
  list.files(recursive = TRUE, 
             pattern=".Rmd",
             full.names = FALSE) %>%
  tar(tarfile=tarfile_path,
      files=.,
      compression = "gzip")
```

    ## Warning: invalid gid value replaced by that for user 'nobody'

#### Tarring: Only Notebooks with a common name

If you want only modified notebooks **and** you saved them with a
standard naming scheme, e.g. leaving `-Copy1` in the name, for example,
renaming `demultiplex.Rmd` to `demultiplex-Copy1.Rmd`, you could use the
following to only grab the modified files from 2023-mic

``` r
tarfile_path=path.expand("~/2023-mic-copyof.tar.gz")
here() %>%
  setwd

here() %>%
  list.files(recursive = TRUE, 
             pattern="CopyOf",
             full.names = FALSE) %>%
  tar(tarfile=tarfile_path,
      files=.,
      compression = "gzip")
```

<!-- #region -->

### Downloadng the tarball

Now you can do one of the following to download the tarball to your
laptop.

1.  In the RStudio *Files* pane, click on *Home* to navigate to the
    directory where you saved the tarball. T
2.  Click the checkbox next to `2023-mic-rmarkdowns.tar.gz`
3.  In the *More* menu of the *Files* pane, select *Export*, then click
    the *Download* in the dialog box that pops up.

### Unpacking the tarball

On a Mac you can “untar” by double clicking on the file in finder, or at
the terminal with the command `tar -zxf 2023-mic-rmarkdowns.tar.gz`.

On Windows, you can download software that will do it, such as
[7-Zip](http://www.7-zip.org/)

> If you named your tarball differently, you should substitute whatever
> name you used above.

# Accessing Course Material

You can access the course material in three different ways:

1.  You can browse and download the material from the [2023 MIC Course
    Repository](https://gitlab.oit.duke.edu/mic-course/2023-mic) by
    clicking on the Download button, which is right next to the blue
    **Clone** button. It will give you a choice of format you want. The
    best options are probably “zip” or “tar.gz”
2.  You can **clone** the repo using git:
    `git clone https://gitlab.oit.duke.edu/mic-course/2023-mic.git`

## Running Course Code

After the course, you will probably need to modify some paths in
[config.R](../content/config.R) to get Rmds in the course repo to run in
a different computing environment. This will probably be necessary even
for Duke affiliates, because as things are currently set up, raw data,
reference files, and databases are in the `mic-2023` group directory,
which will be deleted some time after the course ends.

We tried to make things portable, so it will probably be sufficient to
modify the following two lines of code in the
[config.R](../content/config.R).

    scratch_dir="/work"
    group_dir="/hpc/group/mic-2023"

`scratch_dir` is where output files go, and `group_dir` is where raw
data, reference files, and databases are stored.

# Course Data

The data that we used in the course can be downloaded from [BioProject
PRJNA399742](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA399742).

The script
[download_matson_data.Rmd](../content/prep/download_matson_data.Rmd)
will download all of the Matson data.

# MIC Computing Environment

## Using the MIC Computing Environment After the Course

### Duke Affiliates

If you are a Duke Affiliate, you can continue to use DCC OnDemand.

1.  If you did not have a DCC account before the course, you will need
    to [follow these
    instructions](https://oit-rc.pages.oit.duke.edu/rcsupportdocs/dcc/#getting-a-dcc-account)
    to request one. We recommend you do this as soon as possible,
    otherwise there is a risk that your DCC account will be deleted when
    the mic-2023 group is decommissioned.

2.  When launching a container, you will need to change the *Account*
    and *Partition* values to an account and partition to which you have
    access.

### People from outside Duke

#### Open OnDemand

The computing environment for our course ran within an [Open
OnDemand](https://openondemand.org/) system. Look at your own
institution to find out if you have access to a cluster running [Open
OnDemand](https://openondemand.org/). If so, you can ask the
administrators if you can run the MIC Course environment there.

1.  You will need the [Singularity Recipe or Singularity
    Image](#the-mic-course-singularity-image-and-recipe)
2.  You may need our [Open OnDemand Batch Connect
    App](https://gitlab.oit.duke.edu/chsi-informatics/ood_apps/mic-course-bc)

If [Open OnDemand](https://openondemand.org/) is not installed on your
local cluster, ask the system administrators to install it. It is free,
was developed with funding from the US National Science Foundation, and
is becoming a standard!

You can also run the Singularity image on a cluster or a local server
without Open OnDemand, but that is more complicated!

## The MIC Course Singularity Image and Recipe

The computing environment that we used in the MIC course ran withing a
Singularity Container. The Singularity Image Recipe is here: [2023 MIC
RStudio Singularity
Image](https://gitlab.oit.duke.edu/mic-course/2023-mic-singularity-image)
and the built image can be pulled (downloaded) to your current directory
by running the following command in a terminal on a computer where
`singularity` or `apptainer` is installed

    export IMAGEDIR=/work/${USER}/images
    mkdir -p $IMAGEDIR
    singularity pull --force --dir $IMAGEDIR oras://gitlab-registry.oit.duke.edu/mic-course/2023-mic-singularity-image:2023_final

> Note: 1. This command will not work within the MIC course environment,
> because Singularity is not installed within it. 2. You will want to
> change IMAGEDIR to a different directory if you are not running this
> command on DCC

The [SingularityCE User
Guide](https://docs.sylabs.io/guides/latest/user-guide/) has information
on installing and using Singularity
