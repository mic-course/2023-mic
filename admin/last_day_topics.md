# Hands-on Activities
- Building phylogenetic trees [Josh]
- Demultiplexing [Josh]
- More git
  - Collaborating with git [Josh]
  - Starting a repo on Gitlab/Github [Josh]
- Building Singularity Containers [Josh]
- Help with Week Two "Assignments" [Josh]
- Running (big) jobs with SLURM [Josh]


